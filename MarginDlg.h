#pragma once


// CMarginDlg dialog

class CMarginDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CMarginDlg)

public:
	CMarginDlg(tagComponentParameter cp,CWnd* pParent = nullptr);   // standard constructor
	virtual ~CMarginDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_MARGIN_DLG };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	tagComponentParameter m_cp;
	afx_msg void OnBnClickedOk();
};
