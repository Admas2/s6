﻿#include "pch.h"
#include <AcExtensionModule.h>
#include <windows.h>
#include "arxHeaders.h"
#include "TestPlatform.h"
#include "DataBaseOpr.h"
#include "ComponentMargin.h"
#include "ComponentBench.h"
#include "ComponentDivider.h"
#include "Roadway.h"
#include "CreateBenchTool.h"
#include "RawModeDataCheck.h"
#include "global.h"
#include "ModuleApp.h"
#include "StringTransfer.h"

CModuleApp theApp;
extern "C" AcRx::AppRetCode acrxEntryPoint(AcRx::AppMsgCode msg, void* pkt)
{
	switch (msg)
	{
		case AcRx::kInitAppMsg:
		{
			theApp.InitModule(pkt);
			break;
		}
		case AcRx::kUnloadAppMsg:
			theApp.UninitModlue();
			break;
		default:
			break;
	}
	return AcRx::kRetOK;
}

//----------------------------------------------------------
EXPORT_DEFINE int SetDatabasePath(wchar_t* dbName) {
	theApp.SetDatabasePath(Unicode2Utf8(std::wstring(dbName)));
	return 1;
}

EXPORT_DEFINE int CalcActualWidthByPickLine(char* RoadLine_Name) {
	::MessageBox(acedGetAcadDwgView()->GetSafeHwnd(), L"拾取老路道路边线", L"道路改扩建", MB_OK);

	//1.
	acDocManager->lockDocument(acDocManager->curDocument());
	int result = -1;
	AcDbEntity* pEntity1 = nullptr;
	AcDbEntity* pEntity2 = nullptr;
	AcArray<tagMeasureWidth> mws;
	AcDbObjectIdArray IDs;
	AcGePoint3dArray& points = theApp.BorrowGlobalPt3dAry();

	//2.查询道路中线
	AcArray<tagFitPoint> fitPoints;
	CDataBaseOpr db;
	bool r = db.Open();
	std::string roadLineID = db.QueryRoadLineID(RoadLine_Name);
	fitPoints = db.QueryRoadLineCoordinate(roadLineID);
	if (fitPoints.isEmpty()) goto exit;

	//3. 拾取两边线
	acutPrintf(L"请拾取两道路边线\n");
	IDs = PickEntities();
	if (IDs.length() != 2)  goto exit;
	es = acdbOpenAcDbEntity(pEntity1, IDs[0], AcDb::kForWrite);
	if (es != Acad::eOk) goto exit;
	if (!pEntity1->isKindOf(AcDbPolyline::desc())) goto exit;
	es = acdbOpenAcDbEntity(pEntity2, IDs[1], AcDb::kForWrite);
	if (es != Acad::eOk) goto exit;
	if (!pEntity2->isKindOf(AcDbPolyline::desc())) goto exit;
	AcDbPolyline* pline1 = AcDbPolyline::cast(pEntity1);
	AcDbPolyline* pline2 = AcDbPolyline::cast(pEntity2);

	//4.计算实际左右幅路宽
	
	for (int index = 0; index < fitPoints.length(); index++) {
			
		//4.1 计算中点两边线的距离
		AcGePoint3d point(fitPoints[index].x, fitPoints[index].y, 0.0);
		tagMeasureWidth item;
		item.stakeNo = fitPoints[index].stakeNo;
		AcGePoint3d pointOnCurve1, pointOnCurve2;
		es = pline1->getClosestPointTo(point, pointOnCurve1);
		if (es != Acad::eOk) goto exit;
		es = pline2->getClosestPointTo(point, pointOnCurve2);
		if (es != Acad::eOk) goto exit;

		//判断左右幅并保存
		item.LeftWidth = pointOnCurve1.distanceTo(point);
		item.RightWidth = pointOnCurve2.distanceTo(point);
		AcGeVector3d v1 = point - pointOnCurve1;
		AcGeVector3d flag = v1.crossProduct(fitPoints[index].vecTagent);// 利用矢量作为判别式
		if (flag.z > 0) {
			swap(item.LeftWidth, item.RightWidth);
		}
		mws.append(item);
	}

		//7.保存到数据中
		db.SaveToTable_OldRoadActualWidth(mws, roadLineID);
		result = 0;
exit:
	if(pEntity1) pEntity1->close();
	if(pEntity2) pEntity2->close();
	theApp.ReturnGlobalPt3dAry(&points);
	acDocManager->unlockDocument(acDocManager->curDocument());
	return result;
}

EXPORT_DEFINE int CreateExtendRoad(char* newRoadName, char* oldRoadName, double startStake, double endStake) {
	std::wstring newRoad = Utf82Unicode(std::string(newRoadName));
	std::wstring oldRoad = Utf82Unicode(std::string(oldRoadName));
	if (theApp[newRoad] == nullptr) {
		logInfo.Format(L"请先创建新路 %s", newRoad.c_str()); MESSAGE_BOX
		return 0;
	}
	
	if (theApp[oldRoad] == nullptr) {
		logInfo.Format(L"请先创建老路 %s", oldRoad.c_str());  MESSAGE_BOX
		return 0;
	}

	acDocManager->lockDocument(acDocManager->curDocument());

	logInfo.Format(L"开始创建 扩建道路");		 MESSAGE_BOX
	CRoadway* extendRoad = CRoadway::CreateFromeExtendRoad(theApp[newRoad], theApp[oldRoad], startStake, endStake);
	theApp.AppendRoad(extendRoad);
	
	//创建完改扩建道路后，直接对其算量
	theApp[extendRoad->name()]->StatisticsMaterialVolume();

	logInfo.Format(L"完成创建 扩建道路");			  MESSAGE_BOX
	acDocManager->unlockDocument(acDocManager->curDocument());
	return 1;
}						   

EXPORT_DEFINE int CreateRoadModel(char* RoadLine_Name,double startStake, double endStake,int isNewRoad) {

	acDocManager->lockDocument(acDocManager->curDocument());

	bool result = 0;
	tagRoadwayData	rd;
	rd.isNewRoad = (isNewRoad == 1);
	rd.startStake = startStake;
	rd.endStake = endStake;
	rd.RoadLine_Name = Utf82Unicode(std::string(RoadLine_Name));
	if (ReadRoadDataFromDB(rd)) {
			logInfo.Format(L"开始道路建模 %s[%.2f---%.2f]\n", Utf82Unicode(std::string(RoadLine_Name)).c_str(), startStake, endStake); MESSAGE_BOX RDTRACE
			CRoadway* pRoad = CRoadway::CreateFromRawData(rd);
			theApp.AppendRoad(pRoad);

			//对道路算量
			pRoad->StatisticsMaterialVolume();
			result = 1;
			logInfo.Format(L"道路建模结束 %s[%.2f---%.2f]\n", Utf82Unicode(std::string(RoadLine_Name)).c_str(), startStake, endStake); MESSAGE_BOX  RDTRACE
	}
	acDocManager->unlockDocument(acDocManager->curDocument());
	
	return result;
}