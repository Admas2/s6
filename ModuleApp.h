﻿#pragma once
#include <vector>
#include "Roadway.h"
#include "ReactorOperation.h"
class CModuleApp
{
private:
	struct tagRoadModel {
		AcDbDatabase* pDbDatabase = nullptr;
		AcArray<CRoadway*> roads_;
	};

	AcArray<tagRoadModel>  allRoads_;
	std::string dbName_;

	RDEventReactor*					m_eventReactor = nullptr;
	RDDatabaseReactor*			m_dbReactor = nullptr;
	RDDocumentReactor*			m_docReactor = nullptr;
	RDEditorReactor*				m_edReactor = nullptr;

	//对于AcGePoint3dArray类型的使用，在释放时会引发崩溃，原因还没找到，所以程序中先定义好备用，
	//以后要使用都从这里引用，只到程序退出都不会释放，会有内存泄漏，暂时只能这样
	AcArray<AcGePoint3dArray*> pt3DAry_;
private:
	void RegisterCommand();
	CStdioFile logFile_;
	bool isFileOpen_ = false;
public:
	CModuleApp();
	~CModuleApp();
	void InitModule(void* pkt);
	void UninitModlue();
	void AppendRoad(CRoadway* road);
	CRoadway* operator[](int index);
	CRoadway* operator[](std::wstring roadName);
	void SetDatabasePath(std::string dbName) { dbName_ = dbName; }
	std::string GetDatabasePath() { return dbName_; }
	AcArray<CRoadway*>& FindRoadways(AcDbDatabase* pDb);
	void OnDocumentCreated(AcDbDatabase* pDbDatabase);
	void OnDocumentToBeDestroyed(AcDbDatabase* pDbDatabase);
	void CreatePresetLayer(AcDbDatabase* pDbDatabase);
	void CloseLogFile();
	void AppendInfoToLogFile();
	void OpenLogFile();

	AcGePoint3dArray& BorrowGlobalPt3dAry();
	void ReturnGlobalPt3dAry(AcGePoint3dArray* ptr);
};

