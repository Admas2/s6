#pragma once

#include <adui.h>
// CPickRoadEdgeDlg dialog

class CPickRoadEdgeDlg : public CAdUiBaseDialog
{
	DECLARE_DYNAMIC(CPickRoadEdgeDlg)

public:
	CPickRoadEdgeDlg(CWnd* pParent, HINSTANCE hDialogResource);   // standard constructor
	virtual ~CPickRoadEdgeDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_PICK_ROAD_EDGE_DLG };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedPickEdge();
};
