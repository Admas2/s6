﻿#include "pch.h"
#include "global.h"
#include "EntityXData.h"

void SetRegionXData(AcDbObjectId entityID, tagRegionXData info) {
	AcDbEntity *pEntity = nullptr;
	es = acdbOpenAcDbEntity(pEntity, entityID, AcDb::kForWrite);
	if (es == Acad::eOk) {
		resbuf* pRb = acutBuildList(
			AcDb::kDxfRegAppName, gOptions.appName,			  // 应用程序名称
			AcDb::kDxfXdInteger32, info.materialID,
			AcDb::kDxfXdInteger32, info.isLeft ? 1 : 0,
			AcDb::kDxfXdInteger32, info.colorIndex,
			AcDb::kDxfXdInteger32, info.sideKind,
			AcDb::kDxfXdAsciiString,info.name.c_str(),
			AcDb::kDxfXdReal,info.stakeNo,
			AcDb::kDxfXdReal, info.depth,
			RTNONE);
		pEntity->setXData(pRb);
		acutRelRb(pRb);
		pEntity->close();
	}
}

tagRegionXData GetSectionXData(AcDbObjectId entityID) {
	tagRegionXData info = {};
	AcDbEntity *pEntity = nullptr;
	es = acdbOpenAcDbEntity(pEntity, entityID, AcDb::kForRead);
	if (es != Acad::eOk) {
		return info;
	}

	if (!pEntity->isKindOf(AcDbRegion::desc())) {
		pEntity->close();
		return info;
	}

	assert(gOptions.appName);
	resbuf* buffer = pEntity->xData(gOptions.appName);
	if (buffer == nullptr) {
		pEntity->close();
		return info;
	}
	
	buffer = buffer->rbnext;
	assert(buffer->restype == AcDb::kDxfXdInteger32);
	info.materialID = buffer->resval.rlong;

	buffer = buffer->rbnext;
	assert(buffer->restype == AcDb::kDxfXdInteger32);
	info.isLeft = (buffer->resval.rint == 1);

	buffer = buffer->rbnext;
	assert(buffer->restype == AcDb::kDxfXdInteger32);
	info.colorIndex = buffer->resval.rlong;

	buffer = buffer->rbnext;
	assert(buffer->restype == AcDb::kDxfXdInteger32);
	info.sideKind = (eSectionSideKind)(buffer->resval.rlong);

	buffer = buffer->rbnext;
	assert(buffer->restype == AcDb::kDxfXdAsciiString);
	info.name = std::wstring(buffer->resval.rstring);

	buffer = buffer->rbnext;
	assert(buffer->restype == AcDb::kDxfXdReal);
	info.stakeNo = buffer->resval.rreal;

	buffer = buffer->rbnext;
	assert(buffer->restype == AcDb::kDxfXdReal);
	info.depth = buffer->resval.rreal;

	pEntity->close();
	return 	info;
}

void SetBodyXData(AcDbObjectId entityID, tagBodyXData info)
{
	AcDbEntity *pEntity = nullptr;
	es = acdbOpenAcDbEntity(pEntity, entityID, AcDb::kForWrite);
	if (es == Acad::eOk ) {
		resbuf* pRb = acutBuildList(
			AcDb::kDxfRegAppName, gOptions.appName,			  // 应用程序名称
			AcDb::kDxfXdReal, info.startStake,
			AcDb::kDxfXdReal, info.endStake,
			AcDb::kDxfXdInteger32, info.materialID,		// 材料编号 		
			AcDb::kDxfXdInteger32, info.sideKind,
			AcDb::kDxfXdInteger32, info.isLeft ? 1 : 0,
			AcDb::kDxfXdAsciiString, info.name.c_str(),
			RTNONE);
		pEntity->setXData(pRb);
		acutRelRb(pRb);
		pEntity->close();
	}
}

tagBodyXData GetBodyXData(AcDbEntity* pEntity) {
	tagBodyXData info = {};
	if (!pEntity->isKindOf(AcDb3dSolid::desc())) {
		pEntity->close();
		return info;
	}

	resbuf* buffer = pEntity->xData(gOptions.appName);
	if (buffer == nullptr) {
		pEntity->close();
		return info;
	}
	buffer = buffer->rbnext;
	assert(buffer->restype == AcDb::kDxfXdReal);
	info.startStake = buffer->resval.rreal;

	buffer = buffer->rbnext;
	assert(buffer->restype == AcDb::kDxfXdReal);
	info.endStake = buffer->resval.rreal;

	buffer = buffer->rbnext;
	assert(buffer->restype == AcDb::kDxfXdInteger32);
	info.materialID = buffer->resval.rlong;

	buffer = buffer->rbnext;
	assert(buffer->restype == AcDb::kDxfXdInteger32);
	info.sideKind = (eSectionSideKind)(buffer->resval.rint);

	buffer = buffer->rbnext;
	assert(buffer->restype == AcDb::kDxfXdInteger32);
	info.isLeft = (buffer->resval.rint == 1);

	buffer = buffer->rbnext;
	assert(buffer->restype == AcDb::kDxfXdAsciiString);
	info.name = std::wstring(buffer->resval.rstring);
	return 	info;
}

tagBodyXData GetBodyXData(AcDbObjectId entityID) {
	tagBodyXData info = {};
	AcDbEntity *pEntity = nullptr;
	es = acdbOpenAcDbEntity(pEntity, entityID, AcDb::kForRead);
	if (es != Acad::eOk) {
		return info;
	}

	info  = GetBodyXData(pEntity);

	pEntity->close();
	return 	info;
}