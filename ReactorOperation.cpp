#include "pch.h"
#include "ReactorOperation.h"
#include "global.h"

//-------------------------------------------------------
ACRX_NO_CONS_DEFINE_MEMBERS(RDEventReactor, AcRxEventReactor);
RDEventReactor::RDEventReactor() {
	acrxEvent->addReactor(this);
}

RDEventReactor::~RDEventReactor() {
	acrxEvent->addReactor(this);
}

void RDEventReactor::dwgFileOpened(AcDbDatabase* db, ACHAR * fileName) 
{
	TRACE("RDEventReactor::dwgFileOpened\n");
}

//----------------------------------------------------------------------
ACRX_NO_CONS_DEFINE_MEMBERS(RDDatabaseReactor, AcDbDatabaseReactor);
RDDatabaseReactor::RDDatabaseReactor() {

}

RDDatabaseReactor::~RDDatabaseReactor() {

}

void RDDatabaseReactor::objectAppended(const AcDbDatabase* dwg, const AcDbObject* obj)
{
	//TRACE("RDDatabaseReactor::objectAppended\n");
}

//----------------------------------------------------------------------
ACRX_NO_CONS_DEFINE_MEMBERS(RDDocumentReactor, AcApDocManagerReactor);
RDDocumentReactor::RDDocumentReactor() {
	acDocManager->addReactor(this);
}

RDDocumentReactor::~RDDocumentReactor() {
	acDocManager->addReactor(this);
}

void RDDocumentReactor::documentCreated(AcApDocument* pDocCreating) {
	TRACE("RDDocumentReactor::documentCreated\n");
	AcDbDatabase* pDB = pDocCreating->database();
	theApp.OnDocumentCreated(pDB);
}

void RDDocumentReactor::documentToBeDestroyed(AcApDocument* pDocToDestroy)
{
	AcDbDatabase* pDB = pDocToDestroy->database();
	theApp.OnDocumentToBeDestroyed(pDB);
}

//------------------------------------------------------------------------
ACRX_NO_CONS_DEFINE_MEMBERS(RDEditorReactor, AcEditorReactor2);
RDEditorReactor::RDEditorReactor() {
	acedEditor->addReactor(this);
}

RDEditorReactor::~RDEditorReactor() {
	acedEditor->addReactor(this);
}

void RDEditorReactor::commandWillStart(const TCHAR* cmdStr) {
	TRACE("RDEditorReactor::commandWillStart\n");
}

void RDEditorReactor::commandEnded(const TCHAR* cmdStr){
	TRACE("RDEditorReactor::commandEnded\n");
};