﻿#pragma once
#include "pch.h"
#include "TypeDef.h"


//道路中线上的拟合点，提供三种方式，并提供三者之间的转换
//所在 数组|segmentPoints_| 中的索引值
//弧长坐标
//拟合点的直角坐标值
class CRoadCenterLine
{
private:
	AcDbObjectId curveEntityID_;									//供显示的中线
	AcDbObjectId splineID_;												//供造型的中线
	AcArray<tagFitPoint>			segmentPoints_;			//中线的拟合点集表示
	AcArray<tagLineInterval>	noModelIntervals_;	//不须建模区间
	AcArray<tagLineInterval>	modelSegments_;			//建模时分段
	
	std::string roadLineID_;
public:
	CRoadCenterLine();
	CRoadCenterLine(std::string roadLineID);
	virtual ~CRoadCenterLine();
	double GetWholeLength();
	std::string GetRoadlineID() const { return roadLineID_; }
	AcArray<tagLineInterval> GetModelIntervals();
	void Create(AcArray<tagFitPoint> fitPoints);
	void AppendNoModelInterval(int startStake, int endStake);
	void SetNoModelIntervals(const AcArray<tagLineInterval>&	noModelIntervals);
	void SetModelSegments(const AcArray<tagLineInterval>&	modelSegments);
	void RemoveEntities();
	double GetFirstStakeNo() const;
	double GetLastStakeNo() const;
	AcDbObjectId GetCenterLineEntityID() { return splineID_; }
	int GetIndexAtStake(double stakeNo) const;
	AcGeVector3d GetTagentDir(double stakeNo) const;
	AcGePoint3d  GetPoint(double stakeNo) const;
	AcGePoint3dArray GetSegmentPoints() const;
	CRoadCenterLine* PartialCopy();
private:
	void DebugLookData();
};

