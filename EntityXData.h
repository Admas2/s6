#pragma once
#include "pch.h"
void SetBodyXData(AcDbObjectId entityID, tagBodyXData info);
tagBodyXData GetBodyXData(AcDbObjectId entityID);
tagRegionXData GetSectionXData(AcDbObjectId entityID);
void SetRegionXData(AcDbObjectId entityID, tagRegionXData info);
tagBodyXData GetBodyXData(AcDbEntity* pEntity);

