﻿#include "pch.h"
#include "TestPlatform.h"
#include "Roadway.h"
#include "RoadCenterLine.h"
#include "AcExtensionModule.h"
#include "ComponentMargin.h"
#include "ComponentBench.h"
#include "ComponentDivider.h"
#include "CreateRegionTool.h"
#include "CreateBenchTool.h"
#include "DataBaseOpr.h"
#include "PickRoadEdgeDlg.h"
#include "BooleanOperation.h"
#include "RawModeDataCheck.h"
#include "global.h"
#include "EntityXData.h"
#include "LayerOperation.h"
#include "StringTransfer.h"

std::string MakeTestDBName(std::string s) {
	char buffer[200];
	sprintf(buffer, "H:\\17.RodeDesign\\civ3d\\sourse\\RoadDesign\\TestData2\\%s", s.c_str());
	std::string r(buffer);
	return r;
}

std::string MakeTestDBName(int index = -1) {
	if (index == -1) {
		return std::string("H:\\17.RodeDesign\\civ3d\\sourse\\RoadDesign\\RoadDesign.plug-in\\bin\\Debug\\db\\zjsqlite.db");
	}
	char buffer[200];
	sprintf(buffer, "H:\\17.RodeDesign\\civ3d\\sourse\\RoadDesign\\TestData2\\zjsqlite%d.db", index);
	std::string r(buffer);
	return r;
}

class TemporaryMoveToCenter
{
public:
	TemporaryMoveToCenter() {
		gOptions.isMoveToViewCenter = true;
	}
	~TemporaryMoveToCenter() {
		gOptions.isMoveToViewCenter = false;
	}
};

string trim(string src) {
	src.erase(0, src.find_first_not_of("\""));
	src.erase(src.find_last_not_of("\"") + 1);
	return src;
}

//模拟纵向坡度
double MockLongitudinalSlope(double x) {
#if false
	double r = 0.09 * x + 20;
	return r;
#endif
	double r = 6 * cos(0.2*x) + 20;
	return r;
}

//----------------------------------------------------------
CTestPlatform::CTestPlatform()
{
}

CTestPlatform::~CTestPlatform() {

}

tagRoadwayData CTestPlatform::MockCreateInputUserData1() {
	tagRoadwayData rd;
	tagSideSectionData ssd;
	ssd.stakeNo = 0;
	tagPavementSegment seg;
	seg.CrossSlop = 0.1;
	seg.width = 32.0;
	ssd.pavementWidth.append(seg);
	ssd.pavamentStruct = gPredefinedPavementStructureList[2];
	tagPavementSegment pavamentStruct;
	pavamentStruct.width = 32.0;
	ssd.pavementWidth.append(pavamentStruct);

	tagComponentParameter cp;
	//隔离带
	cp.componentKind = component_kind_divider;
	cp.h2 = 3.16;
	cp.h3 = 1.9;
	cp.h4 = 0.6;
	cp.w2 = 2.4;
	cp.w3 = 1.4;
	ssd.componentParameters.appendMove(cp);

	//台阶
	cp.componentKind = component_kind_bench;
	cp.h2 = 0.5;
	cp.h3 = 1.5;
  cp.h4 = 2.8; 
	cp.w2 = 0.3;
	cp.w3 = 6;
	ssd.componentParameters.appendMove(cp);

	//边部构造
	cp.componentKind = component_kind_margin;
	cp.h2 = 2;
	cp.h3 = 1;
	cp.h4 = 1.1;
	cp.w2 = 1.8;
	cp.w3 = 0.3;
	cp.w4 = 3;
	cp.n2 = 4.5;
	ssd.componentParameters.appendMove(cp);

	rd.sections.appendMove(ssd);
	return rd;
}

void CTestPlatform::GeneratePavementWidthTestData() {
	gPredefinedPavementWidthList.removeAll();
	AcArray<tagPavementSegment>  item;
	//注意：各路段的排放顺序要按SortID绝对值排升序才行
	//左幅	 SortID < 0						 SortID	width	CrossSlop
	item.append(tagPavementSegment{ -1,	1,	    0.0 });
	item.append(tagPavementSegment{ -2,	0.5,	-0.02 });
	item.append(tagPavementSegment{ -3,	7.5,	-0.02 });
	item.append(tagPavementSegment{ -4,	2.5,	-0.02 });
	item.append(tagPavementSegment{ -5,	0.75,	-0.04});
	gPredefinedPavementWidthList.append(item);

	item.removeAll();
	//左幅	 SortID > 0						 SortID	width	CrossSlop
	item.append(tagPavementSegment{ 1,	1,	    0.0 });
	item.append(tagPavementSegment{ 2,	0.5,	-0.02 });
	item.append(tagPavementSegment{ 3,	7.5,	-0.02 });
	item.append(tagPavementSegment{ 4,	2.5,	-0.02 });
	item.append(tagPavementSegment{ 5,	0.75,	-0.04 });
	gPredefinedPavementWidthList.append(item);
}

void CTestPlatform::GeneratePavementStructTestData() {
	gPredefinedPavementStructureList.removeAll();
	AcArray<tagPavementLayer>  item;
	item.append(tagPavementLayer{ 5.8,4});
	gPredefinedPavementStructureList.append(item);

	item.removeAll();
	item.append(tagPavementLayer{ 0.3,0,});
	item.append(tagPavementLayer{ 0.36,1});
	item.append(tagPavementLayer{ 0.6,2,});
	gPredefinedPavementStructureList.append(item);

	item.removeAll();
	item.append(tagPavementLayer{ 0.05,1 });
	item.append(tagPavementLayer{ 0.08,2 });
	item.append(tagPavementLayer{ 0.13,8 });
	item.append(tagPavementLayer{ 0.14,4 });
	item.append(tagPavementLayer{ 0.2,5});
	item.append(tagPavementLayer{ 0.42,6 });
	gPredefinedPavementStructureList.append(item);

	item.removeAll();
	item.append(tagPavementLayer{ 0.04,0});
	item.append(tagPavementLayer{ 0.07,1});
	item.append(tagPavementLayer{ 0.09,2});
	gPredefinedPavementStructureList.append(item);
}

AcGePoint3dArray CTestPlatform::MockCreateRoadCenterLinePoint1() {

	//1.产生x,y值
	AcGePoint3dArray fitPoints;
	#if false
	fitPoints.append(AcGePoint3d(50.52,  30.0, 0.00));
	fitPoints.append(AcGePoint3d(75.12,  30.0, 0.00));
	fitPoints.append(AcGePoint3d(109.90, 30.0, 0.00));
	fitPoints.append(AcGePoint3d(165.04, 30.0, 0.00));
	fitPoints.append(AcGePoint3d(218.77, 30.0, 0.00));
	fitPoints.append(AcGePoint3d(282.67, 30.0, 0.00));
	fitPoints.append(AcGePoint3d(357.32, 30.0, 0.00));
	fitPoints.append(AcGePoint3d(408.50, 30.0, 0.00));
	fitPoints.append(AcGePoint3d(454.59, 30.0, 0.00));
	#else
	fitPoints.append(AcGePoint3d(50.52, 0.16, 0.00));
	fitPoints.append(AcGePoint3d(75.12, 15.99, 0.00));
	fitPoints.append(AcGePoint3d(109.90, 25.88, 0.00));
	fitPoints.append(AcGePoint3d(165.04, 25.88, 0.00));
	fitPoints.append(AcGePoint3d(218.77, 15.42, 0.00));
	fitPoints.append(AcGePoint3d(282.67, 5.25, 0.00));
	fitPoints.append(AcGePoint3d(357.32, 23.05, 0.00));
	fitPoints.append(AcGePoint3d(408.50, 31.25, 0.00));
	fitPoints.append(AcGePoint3d(454.59, 14.01, 0.00));

	//2.加上Z方向值
	for (int index = 0; index < fitPoints.length(); index++) {
		AcGePoint3d point;
		fitPoints[index].z = MockLongitudinalSlope(fitPoints[index].x);
	}
	#endif

	return fitPoints;
}

AcArray<tagFitPoint> CTestPlatform::MockCreateRoadCenterLinePoint2() {
	AcArray<tagFitPoint> fitPoints;
	AcGePoint3dArray points = MockCreateRoadCenterLinePoint1();
	//加上桩号
	const int step = 1;
	for (int index = 0; index < points.length(); index++) {
		tagFitPoint  fitPt;
		fitPt.stakeNo = 74000 + index * step;
		fitPt.x = points[index].x;
		fitPt.y = points[index].y;
		fitPt.z = points[index].z;
		fitPoints.append(fitPt);
	}

	//计算拟合点的切线方向
	calcRoadLineVecTagent(fitPoints);

	return fitPoints;
}

void CTestPlatform::ReadRoadCenterLineFromCSV(tagRoadwayData& rd,const char* fileName,bool needMove) {

	AcArray<tagFitPoint> fitPoints;

	//1.
	ifstream fin(fileName); //打开文件流操作
	if (!fin.is_open()) {
		acutPrintf(L"Model_ContStakeXYZ.csv 没找到！\n", fileName);
		return ;
	}
	
	//2.
	tagFitPoint  fitPt;
	string line;
	getline(fin, line);
	while (getline(fin, line))							 //整行读取，“\n”区分
	{
		istringstream sin(line);								//将整行字符串line读入到字符串流istringstream中
		vector<string> fields;									//声明一个字符串向量
		string field;
		while (getline(sin, field, ','))				//将字符串流sin中的字符读入到field字符串中，以逗号为分隔符
		{
			fields.push_back(field);							//将刚刚读取的字符串添加到向量fields中
		}
		if (trim(fields[2]) == "left") {
			fitPt.stakeNo = ::atoi(trim(fields[3]).c_str());
			fitPt.x = ::atof(trim(fields[5]).c_str());
			fitPt.y = ::atof(trim(fields[6]).c_str());
			fitPt.z = ::atof(trim(fields[7]).c_str());
			fitPoints.append(fitPt);
		}
	}
	
	//为了方便观察，将曲线包围盒中心移到坐标原点
	if (needMove) {
		MoveToViewCenter(fitPoints);
	}
	rd.fitPoints = fitPoints;
}


Acad::ErrorStatus CTestPlatform::MockGenerateRoadCenterLine1(AcDbSpline*& p3DRoadLine) {

	//拾取一条平面道路中心线
	acutPrintf(L"\n 请拾取一条样条曲线作为道路平面线\n");
	ads_name ss;
	int rt = acedSSGet(NULL, NULL, NULL, NULL, ss);  	// 提示用户选择对象 
	AcDbObjectIdArray objIds;
	if (rt != RTNORM) return Acad::eInvalidInput;
	Adesk::Int32 length;
	acedSSLength(ss, &length);
	if (length != 1) return Acad::eInvalidInput;
	ads_name entityName;
	acedSSName(ss, 0, entityName);
	AcDbObjectId roadCenterLineID;
	acdbGetObjectId(roadCenterLineID, entityName);
	AcDbEntity *pEntity = nullptr;
	acdbOpenAcDbEntity(pEntity, roadCenterLineID, AcDb::kForRead);
	if (!pEntity->isKindOf(AcDbSpline::desc())) return Acad::eInvalidInput;
	AcDbSpline* pRoadCenterLine = AcDbSpline::cast(pEntity);

	//取出所有拟合点，加上纵向坡度，再重新样条曲线
	AcGePoint3dArray points;
	int fitNum = pRoadCenterLine->numFitPoints();
	for (int index = 0; index < fitNum; index++) {
		AcGePoint3d point;
		pRoadCenterLine->getFitPointAt(index, point);
		point.z = MockLongitudinalSlope(point.x);
		points.append(point);
	}
	pRoadCenterLine->close();
	p3DRoadLine = new AcDbSpline(points, 4, 0.0);
	return Acad::eOk;
}

void CTestPlatform::Initialize() {

	GeneratePavementStructTestData();
	GeneratePavementWidthTestData();
}

void CTestPlatform::TestRoadwayDlg() {
	//AFX_MANAGE_STATE(AfxGetStaticModuleState());
	//Initialize();
	#if true
		//static CTestPlatformDlg dlg(CWnd::FromHandle(adsw_acadMainWnd()));
		//dlg.DoModal();
	#else
		CTestPlatformDlg* pDlg = new CTestPlatformDlg;
		pDlg->Create(IDD_TEST_PLATFORM_DLG, CWnd::FromHandle(adsw_acadMainWnd()));
		pDlg->ShowWindow(SW_SHOW);
	#endif
	
}

void CTestPlatform::TestLoftModel() {

	////1.产生三维道路线
	//AcGePoint3dArray fitPoints = MockCreateRoadCenterLinePoint1();
	//CRoadCenterLine centerLine;
	//centerLine.Create(fitPoints);

	////2.拾取一组图元，从中挑出区域对象作为截面
	//acutPrintf(L"\n 请拾取截面图\n");
	//ads_name ss;
	//int rt = acedSSGet(NULL, NULL, NULL, NULL, ss);  	// 提示用户选择对象 
	//if (rt != RTNORM) return;
	//Adesk::Int32 length;
	//acedSSLength(ss, &length);
	//AcArray<AcDbObjectId> sectionRegions;
	//for (int index = 0; index < length; index++) {
	//	ads_name entityName;
	//	acedSSName(ss, index, entityName);
	//	AcDbObjectId entityID;
	//	acdbGetObjectId(entityID, entityName);
	//	AcDbEntity *pEntity = nullptr;
	//	acdbOpenAcDbEntity(pEntity, entityID, AcDb::kForWrite);
	//	if (pEntity->isKindOf(AcDbRegion::desc())) {
	//		sectionRegions.append(entityID);
	//	}
	//}
	//if (sectionRegions.length() == 0) return;

	////4.逐一生成每个区域的放样体
	//AcGePoint3dArray line = centerLine.getWholeFitPoints();
	//for (int index = 0; index < sectionRegions.length(); index++)
	//{
	//	LoftBody(sectionRegions[index],line,0);
	//}
}

tagRoadwayData CTestPlatform::MockCreateInputUserData2() {
	tagRoadwayData rd;

	tagSideSectionData ssd;
	ssd.stakeNo = 0;
	tagPavementSegment seg;
	seg.CrossSlop = 0.08;
	seg.width = 50;
	ssd.isLeft = false;
	ssd.pavementWidth.append(seg);
	tagPavementSegment pavamentStruct;
	pavamentStruct.width = 50.0;
	ssd.pavementWidth.append(pavamentStruct);
	ssd.pavamentStruct = gPredefinedPavementStructureList[0];
	tagComponentParameter cp;

	//隔离带
	cp.componentKind = component_kind_divider;
	cp.h2 = 3.16;
	cp.h3 = 1.9;
	cp.h4 = 0.6;
	cp.w2 = 2.4;
	cp.w3 = 1.4;
	ssd.componentParameters.appendMove(cp);

	//台阶
	cp.componentKind = component_kind_bench;
	cp.h2 = 0.5;
	cp.h3 = 1.5;
	cp.h4 = 2.8;
	cp.w2 = 0.3;
	cp.w3 = 6;
	ssd.componentParameters.appendMove(cp);

	//边部构造
	cp.componentKind = component_kind_margin;
	cp.h2 = 2;
	cp.h3 = 1;
	cp.h4 = 1.1;
	cp.w2 = 1.8;
	cp.w3 = 0.3;
	cp.w4 = 3;
	cp.n2 = 4.5;
	ssd.componentParameters.appendMove(cp);

	rd.sections.append(ssd);
	return rd;
}


void CTestPlatform::TestCenterLine1() {
	Initialize();
	
	SetDatabasePath(L"H:\\17.RodeDesign\\civ3d\\sourse\\RoadDesign\\TestData2\\zjsqlite19.db");
	CDataBaseOpr db;
	bool r = db.Open();
	std::string roadLineID = db.QueryRoadLineID("KZ");

	AcArray<tagFitPoint> fitPoints = db.QueryRoadLineCoordinate(roadLineID);
	AcArray<tagLineInterval> allNoModelInervals = db.QueryRoadNoModelInervals(roadLineID);
	TRACE(L"不可建模区段\n");
	for (int index = 0; index < allNoModelInervals.length(); index++) {
		TRACE(L"%4d) (%4d---%4d)\n", index + 1, allNoModelInervals[index].startStake, allNoModelInervals[index].endStake);
	}
	AcArray<tagLineInterval> ms;
	#if false
		ms = db.QueryRoadModelSegments(roadLineID);
	#else
		ms.append({ 98305,	98330 });
		ms.append({ 98335,	98410 });
		ms.append({ 98405,	98465 });
		ms.append({ 98470,	98495 });
		ms.append({ 98500,	98530 });
		ms.append({ 98535,	98555 });
		ms.append({ 98560,	98570 });
		ms.append({ 98575,	98710 });
		ms.append({ 98715,	98775 });
		ms.append({ 98780,	98800 });
		ms.append({ 98805,	98860 });
	#endif

	CRoadCenterLine centerLine;
	centerLine.SetModelSegments(ms);
	centerLine.SetNoModelIntervals(allNoModelInervals);
	centerLine.Create(fitPoints);

	AcArray<tagLineInterval> li = centerLine.GetModelIntervals();
	TRACE(L"可建模区段\n");
	for (int index = 0; index < li.length(); index++) {
		TRACE(L"%4d) (%4d---%4d)\n", index + 1, li[index].startStake, li[index].endStake);
	}
}

//测试道路中线
void CTestPlatform::TestCenterLine2() {
	Initialize();
	tagRoadwayData rd;
	ReadRoadCenterLineFromCSV(rd,"H:/17.RodeDesign/civ3d/ObjectArxPlugin/sectionData/Model_ContStakeXYZ.csv");
	if (rd.fitPoints.length() == 0) return;

	CRoadCenterLine centerLine;
	centerLine.Create(rd.fitPoints);
	acutPrintf(L"道路总长  %10.4f m\n", centerLine.GetWholeLength()); // 1000.0
	
	centerLine.AppendNoModelInterval(74147, 74191);
	centerLine.AppendNoModelInterval(74215, 74243);
	centerLine.AppendNoModelInterval(74334, 74347);
	int  startStake = 74151, endStake = 74340;
	AcArray<tagLineInterval> s = centerLine.GetModelIntervals();
	for (int index = 0; index < s.length(); index++) {
		acutPrintf(L"%4d) (%8d---%8d)\n", index + 1, s[index].startStake, s[index].endStake);
	}
}

tagRoadwayData CTestPlatform::MockCreateInputUserData3() {
	tagRoadwayData rd = MockCreateInputUserData1();

	rd.fitPoints = MockCreateRoadCenterLinePoint2();
	
	rd.sections[0].stakeNo = 74000;

	tagSideSectionData ssd = rd.sections[0];
	ssd.stakeNo = 74113;
	tagPavementSegment seg;
	seg.CrossSlop = 0.03;
	ssd.pavementWidth.append(seg);
	rd.sections.append(ssd);

	ssd = rd.sections[0];
	ssd.stakeNo = 74351;
	assert(ssd.componentParameters[0].componentKind == component_kind_divider);
	ssd.componentParameters[1].w3 = 4.17; // 1.4 ==> 3.17
	rd.sections.append(ssd);
	
	ssd = rd.sections[0];
	ssd.stakeNo = 74813;
	assert(ssd.componentParameters[1].componentKind == component_kind_bench);
	ssd.componentParameters[1].w3 = 13.37; // 6 ==> 13.37
	rd.sections.append(ssd);
	return rd;
}

void CTestPlatform::forceModifySectionsData(tagSideSectionData& ssd) {
	ssd.ResetWidthScale(0.6);
	ssd.ResetSlopScale(6);
	double w = ssd.OriginalWidth();
	double h = ssd.TotalThickness();
	AcArray<tagComponentParameter>& cps = ssd.componentParameters;
	for (int index = 0; index < cps.length(); index++) {
		tagComponentParameter& cp = cps[index];
		if (cps[index].componentKind == component_kind_divider) {
			cp.stepCount = 3;

			cp.w1 = w * 0.09;
			cp.w2 = w * 0.02;
			cp.w3 = w * 0.02;
			cp.w4 = w * 0.02;

			cp.h1 = h * 0.10;
			cp.h2 = h * 0.12;
			cp.h3 = h * 0.14;
			cp.h4 = h * 0.16;

			cp.n1 = 0.08;
			cp.n2 = 0.04;
			cp.n3 = 0.20;
			cp.horiOffset = 0;
			cp.anchorY = -h * 0.04;
		}
		else if (cps[index].componentKind == component_kind_bench) {
			cp.stepCount = 3;

			cp.w1 = w * 0.10;
			cp.w2 = w * 0.09;
			cp.w3 = w * 0.07;
			cp.w4 = w * 0.05;

			cp.h1 = h * 0.13;
			cp.h2 = h * 0.12;
			cp.h3 = h * 0.14;
			cp.h4 = h * 0.22;

			cp.n1 = 0.08;
			cp.n2 = 0.04;
			cp.n3 = 0.20;

			cp.horiOffset = -w * 0.5;
			cp.anchorY = -h * 0.18;
		}
		if (cps[index].componentKind == component_kind_margin) {
			cp.stepCount = 3;
			cp.h1 = h * 0.10;
			cp.h2 = h * 0.20;
			cp.h3 = h * 0.12;
			cp.h4 = h * 0.17;
			cp.w1 = w * 0.08;
			cp.w2 = w * 0.10;
			cp.w3 = w * 0.12;
			cp.w4 = w * 0.8;
			cp.n1 = 0.90;
			cp.n2 = 0.8;
			cp.n3 = 0.7;
			cp.n4 = 0.6;
			cp.horiOffset = -w * 0.7;
			cp.anchorY = 0.0;
		}
	}
}

void CTestPlatform::TestInsterct() {
	acutPrintf(L"请拾第一组实体\n");
	AcDbObjectIdArray IDs1 = PickEntities();
	acutPrintf(L"请拾第二组实体\n");
	AcDbObjectIdArray IDs2 = PickEntities();
	if (IDs1.isEmpty() || IDs2.isEmpty()) return;
	AcDbEntity* pEntity1 = nullptr;
	AcDbEntity* pEntity2 = nullptr;
	double v1 = 0.0;
	double v2 = 0.0;
	AcDbExtents extents1;
	AcDbExtents extents2;
	for (int i = 0; i < IDs1.length(); i++) {
		for (int j = 0; j < IDs2.length(); j++) {
			es = acdbOpenAcDbEntity(pEntity1, IDs1[i], AcDb::kForWrite);
			es = acdbOpenAcDbEntity(pEntity2, IDs2[j], AcDb::kForWrite);
			CalculateEntityAreaOrVolumeExtents(pEntity1, v1, extents1);
			CalculateEntityAreaOrVolumeExtents(pEntity2, v2, extents2);
			logInfo.Format(L"面积or体积：%d:%.2f %d:%.2f\n", i+1,v1, j+1,v2); RDTRACE
			bool flag2 = DetermineNonIntersect(extents1, extents2);
			if (flag2) {
				logInfo.Format(L"不相交体：Group1: %d vs  Group2: %d\n",i+1,j+1); RDTRACE
			}
			pEntity1->close();
			pEntity2->close();
		}
	}
}

void CTestPlatform::LookEntityXData() {
	logInfo.Format(L"没有查找附加信息\n");
	wchar_t* sectionKind[] = { L"类型未设置！",L"1.老路",L"2.新路扩建侧",L"3.新路加铺区域（内侧）",L"4.新路加铺区域（外侧）",L"5.新路加铺区域（内侧和外侧）",L"6.新老路相交部分" };
	AcDbObjectIdArray IDs = PickEntities();
	if (IDs.length() != 1) return;
	tagRegionXData xData1 = GetSectionXData(IDs[0]);
	if (xData1.stakeNo > -1) {
		if (xData1.depth == -1) {
			logInfo.Format(L"桩号：%.2f  %s  材料号：%d  %s  %s\n",
				xData1.stakeNo, xData1.name.c_str(), xData1.materialID,
				xData1.isLeft ? L"左侧" : L"右侧",
				sectionKind[xData1.sideKind]);
		}
		else {
			logInfo.Format(L"桩号：%.2f  %s  材料号：%d  %s  %s 厚度 %.2f\n",
				xData1.stakeNo, xData1.name.c_str(), xData1.materialID,
				xData1.isLeft ? L"左侧" : L"右侧",
				sectionKind[xData1.sideKind],xData1.depth);
		}
	}
	else {
		tagBodyXData xData2 = GetBodyXData(IDs[0]);
		if (xData2.startStake > -1) {
			logInfo.Format(L"桩号：[%.2f--%.2f]  %s 材料号：%d  %s %s\n", 
				xData2.startStake, xData2.endStake, 
				xData2.name.c_str(), xData2.materialID,
				xData2.isLeft ? L"左侧" : L"右侧", 
				sectionKind[xData2.sideKind]);
		}
	}
	RDTRACE
}

void CTestPlatform::LookASection() {
	Initialize();
	TemporaryMoveToCenter obj;
	theApp.SetDatabasePath(MakeTestDBName("KZ-left - 78000~78500.db"));
	char* roadName = "KZ";
	bool isNewRoad = true;
	static double stakeNo = 78035;

	tagRoadwayData	rd;
	CDataBaseOpr db;
	bool r = db.Open();
	if (!r) return;
	std::string roadLineID = db.QueryRoadLineID(roadName);
	if (isNewRoad) {
		rd.sections = db.QueryNewRoadSections(roadLineID, stakeNo, stakeNo);
	}
	else {
		rd.sections = db.QueryOldRoadSections(roadLineID, stakeNo, stakeNo);
	}
	
	if (rd.sections.isEmpty()) return;
	
	const tagSideSectionData& ssd1 = rd.sections[0];
	if (!CheckSection(ssd1)) return;
	CSideSection* pSection = new CSideSection(0, ssd1);
	pSection->Create();
}

void CTestPlatform::TestSection() {
	Initialize();
}

void CTestPlatform::TestDivider() {
	//tagSideSectionData ssd;
	//CSideSectionAtom* pSection = new CSideSectionAtom(ssd);
	//tagComponentParameter  cp;
	//cp.componentKind = component_kind_divider;
	//cp.stepCount = 4;
	//cp.w1 = 50;
	//cp.h1 = 6;
	//cp.w2 = 8;
	//cp.h2 = 6;
	//cp.w3 = 10;
	//cp.h3 = 7;
	//cp.w4 = 11;
	//cp.h4 = 8;
	//cp.n1 = 0.08;
	//cp.n2 = 0.04;
	//cp.n3 = 0.20;
	//CSectionComponent* component = new CComponentDivider(cp, pSection, false);
	//component->CreatePositiveComponet();
	//component->CreateNegaiveComponet();
}

void CTestPlatform::TestBench() {

}

void CTestPlatform::TestMargin() {
	Initialize();
}

void CTestPlatform::TestRoadway1() {
		Initialize();
		tagRoadwayData rd = MockCreateInputUserData1();
		
		//产生中心线数据
		rd.fitPoints = MockCreateRoadCenterLinePoint2();
		
		//产生一个截面
		rd.sections[0].pavamentStruct = gPredefinedPavementStructureList[2];

		tagPavementSegment seg;
		seg.CrossSlop = 0.02;
		seg.width = 24.5;
		rd.sections[0].pavementWidth.append(seg);

		tagPavementSegment pavamentStruct;
		pavamentStruct.width = 24.5;
		rd.sections[0].pavementWidth.append(pavamentStruct);

		double d = 0.0;
		for (int index = 0; index < rd.sections[0].pavamentStruct.length(); index++) {
			d += rd.sections[0].pavamentStruct[index].thickness;
		}
		tagComponentParameter& cp = rd.sections[0].componentParameters[0];
		assert(cp.componentKind == component_kind_divider);
		cp.h4 = d * 0.1;
		cp.h2 = d * 0.6;
		cp.h3 = d * 0.3;

		tagComponentParameter& cp2 = rd.sections[0].componentParameters[1];
		assert(cp2.componentKind == component_kind_bench);
		cp2.h2 = d * (1 / 6.0);
		cp2.h3 = d * (1 / 4.0);
		cp2.h4 = d * (1 / 3.0);

		tagComponentParameter& cp3 = rd.sections[0].componentParameters[2];
		assert(cp3.componentKind == component_kind_margin);
		cp3.h2 = d * 0.4;
		cp3.h3 = d * 0.12;
		cp3.h4 = d * 0.25;

		rd.startStake = 74002, rd.endStake = 74007;
		rd.allNoModelInervals.append(tagLineInterval(74004, 74005));
		
		//创建道路体
		//CreateRoadBody(rd);
}

void CTestPlatform::TestRoadway2() {
	Initialize();
}

void CTestPlatform::TestRoadway3() {

}


void CTestPlatform::TestCreateOldRoad() {
	theApp.OpenLogFile();
	theApp.SetDatabasePath(MakeTestDBName("KZ-left -74490~74600.db"));
	static double s = 74555;
	static double e = 74590;
	CreateRoadModel("K3", s, e, 0);
	//theApp.SetDatabasePath(MakeTestDBName("20201011bug.db"));
	//static double s = 98350;
	//static double e = 98400;
	//CreateRoadModel("K3", s, e, 1);
	theApp.CloseLogFile();
}

void CTestPlatform::TestCreateNewRoad() {
	//TemporaryMoveToCenter obj;
	theApp.OpenLogFile();
	//theApp.SetDatabasePath(MakeTestDBName("KZ-left -74490~74600.db"));
	SetDatabasePath(L"H:\\17.RodeDesign\\改扩建路面三维设计系统相关资料\\截面组件样图\\KZ-left -74490~74600.db");
	static double s = 74555;		 
	static double e = 74590;
	CreateRoadModel("KZ", s, e, 1);
	
	//theApp.SetDatabasePath(MakeTestDBName("20201011bug.db"));
	//static double s = 98350;
	//static double e = 98400;
	//CreateRoadModel("KY", s, e, 1);
	//theApp.SetDatabasePath(MakeTestDBName("KZ-left.db"));
	//static double s = 98820;
	//static double e = 98860;
	//theApp.SetDatabasePath(MakeTestDBName("KZ-left - 78000~78500.db"));
	//static double s = 78300;
	//static double e = 78400;
	//CreateRoadModel("KZ", s, e, 1);

	

	theApp.CloseLogFile();
}

void CTestPlatform::TestCreateExtendRoad() {
	theApp.SetDatabasePath(MakeTestDBName("KZ-left -74490~74600.db"));
	CreateExtendRoad("KZ","K3", 74555, 74590);
	//theApp.SetDatabasePath(MakeTestDBName("20201011bug.db"));
	//CreateExtendRoad("KY", "K3", 98350, 98400);
}

void CTestPlatform::TestStatisticsBodyMaterialVolume() {
	Initialize();
	theApp.SetDatabasePath(MakeTestDBName(19));
	CreateRoadModel("K3", 74200, 74700, 0);
	//StatisticsBodyMaterialVolume("K3", 74200, 74700);
}

void CTestPlatform::TestActualWidthByPickLine() {
	#if false
	int r = CalcActualWidthByPickLine(MakeTestDBName(4),"K3");
	assert(r == 0);
	#else
		//CAcModuleResourceOverride resOverride;
		////AFX_MANAGE_STATE(AfxGetStaticModuleState());
		//CPickRoadEdgeDlg	dlg;
		//dlg.DoModal();
	#endif
}

AcDbObjectId CreateTestBench(double width,double height,bool toOutter) {
	
	double b5 = width * 0.6;
	double h5 = height * 0.1;
	AcGePoint2d anchor(-b5, -h5);

	double h2 = height * 0.2;
	double h3 = height * 0.2;
	double h4 = height * 0.2;
	double b1 = width * 0.01;
	double b2 = width * 0.005;
	double b3 = width * 0.005;
	double h6 = height;
	CreateRegionTool regionTool;
	regionTool.Init();
	regionTool.AppendKeyPoint(-b5, 0); // A点
	regionTool.AppendKeyPoint(-b5, -h5 - h2); // B点
	regionTool.AppendKeyPoint(-b5 - b1, -h5 - h2); // C点
	regionTool.AppendKeyPoint(-b5 - b1, -h5 - h2 - h3); // D点
	regionTool.AppendKeyPoint(-b5 - b1 - b2, -h5 - h2 - h3); // E点
	regionTool.AppendKeyPoint(-b5 - b1 - b2, -h5 - h2 - h3 - h4); // F点
	regionTool.AppendKeyPoint(-b5 - b1 - b2 - b3, -h5 - h2 - h3 - h4); // G点
	regionTool.AppendKeyPoint(-b5 - b1 - b2 - b3, -h6); // H点
	if (toOutter) {
		regionTool.AppendKeyPoint(0, -h6);	// I点
		regionTool.AppendKeyPoint(0, 0);		// J点
	}
	else {
		regionTool.AppendKeyPoint(-width, -h6); // I'点
		regionTool.AppendKeyPoint(-width, 0);		// J'点
	}
	AcDbObjectId id = regionTool.CreateRegion();
	return id;
}

void CreateOverlaySide(double pavementWidth, int PavementStructureIndex, bool toOutter,double height) {
	
	tagSideSectionData ssd;
	
	ssd.pavamentStruct = gPredefinedPavementStructureList[PavementStructureIndex];
	tagPavementSegment pavamentStruct;
	pavamentStruct.width = pavementWidth;
	ssd.pavementWidth.append(pavamentStruct);
	CSideSection aSection(0, ssd);
	aSection.Create();

	AcDbObjectId benchID = CreateTestBench(pavementWidth, height, toOutter);

	AcArray<AcDbObjectId> sliceIDs;
	sliceIDs.append(benchID);
	SubtractRegions(aSection.GetRegions(), sliceIDs);

	RemoveEntityFromModelSpace(benchID);
	
}


void CTestPlatform::TestOverlaySection1() {
	Initialize();

	double height = 0.0;
	for (int index = 0; index < gPredefinedPavementStructureList[2].length(); index++) {
		height += gPredefinedPavementStructureList[2][index].thickness;
	}
	CreateOverlaySide(16.0, 2, true,height);

	CreateOverlaySide(16.0, 3, false,height);
}

//测试道面加铺--使用预设数据--双侧拼宽左幅
void CTestPlatform::TestOverlaySection2() {
	Initialize();
	tagSideSectionData ssd;
	ssd.isLeft = true;
	ssd.pavamentStruct = gPredefinedPavementStructureList[2];
	ssd.pavementWidth  = gPredefinedPavementWidthList[0];	//左幅路幅信息
	#if false
		ssd.isNewRoad = true;
		ssd.newRoadType = widening_type_double_widening;
		ssd.wideningType = new_road_type_direct_overlap;
		ssd.overlapArea1.layers = gPredefinedPavementStructureList[3];
	
		const double width = ssd.OriginalWidth();
		const double height = ssd.TotalThickness();
		tagComponentParameter cp;
		cp.componentKind = component_kind_bench;
		cp.stepCount = 3;
		cp.h1 = height * 0.1;
		cp.h2 = height * 0.2;
		cp.h3 = height * 0.2;
		cp.h4 = height * 0.2;
		cp.w1 = width * 0.01;
		cp.w2 = width * 0.01;
		cp.w3 = width * 0.01;
		cp.w4 = width * 0.01;
		cp.horiOffset = -width * 0.6;
		cp.anchorY = -height * 0.16;
		ssd.dividerBenchParameters.append(cp);
	#endif
	CSideSection aSection(0, ssd);
	aSection.Create();
	
	#if true
	tagRoadwayData rd;
	rd.fitPoints = MockCreateRoadCenterLinePoint2();
	ssd.stakeNo = rd.fitPoints[0].stakeNo;

	//左侧道路
	ssd.isLeft = true;
	rd.sections.append(ssd);
	int len = rd.fitPoints.length();
	rd.startStake = rd.fitPoints[(int)(len * 0.3)].stakeNo;
	rd.endStake = rd.fitPoints[(int)(len * 0.6)].stakeNo;
	//CreateRoadBody(rd);
	#endif
}

//测试道面加铺--使用预设数据--过渡拼宽-加铺同侧-左幅
void CTestPlatform::TestOverlaySection3() {
	Initialize();
	tagSideSectionData ssd;
	ssd.isLeft = true;
	ssd.pavamentStruct = gPredefinedPavementStructureList[2];
	ssd.pavementWidth = gPredefinedPavementWidthList[0];	//左幅路幅信息
#if true
	ssd.isNewRoad = true;
	ssd.isLeft = true;
	//ssd.newRoadType = widening_type_double_widening;
	//ssd.wideningType = new_road_type_direct_overlap;
	ssd.overlapArea1.layers = gPredefinedPavementStructureList[3];
	ssd.overlapArea2.layers = gPredefinedPavementStructureList[1];

	const double width = ssd.OriginalWidth();
	const double height = ssd.TotalThickness();
	tagComponentParameter cp;
	cp.componentKind = component_kind_bench;
	cp.stepCount = 3;
	cp.h1 = height * 0.1;
	cp.h2 = height * 0.2;
	cp.h3 = height * 0.2;
	cp.h4 = height * 0.2;
	cp.w1 = width * 0.01;
	cp.w2 = width * 0.01;
	cp.w3 = width * 0.01;
	cp.w4 = width * 0.01;
	cp.horiOffset = width * 0.7;
	cp.anchorY = -height * 0.01;
	cp.benchToOutward = true;
	ssd.dividerBenchParameters.append(cp);

	cp.componentKind = component_kind_bench;
	cp.stepCount = 3;
	cp.h1 = height * 0.1;
	cp.h2 = height * 0.2;
	cp.h3 = height * 0.3;
	cp.h4 = height * 0.2;
	cp.w1 = width * 0.03;
	cp.w2 = width * 0.02;
	cp.w3 = width * 0.01;
	cp.w4 = width * 0.02;
	cp.horiOffset = width * 0.3;
	cp.anchorY = -height * 0.005;
	cp.benchToOutward = false;
	ssd.dividerBenchParameters.append(cp);
#endif
	if (!CheckSection(ssd)) return;
	CSideSection aSection(0, ssd);
	aSection.Create();
}

void CTestPlatform::TestOverlaySection4() {

}

void CTestPlatform::TestOverlaySection5() {

}

//测试错切变换
void CTestPlatform::TestShearTrasfrom1() {
	#if false
		#define col  5
		#define row  4
		double w[col] = {11,15,19,27,17};
		double h[row] = {9,13,7,15};
		double n[col] = { 0.0,-0.04,-0.07,-0.09,+0.19 };
	#else
		#define col  5
		#define row  7
		double w[col] = { 0.75,	3,		14.75,2.5,	1.5 };
		double n[col] = { -0.4,-0.2,-0.2,-0.2,0.0 };
		double h[row] = { 0.4,	0.6,	0.8,	0.06,3.8,2,0.1 };
	#endif
		for (int index = 0; index < col / 2; index++) {
			swap(w[index], w[col - index - 1]);
			swap(n[index], n[col - index - 1]);
		}
		int rowMaterialID[50] = { 0 };
}

//测试错切变换
//对比变换前后的位置关系
void CTestPlatform::TestShearTrasfrom3() {
	#define col  5
	#define row  7
	double w[col] = { 0.75,	3,		14.75,2.5,	1.5 };
	double n1[col] = { 0 };
	double n2[col] = { -0.4,-0.2,-0.2,-0.2,0.0 };
	double h[row] = { 0.4,	0.6,	0.8,	0.06,3.8,2,0.1 };
	for (int index = 0; index < col / 2; index++) {
		swap(w[index], w[col - index - 1]);
		swap(n1[index], n1[col - index - 1]);
		swap(n2[index], n2[col - index - 1]);
	}
	int rowMaterialID[50] = { 0 };
//	CreateMultiRectWithSlopRegion(col, row, w, h, rowMaterialID, n1, true, section_side_kind_null); //没有发时错切时
//	CreateMultiRectWithSlopRegion(col, row, w, h, rowMaterialID, n2, true, section_side_kind_null); //发生了错切时
}

//测试矩形中带折线的错切变换，检查是否协调吻合
void CTestPlatform::TestShearTrasfrom2() {
	//1.创建一个矩形，并作-60%的坚向错切变形
	double w[1] = { 15 };
	double h[1] = { 20 };
	double n[1] = { -0.6 };
	int rowMaterialID[50] = { 0 };
//	CreateMultiRectWithSlopRegion(1, 1, w, h, rowMaterialID, n, true, section_side_kind_null);

	//2.创建一个矩形中一条折线段
	AcGePoint2d pts[4];
	pts[0] = AcGePoint2d(-w[0] * .3, 0);
	pts[1] = AcGePoint2d(-w[0] * .3, -h[0] * 0.5);
	pts[2] = AcGePoint2d(-w[0] * .7, -h[0] * 0.5);
	pts[3] = AcGePoint2d(-w[0] * .7, -h[0]);
	
	//3.对此折线段做-60%的坚向错切变形
	pts[0].y += w[0] * .3 * n[0];
	pts[1].y += w[0] * .3 * n[0];
	pts[2].y += w[0] * .7 * n[0];
	pts[3].y += w[0] * .7 * n[0];

	AcDbPolyline *pPolyline = new AcDbPolyline;
	for (int index = 0; index < 4; index++) {
		pPolyline->addVertexAt(index, pts[index]);
	}
	pPolyline->setColorIndex(2);
	AppendEntityToModelSpace(pPolyline, layer_Sections);
}


void CTestPlatform::TestBenchRegion() {
	bool isLeftOfAxis = false;
	//1.创建一个矩形
	double w[1] = { 15 };
	double h[1] = { 20 };
	double n[1] = { -0.5};
	int rowMaterialID[50] = { 0 };
//	CreateMultiRectWithSlopRegion(1, 1, w, h, rowMaterialID, n, isLeftOfAxis);
	
	//2. 创建一个台阶
	CreateBenchTool::tagParam param;
	param.width = w[0], param.height = h[0];
	param.sx = -param.width * 0.5;
	param.sy = -param.height * 0.05;

	param.h[0] = param.height * 0.18;
	param.h[1] = param.height * 0.24;
	param.h[2] = param.height * 0.29;
	param.h[3] = param.height * 0.13;

	param.w[0] = param.width * 0.05;
	param.w[1] = param.width * 0.07;
	param.w[2] = param.width * 0.09;
	param.w[3] = param.width * 0.12;
	param.kind = CreateBenchTool::bench_kind_vertial_through;
	param.count = 3;
	param.isLeft = isLeftOfAxis;
	if (param.kind == CreateBenchTool::bench_kind_horizontal_through) {
		param.extendInward = param.isLeft;
	}
	CreateBenchTool benchTool(param);
	benchTool.CreateRegion();
}

void CTestPlatform::TestPickEntity() {
	Initialize();
	acutPrintf(L"请拾取两条对象\n");
	AcDbObjectIdArray IDs = PickEntities();
	if (IDs.length() != 2) return ;
	AcDbEntity* pEntity1 = nullptr;
	AcDbEntity* pEntity2 = nullptr;
	acdbOpenAcDbEntity(pEntity1, IDs[0], AcDb::kForRead);
	acdbOpenAcDbEntity(pEntity2, IDs[1], AcDb::kForRead);
	//AcGePoint3dArray& points = * (new AcGePoint3dArray);
	AcGePoint3dArray points;
	points.removeAll();
	es = pEntity1->intersectWith(pEntity2, AcDb::Intersect::kOnBothOperands, points);
	if (es == Acad::eOk) {
		for (int index = 0; index < points.length(); index++) {
			AcDbCircle *pCircle = new AcDbCircle(points[index], AcGeVector3d(0, 0, 1), 2);
			AppendEntityToModelSpace(pCircle, layer_Sections);
		}
	}
	pEntity1->close();
	pEntity2->close();
	points.removeAll();

}

void CTestPlatform::TestSliceBody() {
	Initialize();
	//创建一个长方体
	AcDb3dSolid *pSolid = new AcDb3dSolid();
	es = pSolid->createBox(30, 40, 50);
	assert(es == Acad::eOk);
	AcDbObjectId entityID = AppendEntityToModelSpace(pSolid);
	
	//切割这个长方体
	AcDbEntity* pEnt = nullptr;
	es = acdbOpenAcDbEntity(pEnt,entityID, AcDb::kForWrite);
	AcDb3dSolid *pSolid2 = AcDb3dSolid::cast(pEnt);
	AcGePlane plane(AcGePoint3d(10,10,10), AcGeVector3d(-1,-1,-1));
	Adesk::Boolean getNegHalfToo = true;
	AcDb3dSolid* negHalfSolid = nullptr;
	//plane 平面法向方向指向的体将保留到pSolid2中，切割下的体保存在negHalfSolid中
	es = pSolid2->getSlice(plane, getNegHalfToo, negHalfSolid);
	pSolid2->close();
	//AppendEntityToModelSpace(negHalfSolid);
}

AcDbObjectId CreateTestBody(tagBodyXData info) {
	AcDb3dSolid *pSolid = new AcDb3dSolid();
	es = pSolid->createBox(1, 2, 3);
	assert(es == Acad::eOk);
	AcDbObjectId entityID = AppendEntityToModelSpace(pSolid);

	SetBodyXData(entityID, info);
	return entityID;
}

void CTestPlatform::TestSetAddtionalInfo() {
	Initialize();
	AcDbObjectId entityID = CreateTestBody({ 1,2,3,section_side_kind_old_road,false,L"abc",3.14 });
	tagBodyXData  info2 = GetBodyXData(entityID);
}

void CTestPlatform::TestRoadwayIntersect2() {
	theApp.SetDatabasePath(MakeTestDBName(16));
	int startStake = 80180;
	int endStake = 80290;

	//1.创建老路
	tagRoadwayData	oldRD;
	oldRD.isNewRoad = false;
	oldRD.startStake = startStake;
	oldRD.endStake = endStake;
	oldRD.RoadLine_Name = L"K3";
	if (!ReadRoadDataFromDB(oldRD)) return;
	theApp.AppendRoad(CRoadway::CreateFromRawData(oldRD));

	//2.创建新路
	tagRoadwayData	newRD;
	newRD.isNewRoad = true;
	newRD.startStake = startStake;
	newRD.endStake = endStake;
	newRD.RoadLine_Name = L"KZ";
	if (!ReadRoadDataFromDB(newRD)) return;
	if (!CheckRawModelData(newRD)) return;
	theApp.AppendRoad(CRoadway::CreateFromRawData(newRD));
}

//测试两条道路相交的情况
void CTestPlatform::TestRoadwayIntersect() {
	Initialize();
	gOptions.searchRange = 20;
	AcArray<AcDbObjectId> IDs1 = GetEntitiesOnLayer(acdbHostApplicationServices()->workingDatabase(), gOptions.layers[layer_old_road], AcDb3dSolid::desc());
	AcArray<AcDbObjectId> IDs2 = GetEntitiesOnLayer(acdbHostApplicationServices()->workingDatabase(), gOptions.layers[layer_new_road], AcDb3dSolid::desc());

	AcArray<AcDbObjectId> A_I_B = CRoadway::IntersectRoadwayBodies(IDs1, IDs2);

}


//增加调平层
void CTestPlatform::TestAddLevelingLayer() {
	Initialize();

	//1.创建一个路面结构
	theApp.SetDatabasePath(MakeTestDBName(14));
	tagRoadwayData	rd;
	CDataBaseOpr db;
	bool r = db.Open();
	if (!r) return;
	std::string roadLineID = db.QueryRoadLineID("KZ");
	if (roadLineID.empty()) return;
	rd.sections = db.QueryNewRoadSections(roadLineID);
	if (rd.sections.isEmpty()) return;
	tagSideSectionData& ssd1 = rd.sections[0];
	ssd1.groundLine[0] = AcGePoint3d(-12.77, -0.81, 0);
	ssd1.groundLine[1] = AcGePoint3d(0.41, -0.36, 0);

	ssd1.isNewRoad = true;
	//ssd1.newRoadType = widening_type_double_widening;
	//ssd1.wideningType = new_road_type_add_leveling_layer;
	if (!CheckSection(ssd1)) return;
	CSideSection aSection(0, ssd1);
	aSection.Create();
	AcArray<AcDbObjectId> rects = aSection.GetRegions();
}

//分台阶铣刨
void CTestPlatform::TestMillingWithStep() {

	//1.创建一个带超高的路面结构
#if true
#define	 col 5
#define	 row 7
	double w[col] = { 0.75,	3,		14.75,2.5,	1.5 };
	double n[col] = { 0,		0.2,	0.2,	0.4,	0.8 };
	double h[row] = { 0.4,	0.6,	0.8,	0.6,  1.8,	2,	1.1 };
	double totalH = 0.0;
	for (int index = 0; index < row; index++) {
		totalH += h[index];
	}
	double totalW = 0.0;
	for (int index = 0; index < col; index++) {
		totalW += w[index];
	}

	int rowMaterialID[50] = { 0 };
	const int colorIndexStart = 1;
	AcArray<AcDbObjectId> rects = CreateMultiRectWithSlopRegion(col, row, w, h, rowMaterialID, n, false, colorIndexStart, section_side_kind_null,0);
	AcArray<AcGePoint3d> groundLine;// = { ,AcGePoint3d(),AcGePoint3d() };
	groundLine.append(AcGePoint3d(-18.10, 1.86, 0));
	groundLine.append(AcGePoint3d(41.34, -6.52, 0));
	groundLine.append(AcGePoint3d());
	groundLine.append(AcGePoint3d());
#else
//...
#endif

	CSideSectionAtom section(0, false, AcArray<tagPavementSegment>(), AcArray<tagPavementLayer>(), section_side_kind_new_road_overlap_inner);
	section.SetGroundLine(groundLine);
	section.MillingWithStep(rects, totalW, totalH);
}


