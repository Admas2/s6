﻿#include "ModuleApp.h"
#include "TestPlatform.h"
#include "LayerOperation.h"
#include "global.h"
#define COMMAND_GROUP1 L"ObjectArxFunction"
#define COMMAND_GROUP2 L"RoadDesign"

#define APPEND_ARX_COMMAND(name) acedRegCmds->addCommand(COMMAND_GROUP1, L"name", L"name", ACRX_CMD_MODAL, OnCommand##name);

#define DEFINE_COMMAND_ENTRY(entry) void OnCommand##entry() {  \
																			static CTestPlatform obj;				\
																			obj.entry();										\
																		}					

DEFINE_COMMAND_ENTRY(TestLoftModel)
DEFINE_COMMAND_ENTRY(TestRoadway1)
DEFINE_COMMAND_ENTRY(TestRoadway2)
DEFINE_COMMAND_ENTRY(TestRoadway3)

DEFINE_COMMAND_ENTRY(TestCreateOldRoad)
DEFINE_COMMAND_ENTRY(TestCreateNewRoad)
DEFINE_COMMAND_ENTRY(TestCreateExtendRoad)
DEFINE_COMMAND_ENTRY(TestRoadwayDlg)
DEFINE_COMMAND_ENTRY(TestCenterLine1)
DEFINE_COMMAND_ENTRY(TestCenterLine2)
DEFINE_COMMAND_ENTRY(TestSection)
DEFINE_COMMAND_ENTRY(LookASection)
DEFINE_COMMAND_ENTRY(LookEntityXData)


DEFINE_COMMAND_ENTRY(TestRoadwayIntersect)
DEFINE_COMMAND_ENTRY(TestRoadwayIntersect2)
DEFINE_COMMAND_ENTRY(TestOverlaySection1)
DEFINE_COMMAND_ENTRY(TestOverlaySection2)
DEFINE_COMMAND_ENTRY(TestOverlaySection3)
DEFINE_COMMAND_ENTRY(TestOverlaySection4)
DEFINE_COMMAND_ENTRY(TestOverlaySection5)
DEFINE_COMMAND_ENTRY(TestShearTrasfrom1)
DEFINE_COMMAND_ENTRY(TestShearTrasfrom2)
DEFINE_COMMAND_ENTRY(TestShearTrasfrom3)
DEFINE_COMMAND_ENTRY(TestMillingWithStep)
DEFINE_COMMAND_ENTRY(TestAddLevelingLayer)
DEFINE_COMMAND_ENTRY(TestBenchRegion)
DEFINE_COMMAND_ENTRY(TestActualWidthByPickLine)
DEFINE_COMMAND_ENTRY(TestPickEntity)
DEFINE_COMMAND_ENTRY(TestDivider)
DEFINE_COMMAND_ENTRY(TestBench)
DEFINE_COMMAND_ENTRY(TestMargin)
DEFINE_COMMAND_ENTRY(TestSliceBody)
DEFINE_COMMAND_ENTRY(TestSetAddtionalInfo)
DEFINE_COMMAND_ENTRY(TestStatisticsBodyMaterialVolume)
DEFINE_COMMAND_ENTRY(TestInsterct)


CModuleApp::CModuleApp(){

}

CModuleApp::~CModuleApp() {

}
void CModuleApp::RegisterCommand() {
	acedRegCmds->addCommand(COMMAND_GROUP1, L"Hello", L"HL", ACRX_CMD_MODAL, CommandHelloWorld);
	acedRegCmds->addCommand(COMMAND_GROUP1, L"Misc", L"Misc", ACRX_CMD_MODAL, CommandMisc);
	acedRegCmds->addCommand(COMMAND_GROUP1, L"CommandTestVector", L"TV", ACRX_CMD_MODAL, CommandTestVector);
	acedRegCmds->addCommand(COMMAND_GROUP1, L"MoveUCS", L"MU", ACRX_CMD_MODAL, CommandTestMoveCurrentUCS);
	acedRegCmds->addCommand(COMMAND_GROUP1, L"Polyline", L"CP", ACRX_CMD_MODAL, CommandCreatePolyline);
	acedRegCmds->addCommand(COMMAND_GROUP1, L"CreateBox", L"CB", ACRX_CMD_MODAL, CommandCreateBox);
	acedRegCmds->addCommand(COMMAND_GROUP1, L"CreateRegion", L"CR2", ACRX_CMD_MODAL, CommandCreateRegion);
	acedRegCmds->addCommand(COMMAND_GROUP1, L"EntityCopy", L"EC", ACRX_CMD_MODAL, CommandEntityCopy);
	acedRegCmds->addCommand(COMMAND_GROUP1, L"EntityMirror", L"EM", ACRX_CMD_MODAL, CommandEntityMirror);
	acedRegCmds->addCommand(COMMAND_GROUP1, L"AddInfo", L"AI", ACRX_CMD_MODAL, CommandTestAddInfoToEnttiy);
	acedRegCmds->addCommand(COMMAND_GROUP1, L"CreateBox", L"CB", ACRX_CMD_MODAL, CommandCreateBox);
	acedRegCmds->addCommand(COMMAND_GROUP1, L"CreateSpire", L"CS", ACRX_CMD_MODAL, CommandCreateSpire);
	acedRegCmds->addCommand(COMMAND_GROUP1, L"CreateRevolveBody", L"CR3", ACRX_CMD_MODAL, CommandCreateRevolveBody);
	acedRegCmds->addCommand(COMMAND_GROUP1, L"CreateCylinder", L"CC2", ACRX_CMD_MODAL, CommandCreateCylinder);
	acedRegCmds->addCommand(COMMAND_GROUP1, L"CreateLoft", L"CreateLoft", ACRX_CMD_MODAL, CommandCreateLoft);
	acedRegCmds->addCommand(COMMAND_GROUP1, L"CreateLoft2", L"CreateLoft2", ACRX_CMD_MODAL, CommandCreateLoft2);
	acedRegCmds->addCommand(COMMAND_GROUP1, L"CreateLoft3", L"CreateLoft3", ACRX_CMD_MODAL, CommandCreateLoft3);
	acedRegCmds->addCommand(COMMAND_GROUP1, L"BooleanOperation", L"BO", ACRX_CMD_MODAL, CommandBooleanOperation);
	acedRegCmds->addCommand(COMMAND_GROUP1, L"GenerateKeyPoint", L"GP", ACRX_CMD_MODAL, CommandTestQuveryAEntity);

	acedRegCmds->addCommand(COMMAND_GROUP2, L"TestLoftModel", L"TestLoftModel", ACRX_CMD_MODAL, OnCommandTestLoftModel);
	acedRegCmds->addCommand(COMMAND_GROUP2, L"TestCenterLine1", L"TestCenterLine1", ACRX_CMD_MODAL, OnCommandTestCenterLine1);
	acedRegCmds->addCommand(COMMAND_GROUP2, L"TestCenterLine2", L"TestCenterLine2", ACRX_CMD_MODAL, OnCommandTestCenterLine2);
	acedRegCmds->addCommand(COMMAND_GROUP2, L"TestSection", L"TestSection", ACRX_CMD_MODAL, OnCommandTestSection);
	acedRegCmds->addCommand(COMMAND_GROUP2, L"LookASection", L"LookASection", ACRX_CMD_MODAL, OnCommandLookASection);
	acedRegCmds->addCommand(COMMAND_GROUP2, L"LookEntityXData", L"LookEntityXData", ACRX_CMD_MODAL, OnCommandLookEntityXData);
	acedRegCmds->addCommand(COMMAND_GROUP2, L"TestInsterct", L"TestInsterct", ACRX_CMD_MODAL, OnCommandTestInsterct);
	acedRegCmds->addCommand(COMMAND_GROUP2, L"TestRoadway1", L"TestRoadway1", ACRX_CMD_MODAL, OnCommandTestRoadway1);
	acedRegCmds->addCommand(COMMAND_GROUP2, L"TestRoadway2", L"TestRoadway2", ACRX_CMD_MODAL, OnCommandTestRoadway2);
	acedRegCmds->addCommand(COMMAND_GROUP2, L"TestRoadway3", L"TestRoadway3", ACRX_CMD_MODAL, OnCommandTestRoadway3);
	acedRegCmds->addCommand(COMMAND_GROUP2, L"TestRoadwayDlg", L"TestRoadwayDlg", ACRX_CMD_MODAL, OnCommandTestRoadwayDlg);
	acedRegCmds->addCommand(COMMAND_GROUP2, L"TestRoadwayIntersect", L"TestRoadwayIntersect", ACRX_CMD_MODAL, OnCommandTestRoadwayIntersect);
	acedRegCmds->addCommand(COMMAND_GROUP2, L"TestRoadwayIntersect2", L"TestRoadwayIntersect2", ACRX_CMD_MODAL, OnCommandTestRoadwayIntersect2);

	acedRegCmds->addCommand(COMMAND_GROUP2, L"TestCreateOldRoad", L"TestCreateOldRoad", ACRX_CMD_MODAL, OnCommandTestCreateOldRoad);
	acedRegCmds->addCommand(COMMAND_GROUP2, L"TestCreateNewRoad", L"TestCreateNewRoad", ACRX_CMD_MODAL, OnCommandTestCreateNewRoad);

	acedRegCmds->addCommand(COMMAND_GROUP2, L"TestOverlaySection1", L"TestOverlaySection1", ACRX_CMD_MODAL, OnCommandTestOverlaySection1);
	acedRegCmds->addCommand(COMMAND_GROUP2, L"TestOverlaySection2", L"TestOverlaySection2", ACRX_CMD_MODAL, OnCommandTestOverlaySection2);
	acedRegCmds->addCommand(COMMAND_GROUP2, L"TestOverlaySection3", L"TestOverlaySection3", ACRX_CMD_MODAL, OnCommandTestOverlaySection3);
	acedRegCmds->addCommand(COMMAND_GROUP2, L"TestOverlaySection4", L"TestOverlaySection4", ACRX_CMD_MODAL, OnCommandTestOverlaySection4);
	acedRegCmds->addCommand(COMMAND_GROUP2, L"TestOverlaySection5", L"TestOverlaySection5", ACRX_CMD_MODAL, OnCommandTestOverlaySection5);
	acedRegCmds->addCommand(COMMAND_GROUP2, L"TestShearTrasfrom1", L"TestShearTrasfrom1", ACRX_CMD_MODAL, OnCommandTestShearTrasfrom1);
	acedRegCmds->addCommand(COMMAND_GROUP2, L"TestShearTrasfrom2", L"TestShearTrasfrom2", ACRX_CMD_MODAL, OnCommandTestShearTrasfrom2);
	acedRegCmds->addCommand(COMMAND_GROUP2, L"TestShearTrasfrom3", L"TestShearTrasfrom3", ACRX_CMD_MODAL, OnCommandTestShearTrasfrom3);
	acedRegCmds->addCommand(COMMAND_GROUP2, L"TestMillingWithStep", L"TestMillingWithStep", ACRX_CMD_MODAL, OnCommandTestMillingWithStep);
	acedRegCmds->addCommand(COMMAND_GROUP2, L"TestAddLevelingLayer", L"TestAddLevelingLayer", ACRX_CMD_MODAL, OnCommandTestAddLevelingLayer);

	acedRegCmds->addCommand(COMMAND_GROUP2, L"TestBenchRegion", L"TestBenchRegion", ACRX_CMD_MODAL, OnCommandTestBenchRegion);
	acedRegCmds->addCommand(COMMAND_GROUP2, L"TestActualWidthByPickLine", L"TestActualWidthByPickLine", ACRX_CMD_MODAL, OnCommandTestActualWidthByPickLine);
	acedRegCmds->addCommand(COMMAND_GROUP2, L"TestPickEntity", L"TestPickEntity", ACRX_CMD_MODAL, OnCommandTestPickEntity);
	acedRegCmds->addCommand(COMMAND_GROUP2, L"TestBench", L"TestBench", ACRX_CMD_MODAL, OnCommandTestBench);
	acedRegCmds->addCommand(COMMAND_GROUP2, L"TestMargin", L"TestMargin", ACRX_CMD_MODAL, OnCommandTestMargin);
	acedRegCmds->addCommand(COMMAND_GROUP2, L"TestDivider", L"TestDivider", ACRX_CMD_MODAL, OnCommandTestDivider);
	acedRegCmds->addCommand(COMMAND_GROUP2, L"TestSliceBody", L"TestSliceBody", ACRX_CMD_MODAL, OnCommandTestSliceBody);
	acedRegCmds->addCommand(COMMAND_GROUP2, L"TestSetAddtionalInfo", L"TestSetAddtionalInfo", ACRX_CMD_MODAL, OnCommandTestSetAddtionalInfo);
	acedRegCmds->addCommand(COMMAND_GROUP2, L"TestStatisticsBodyMaterialVolume", L"TestStatisticsBodyMaterialVolume", ACRX_CMD_MODAL, OnCommandTestStatisticsBodyMaterialVolume);
	acedRegCmds->addCommand(COMMAND_GROUP2, L"TestCreateExtendRoad", L"TestCreateExtendRoad", ACRX_CMD_MODAL, OnCommandTestCreateExtendRoad);

}

void CModuleApp::InitModule(void* pkt) {
	acrxDynamicLinker->unlockApplication(pkt);
	acrxRegisterAppMDIAware(pkt);

	RegisterCommand();
	
	gOptions.appName = L"RoadeDesign";
	
	OnDocumentCreated(acdbHostApplicationServices()->workingDatabase());
	
	RDEventReactor::rxInit();
	RDDatabaseReactor::rxInit();
	RDDocumentReactor::rxInit();
	RDEditorReactor::rxInit();
	m_eventReactor = new RDEventReactor;
	m_dbReactor = new RDDatabaseReactor;
	m_docReactor = new RDDocumentReactor;
	m_edReactor = new RDEditorReactor;
	auto dbPtrs = acdbActiveDatabaseArray();
	for (auto&& pDb : dbPtrs) {
		pDb->addReactor(m_dbReactor);
	}
}

void CModuleApp::CloseLogFile() {
	if (isFileOpen_) {
		logFile_.Flush();
		logFile_.Close();
		isFileOpen_ = false;
	}
}
FILE* gFile = nullptr;
void CModuleApp::OpenLogFile() {
	isFileOpen_ = logFile_.Open(L"H:\\17.RodeDesign\\civ3d\\sourse\\RoadDesign\\TestData2\\modelLog.txt", CFile::modeCreate | CFile::modeWrite);
}

void CModuleApp::AppendInfoToLogFile() {
	if (isFileOpen_) {
		//logFile_.WriteString(logInfo.GetString());
		logFile_.Write(logInfo, logInfo.GetLength());
	}																														 
}

void CModuleApp::CreatePresetLayer(AcDbDatabase* pDbDatabase) {
	//注意与：enum eLayerID 要一一对应
	gOptions.layers.removeAll();
	gOptions.layers.append(L"1.道路中线");
	gOptions.layers.append(L"2.截面");
	gOptions.layers.append(L"3.老路");
	gOptions.layers.append(L"4.新路");
	gOptions.layers.append(L"5.扩建路");
	gOptions.layers.append(L"6.新路∩老路");
	gOptions.layers.append(L"7.其它");
	for (int index = 0; index < gOptions.layers.length(); index++) {
		CreateLayer(pDbDatabase,gOptions.layers[index]);
	}
}

void CModuleApp::UninitModlue() {
	acedRegCmds->removeGroup(COMMAND_GROUP1);
	acedRegCmds->removeGroup(COMMAND_GROUP2);

	deleteAcRxClass(RDEventReactor::desc());
	deleteAcRxClass(RDDatabaseReactor::desc());
	deleteAcRxClass(RDDocumentReactor::desc());
	deleteAcRxClass(RDEditorReactor::desc());
	delete 	m_eventReactor;

	auto dbPtrs = acdbActiveDatabaseArray();
	for (auto&& pDb : dbPtrs) {
		pDb->removeReactor(m_dbReactor);
		OnDocumentToBeDestroyed(pDb);
	}
	delete 	m_dbReactor;
	delete  m_docReactor;
	delete m_edReactor;
}

void CModuleApp::AppendRoad(CRoadway* road) {
	//1.向已经项添加
	AcArray<CRoadway*>& ary = FindRoadways(acdbHostApplicationServices()->workingDatabase());
	for (int index = 0; index < ary.length(); index++) {
		if (ary[index]->name() == road->name()) {
			delete ary[index];
			ary[index] = road;
			return;
		}
	}

	//2.新增一项
	ary.append(road);
}

CRoadway* CModuleApp::operator[](int road_index) {
	for (int index = 0; index < allRoads_.length(); index++) {
		if (allRoads_[index].pDbDatabase == acdbHostApplicationServices()->workingDatabase()) {
			if (0 <= road_index && road_index < allRoads_[index].roads_.length()) {
				return allRoads_[index].roads_[road_index];
			}
		}
	}

	return nullptr;
}
AcArray<CRoadway*>& CModuleApp::FindRoadways(AcDbDatabase* pDb) {
	for (int index = 0; index < allRoads_.length(); index++) {
		if (allRoads_[index].pDbDatabase == pDb) {
			return 	allRoads_[index].roads_;
		}
	}

	static AcArray<CRoadway*> nullary;
	return nullary;
}

CRoadway* CModuleApp::operator[](std::wstring roadName) {
	AcArray<CRoadway*>& roads = FindRoadways(acdbHostApplicationServices()->workingDatabase());
	for (int index = 0; index < roads.length(); index++) {
		if (roads[index]->name() == roadName) {
			return roads[index];
		}
	}
	return nullptr;
}

void CModuleApp::OnDocumentCreated(AcDbDatabase* pDbDatabase) {

	acdbRegApp(gOptions.appName);

	CreatePresetLayer(pDbDatabase);

	tagRoadModel item;
	item.pDbDatabase = pDbDatabase;
	allRoads_.append(item);

	//2.
	//ACHAR* layerName = nullptr;
	//AcArray<AcDbObjectId> region = GetEntitiesOnLayer(pDbDatabase, layerName, AcDbRegion::desc());
	//AcArray<AcDbObjectId> bodies = GetEntitiesOnLayer(pDbDatabase, layerName, AcDb3dSolid::desc());
}

void CModuleApp::OnDocumentToBeDestroyed(AcDbDatabase* pDbDatabase) {
	AcArray<CRoadway*>& ary = FindRoadways(pDbDatabase);
	for (int j = 0; j < ary.length(); j++) {
		if (ary[j] != nullptr) {
			delete ary[j];
		}
	}
	ary.removeAll();
}

AcGePoint3dArray& CModuleApp::BorrowGlobalPt3dAry() {
	if (pt3DAry_.isEmpty()) {
		pt3DAry_.append(new AcGePoint3dArray(50,50));
	}
	AcGePoint3dArray* r = pt3DAry_.last();
	pt3DAry_.removeLast();
	r->removeAll();
	return *r;
}

void CModuleApp::ReturnGlobalPt3dAry(AcGePoint3dArray* ptr) {
	pt3DAry_.append(ptr);
}

