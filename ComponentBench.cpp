﻿#include "pch.h"
#include "ComponentBench.h"
#include "SideSection.h"
#include "CreateRegionTool.h"
#include "CreateBenchTool.h"
#include "BooleanOperation.h"
#include "EntityXData.h"

ComponentBench::ComponentBench(const tagComponentParameter& cp, CSideSectionAtom* owner_section,bool isLeft):
	CSectionComponent(cp, owner_section, isLeft){
	assert(cp.componentKind == component_kind_bench);
	assert(cp.stepCount > 0);
	colorIndex_ = 3; //Green
}

ComponentBench::~ComponentBench() {

}
AcDbObjectId ComponentBench::CreatePositiveComponet() {
	CreateBenchTool::tagParam param;
	// 注意： 这里用更大的宽度作面域，以将可能存在的斜坡包括进来，再通过求交将多余部分去掉
	param.width  = owner_section_->OriginalWidth() * 2;
	param.height = owner_section_->Totalthickness();
	if (isLeft_) {
		param.sx = -cp_.horiOffset;
	}
	else {
		param.sx = cp_.horiOffset;
	}
	param.sy = cp_.anchorY;
	param.h[0] = cp_.h1;
	param.h[1] = cp_.h2;
	param.h[2] = cp_.h3;
	param.h[3] = cp_.h4;
	param.w[0] = cp_.w1;
	param.w[1] = cp_.w2;
	param.w[2] = cp_.w3;
	param.w[3] = cp_.w4;
	param.count = cp_.stepCount;
	param.kind = CreateBenchTool::bench_kind_horizontal_through;
	param.stepIsLeft = param.extendInward = param.isLeft = isLeft_;
	param.pw.append(owner_section_->GetPavementWidth());
	param.colorIndex = colorIndex_;

	CreateBenchTool benchTool(param);
	AcDbObjectId entId = benchTool.CreateRegion();

	//通过与路面结构求交，去掉多余部分
	AcArray<AcDbObjectId> srcIDs = owner_section_->GetSideRoadStructRegions();
	IntersectRegions(entId, srcIDs);
	
	SetRegionXData(entId, tagRegionXData{ cp_.materialID,isLeft_,colorIndex_,owner_section_->GetSideKind(),cp_.Name,owner_section_->stakeNo(),-1});

	return entId;
}