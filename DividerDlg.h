#pragma once


// CDividerDlg dialog

class CDividerDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CDividerDlg)

public:
	CDividerDlg(tagComponentParameter cp,CWnd* pParent = nullptr);   // standard constructor
	virtual ~CDividerDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIVIDER_DLG };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	tagComponentParameter m_cp;
	afx_msg void OnBnClickedOk();
};
