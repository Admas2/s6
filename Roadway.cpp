﻿#include "pch.h"
#include "Roadway.h"
#include <actrans.h>
#include "global.h"
#include "EntityXData.h"
#include "BooleanOperation.h"
#include "DataBaseOpr.h"
#include "LayerOperation.h"

CRoadway::CRoadway() {
}

CRoadway::~CRoadway() {

	delete centerLine_;

	for (int index = 0; index < sections_.length(); index++) {
		delete sections_[index];
	}
	sections_.removeAll();

	RemoveEntityFromModelSpace(solidBodies_);
	solidBodies_.removeAll();

	RemoveEntityFromModelSpace(solidIntersectBodies_);
	solidIntersectBodies_.removeAll();

	RemoveEntityFromModelSpace(sectionRegions_);
	sectionRegions_.removeAll();
}

void CRoadway::SetCenterLine(CRoadCenterLine* centerLine) {
	centerLine_ = centerLine;
}

void CRoadway::SetCorrectStakes(const AcArray<tagCorrectStake>& correctStakes) {
	correctStakes_.removeAll();
	correctStakes_.append(correctStakes);
}
;
void CRoadway::AppendSection(CSideSection* section) {
	sections_.append(section);
}

static AcDbObjectId FindNearistSectionComponet(const AcArray<AcDbObjectId>& componets, double stakeNo) {
	assert(componets.length() > 0);
	int pos = -1;
	for (int index = 0; index < componets.length(); index++) {
		tagRegionXData xData = GetSectionXData(componets[index]);
		if (xData.stakeNo > stakeNo) {
			pos = index - 1;
			break;
		}
	}
	if (pos == -1) {
		pos = componets.length() - 1;
	}
	return componets[pos];
}

void CRoadway::Create3DSolid() {
	//1. 纵向分段
	AcArray<AcArray<CSideSection*>> segmentSectionGroup;
	DivideToRoadSegement(segmentSectionGroup);

	//2. 横向编组：按截面组件编组
	AcArray<AcArray<AcDbObjectId>> componentsGroup;
	for (int index = 0; index < segmentSectionGroup.length(); index++) {
		DivideToComponentsGroup(segmentSectionGroup[index], componentsGroup);
	}

	//3. 放样造型
	if (!componentsGroup.isEmpty()) {
		LoftComponentsGroup(componentsGroup);
	}
	else {
		logInfo.Format(L"没有道路体放样!\n"); RDTRACE
	}
	
}

static void DivideToComponentsGroupByStep(AcArray<AcArray<AcDbObjectId>>& componentsGroup, const AcArray<CSideSection*>& sections) {
	if (sections.isEmpty()) return;
	if (sections.length() == 1) {
		logInfo.Format(L"此路段只有一个截面，不能放样\n");  RDTRACE
			return;  //不足两个截面的不能放样成体
	}
	const int component_count = sections[0]->GetRegions().length();
	for (int component_index = 0; component_index < component_count; component_index++)
	{
		tagRegionXData xData = GetSectionXData(sections[0]->GetRegionByIndex(component_index));
		if (xData.stakeNo == -1) {
			logInfo.Format(L"查找面域失败 %.2f\n", sections[0]->stake_no()); RDTRACE
			continue;
		}

		//1 将所有截面中的所有第componet_index号组件都选出来编成一组：componets
		AcArray<AcDbObjectId> components;
		for (CSideSection* section : sections)
		{
			AcDbObjectId regionID = section->GetRegionByXData(xData);
			if (regionID.isValid()) {
				components.append(regionID);
			}
			else {
				logInfo.Format(L"组件面域查找失败 桩号: %.2f  %s  %s\n", section->stake_no(),section->isLeft()?L"左侧":L"右侧",xData.name.c_str());
				RDTRACE
			}
		}

		//2.
		componentsGroup.append(components);
	}
}

static void DivideToComponentsGroupMultiStep(AcArray<AcArray<AcDbObjectId>>& componentsGroup, const AcArray<CSideSection*>& sections) {
	if (sections.isEmpty()) return;
	if (sections.length() == 1) {
		logInfo.Format(L"此路段只有一个截面，不能放样\n");  RDTRACE
		return;  //不足两个截面的不能放样成体
	}

	const int component_count = sections[0]->GetRegions().length();
	for (int component_index = 0; component_index < component_count; component_index++)
	{
		tagRegionXData xData = GetSectionXData(sections[0]->GetRegionByIndex(component_index));
		if (xData.stakeNo == -1) {
			logInfo.Format(L"查找面域失败 %.2f\n", sections[0]->stake_no()); RDTRACE
				continue;
		}

		AcArray<AcDbObjectId> components;
		int start_section_index = 0;
		for (int section_index = 0; section_index < sections.length(); section_index++)
		{
			AcDbObjectId regionID = sections[section_index]->GetRegionByXData(xData);
			if (regionID.isValid()) {
				components.append(regionID);
				double dist = sections[section_index]->stake_no() - sections[start_section_index]->stake_no();
				if (dist > gOptions.maxModelLength) {
					componentsGroup.append(components);
					components.removeAll();
					components.append(regionID);
					start_section_index = section_index;
				}
			}
			else {
				logInfo.Format(L"组件面域查找失败 桩号: %.2f  %s  %s\n", 
					sections[section_index]->stake_no(), 
					sections[section_index]->isLeft() ? L"左侧" : L"右侧", 
					xData.name.c_str());
				RDTRACE
			}
		}

		if (components.length() > 1) {
			componentsGroup.append(components);
		}
	}
}

void CRoadway::DivideToComponentsGroup(const AcArray<CSideSection*>& sections,AcArray<AcArray<AcDbObjectId>>& componentsGroup) {
	//1.分类出左、右两截面	
	AcArray<CSideSection*> leftSections;
	AcArray<CSideSection*> rightSections;
	for (int index = 0; index < sections.length(); index++) {
		if (sections[index]->isLeft()) {
			leftSections.append(sections[index]);
		}
		else {
			rightSections.append(sections[index]);
		}
	}

	//2.对各截面进行分组
	if (gOptions.isStepModel) {
		::DivideToComponentsGroupByStep(componentsGroup, leftSections);
		::DivideToComponentsGroupByStep(componentsGroup, rightSections);
	}
	else {
		::DivideToComponentsGroupMultiStep(componentsGroup, leftSections);
		::DivideToComponentsGroupMultiStep(componentsGroup, rightSections);
	}
}

//纵向分段
void CRoadway::DivideToRoadSegement(AcArray<AcArray<CSideSection*>>& segmentSectionGroup) {
	
	//1.提取建模区段
	AcArray<tagLineInterval> m = centerLine_->GetModelIntervals();
	if (m.length() == 0) {
		return ;
	}

	//2.按区段分类截面
	AcArray<CSideSection*> sections;
	for (int index = 0; index < m.length(); index++) {
		sections.removeAll();
		for (int j = 0; j < sections_.length(); j++) {
			CSideSection* ss = sections_[j];
			if (m[index].startStake <= ss->stake_no() && ss->stake_no() <= m[index].endStake) {
					sections.append(ss);
			}
		}

		if ( sections.length() == 2 && 
			   sections[0]->isLeft() == !sections[1]->isLeft() && 
			   sections[0]->stake_no() == sections[1]->stake_no() ) {
				logInfo.Format(L"桩号：%.2f的左右两个截面，只在一个区段中，将不能被建模型\n", sections[0]->stake_no());
				RDTRACE
				continue;
			}

		if (sections.length() == 1) {
			logInfo.Format(L"桩号：%.2f的截面，只在一个区段中，将不能被建模型\n", sections[0]->stake_no());
			RDTRACE
			continue;
		}

		segmentSectionGroup.append(sections);
		//logInfo.Format(L"划分区间段2：%.2f -- %.2f\n", sections[0]->stake_no(), sections[sections.length() - 2]->stake_no()); RDTRACE
	}
}

//按每两个相邻截面逐步建模
bool CRoadway::GraduallyStepModelBody(const AcArray<AcDbObjectId>& componets, int componentIndex,int totalNum, AcArray<AcDbEntity*>& sectionRegions) {
	bool result = true;
	AcArray<AcDbObjectId> componets2;
	for (int index = 0; index < componets.length() - 1; index++) {
		componets2.removeAll();
		componets2.append(componets[index]);
		componets2.append(componets[index+1]);
		AcDbObjectId bodyID;
		bool r = LoftBody(componets2, componentIndex, totalNum, true, sectionRegions,bodyID);

		if (bodyID.isValid()) {
			solidBodies_.append(bodyID);
		}
		else {
			//其中只要有一对建模失败，那么就认定这一样建模失败
			result = false;
			break;
		}
	}
	return result;
}

//针对建模结果输出提示信息供调试
void CRoadway::ShowInfoForModelResult(const AcArray<AcDbObjectId>& componets, int componentIndex, int totalNum, bool isSuccess, AcArray<AcDbEntity*>& sectionRegions) {
	tagRegionXData xDataFirst = GetSectionXData(componets.first());
	tagRegionXData xDataLast = GetSectionXData(componets.last());
	if (isSuccess) {
		logInfo.Format(L"创建体成功 %d / %d  [%.2f--%.2f] 组件: %s  %s\n",
			componentIndex + 1, totalNum,
			xDataFirst.stakeNo, xDataLast.stakeNo,
			xDataFirst.name.c_str(),
			xDataFirst.isLeft ? L"左侧" : L"右侧");
		RDTRACE
	}else{
		//放样失败的处理
		logInfo.Format(L"创建体失败 %d / %d  [%.2f--%.2f] 组件: %s  %s\n",
			componentIndex + 1, totalNum,
			xDataFirst.stakeNo, xDataLast.stakeNo,
			xDataFirst.name.c_str(),
			xDataFirst.isLeft ? L"左侧" : L"右侧");
		RDTRACE

			//将截面放到单独层layer_Other中，以供调试观察
			logInfo.Format(L"放样失败 有%5d个截面\n", componets.length()); RDTRACE
			for (AcDbObjectId item : componets) {
				tagRegionXData xData = GetSectionXData(item);
				logInfo.Format(L"无法放样的组件面域: %.2f, %s\n", xData.stakeNo, xData.name.c_str()); RDTRACE
			}
		AcDbEntity* pEntity = nullptr;
		for (int index = 0; index < sectionRegions.length(); index++) {
			tagRegionXData xData = GetSectionXData(sectionRegions[index]->id());
			es = acdbOpenAcDbEntity(pEntity, sectionRegions[index]->id(), AcDb::kForWrite);
			AcDbRegion* pReg = AcDbRegion::cast(pEntity);
			double area = 0.0;
			pReg->getArea(area);
			if (!pReg->isNull() && area > 0.0) {
				pEntity->setLayer(gOptions.layers[layer_Other]);
			}
			pEntity->close();
		}
	}
}

void CRoadway::LoftComponentsGroup(AcArray<AcArray<AcDbObjectId>>& componentsGroup) {

	int body_count = 0;
	const int num = componentsGroup.length();
	CString info;
	info.Format(L"放样道路 %s [%.2f -- %.2f] 需建 %d 个体\n",roadName_.c_str(), centerLine_->GetFirstStakeNo(), centerLine_->GetLastStakeNo(), num);
	AcArray<AcDbEntity*> sectionRegions;
	acedSetStatusBarProgressMeter(info.GetString(), 0, num);
	for (int index = 0; index < num; index++) {
		bool result = false;
		if (gOptions.isStepModel) {
			result = GraduallyStepModelBody(componentsGroup[index], index, num, sectionRegions);
		}
		else {
			//首次多截面整体放样
			AcDbObjectId bodyID;
			result = LoftBody(componentsGroup[index], index, num, false, sectionRegions, bodyID);
			if (result) {
				if (bodyID.isValid()) {
					solidBodies_.append(bodyID);
				}
			}
			else {
				//如果 整体放样失败就尝试逐截面，带导向线建模
				result = GraduallyStepModelBody(componentsGroup[index], index, num, sectionRegions);
			}
		}
		
		//显示提示信息
		ShowInfoForModelResult(componentsGroup[index], index, num,result, sectionRegions);

		acedSetStatusBarProgressMeterPos(index);
	}
	acedRestoreStatusBar();
}

void CRoadway::RemoveSectionObject() {
	for (int index = 0; index < sections_.length(); index++) {
		sections_[index]->RemoveEntities();
	}
	sections_.removeAll();
}

void CRoadway::RemoveAdiedEntities() {
	RemoveSectionObject();

	RemoveEntityFromModelSpace(sectionRegions_);
	sectionRegions_.removeAll();

	centerLine_->RemoveEntities();
}

void CRoadway::CorrectStake(double& stakeNo) {
	for (int index = 0; index < correctStakes_.length(); index++) {
		if (correctStakes_[index].idealStakeNo == stakeNo) {
			assert(correctStakes_[index].actualStakeNo != -1.0);
			//logInfo.Format(L"调整 桩号%f为截面的位置,以防止截面突变处出现道路间断\n", stakeNo);  RDTRACE;
			stakeNo = correctStakes_[index].actualStakeNo;
			
			return ;
		}
	}
}

AcDbRegion* CRoadway::CreateComponentRegion(AcDbObjectId regionID) {
	//1.查找截面信息
	tagRegionXData xdata = GetSectionXData(regionID);
	CorrectStake(xdata.stakeNo);	//修正一下桩号，以避免在路段连接处出现一个步长的空隙
	if (xdata.stakeNo == -1) {
		logInfo.Format(L"查找截面信息失败\n");
		RDTRACE
		return nullptr;
	}
	
	//2.查询原始截面
	AcDbEntity *pEntity = nullptr;
	es = acdbOpenAcDbEntity(pEntity, regionID, AcDb::kForWrite);
	if (es != Acad::eOk) return nullptr;
	AcDbRegion* pRefRegion = AcDbRegion::cast(pEntity);

	//3.以原始截面为依据在指定位置上创建一个新截面
	AcGeVector3d vecTagent = centerLine_->GetTagentDir(xdata.stakeNo);
	AcGeVector3d vecSectionXAixs = -vecTagent.crossProduct(AcGeVector3d(0, 0, 1));
	vecSectionXAixs.normalize();
	vecTagent.normalize();
	AcDbRegion* pRegion = CreateRegionAtDirectionPosition(centerLine_->GetPoint(xdata.stakeNo), vecTagent, vecSectionXAixs, pRefRegion);
	AcDbObjectId id = AppendEntityToModelSpace(pRegion, layer_Sections);
	sectionRegions_.append(id);
	pRefRegion->close();
	return pRegion;
}

eLayerID CRoadway::GetBodyLayer() {
	if (roadwayKind_ == roadway_kind_old) return layer_old_road;
	if (roadwayKind_ == roadway_kind_new) return layer_new_road;
	if (roadwayKind_ == roadway_kind_extend) return layer_extend_road;
	return layer_Other;
}

bool CRoadway::LoftBody(const AcArray<AcDbObjectId>& componets,
											   int componentIndex,
	                       int totalNum, 
	                       bool useGuideLine, 
												 AcArray<AcDbEntity*>& sectionRegions,  //输出参数：引用的截面面域
	                       AcDbObjectId& result) {                //输出参数：创建体的ID 
	
	//1.
	assert(componets.length() > 1);

	//2.收集放样截面
	sectionRegions.removeAll();
	for (int index = 0; index < componets.length(); index++) {
		AcDbRegion * pRegion = CreateComponentRegion(componets[index]);
		if (pRegion) {
			sectionRegions.append(pRegion);
		}
	}
	if (sectionRegions.isEmpty()) {
		logInfo.Format(L"放样失败  没有合适的截面\n"); RDTRACE
		return false;
	}

	//3.计算可能需要导向线或者路径线
	AcArray<AcDbEntity*> guideCurves;
	AcDbEntity* pPathCurve = nullptr;
	if (useGuideLine) {
		CalculateGuideLinesOf2Region(sectionRegions.first(), sectionRegions.last(), guideCurves);
		if (guideCurves.isEmpty()) return false;
	}else
	{
		acdbOpenAcDbEntity(pPathCurve, centerLine_->GetCenterLineEntityID(), AcDb::kForRead);
	}

	//5.将收集的截面集放样成三维体
	AcDbLoftOptions loftOptions;
	AcDb3dSolid *pSolid = new AcDb3dSolid();
	es = pSolid->createLoftedSolid(sectionRegions, guideCurves, pPathCurve, loftOptions);
	
	//6.如果放样失败，且只是两个截面，并且设置了导向线，就尝试使用 |extrudeAlongPath|进行“挤出造型”
	if (es != Acad::eOk && componets.length() == 2 && guideCurves.length() > 0) {
			AcDbRegion * pRegion = AcDbRegion::cast(sectionRegions.first());
			es = pSolid->extrudeAlongPath(pRegion, AcDbCurve::cast(guideCurves.first()));
	}
	
	//7.如果失败，就清理后退出
	if (es != Acad::eOk) {
		for (AcDbEntity* pEnt : guideCurves) {
			delete pEnt;
		}
		guideCurves.removeAll();
		if (pPathCurve) {
			pPathCurve->close();
		}
		delete pSolid;
		return false;
	}

	//8.设置材料颜色
	tagRegionXData section_info = GetSectionXData(componets[0]);
	pSolid->setColorIndex(section_info.colorIndex); //三维体采用与每个截面相同的颜色

	//9
	AcDbObjectId bodyID = AppendEntityToModelSpace(pSolid, GetBodyLayer());

	//10.设置附加信息
	int num = componets.length();
	tagRegionXData xData1 = GetSectionXData(componets[0]);
	tagRegionXData xData2 = GetSectionXData(componets[num - 1]);
	
	SetBodyXData(bodyID, 
		tagBodyXData{ xData1.stakeNo ,
								  xData2.stakeNo,
									section_info.materialID,
									section_info.sideKind,
									section_info.isLeft,
									section_info.name,-1});
	result = bodyID;

	//11.清理
	for (AcDbEntity* pEnt : guideCurves) {
		delete pEnt;
	}
	guideCurves.removeAll();
	if (pPathCurve) {
		pPathCurve->close();
	}

	return true;
}

AcArray<AcDbObjectId>  CRoadway::GetBodies() {
	return solidBodies_;
}

void CRoadway::AppendBodies(AcArray<AcDbObjectId> bodies) {
	solidBodies_.append(bodies);
}

void CRoadway::AppendIntersectBodies(AcArray<AcDbObjectId> bodies) {
	solidIntersectBodies_.append(bodies);
}

//判断两个截面的拓朴结构是否相同
bool isSameSectionTopology(const tagSideSectionData& s1, const tagSideSectionData& s2) {
	if (s1.pavementWidth.length() != s2.pavementWidth.length()) return false;
	if (s1.pavamentStruct.length() != s2.pavamentStruct.length()) return false;
	if (s1.componentParameters.length() != s2.componentParameters.length()) return false;
	//要求两个截面所含的组件类型要相同
	for (int index = 0; index < s1.componentParameters.length(); index++) {
		int j = 0;
		for (; j < s2.componentParameters.length(); j++) {
			if (s1.componentParameters[index].componentKind == s2.componentParameters[j].componentKind) break;
		}
		if (j == s2.componentParameters.length()) return false;
	}
	if (s1.isNewRoad		!= s2.isNewRoad)	  return false;
	if (s1.newRoadType1	!= s2.newRoadType1)	return false;
	if (s1.newRoadType2 != s2.newRoadType2)	return false;
	if (s1.wideningType != s2.wideningType) return false;
	if (s1.overlapSame	!= s2.overlapSame)	return false;
	if (s1.overlapArea1.layers.length() != s2.overlapArea1.layers.length()) return false;
	if (s1.overlapArea2.layers.length() != s2.overlapArea2.layers.length()) return false;
	return true;
}

void AppendLineInterval(AcArray<tagLineInterval>& intervals,double s,double e) {
	tagLineInterval interval(s,e);
	if (interval.length() > 0) {
		intervals.append(interval);
		//logInfo.Format(L"道路区段 桩号:%.2f --- %.2f\n",interval.startStake, interval.endStake); RDTRACE;
	}
	else {
		logInfo.Format(L"某一路段中，只有桩号%.2f的截面，将不建模\n", s); RDTRACE
	}
	return;
}

AcArray<tagLineInterval> CreateDivideSegement(const tagRoadwayData& rd,AcArray<tagCorrectStake>& correctStakes) {
	
	//取出单侧作的所有截面
	AcArray<tagSideSectionData> leftAry;
	AcArray<tagSideSectionData> rightAry;
	for (tagSideSectionData item : rd.sections) {
		if (item.isLeft) {
			leftAry.append(item);
		}
		else {
			rightAry.append(item);
		}
	}

	AcArray<tagLineInterval> result;
	AcArray<tagSideSectionData> sdAry;
	if (!leftAry.isEmpty()) {
		sdAry.append(leftAry);

	}	else if (!rightAry.isEmpty()) {
		sdAry.append(rightAry);
	}
	else {
		return result;
	}

	//找出截面拓朴结构结构发生变化的位置
	AcArray<int> ptIndexes;
	for (int index = 0; index < sdAry.length() - 1; index++) {
		if (!isSameSectionTopology(sdAry[index], sdAry[index + 1])) {
			ptIndexes.append(index);
			logInfo.Format(L"桩号:%.2f的截面与下一个截面结构不同\n", sdAry[index].stakeNo); RDTRACE;
		}
	}
	
	//划分出区间段
	correctStakes.removeAll();
	
	if (ptIndexes.length() > 0) {
		int start = 0;
		int end = 0;
		for (int index = 0; index < ptIndexes.length(); index++) {
			end = ptIndexes[index];
			AppendLineInterval(result, sdAry[start].stakeNo, sdAry[end].stakeNo);
			correctStakes.append(tagCorrectStake{ sdAry[end].stakeNo,sdAry[end+1].stakeNo });
			start = end + 1;
		}
		end = sdAry.length() - 1;
		AppendLineInterval(result, sdAry[start].stakeNo, sdAry[end].stakeNo);
	}

	return result;
}

CRoadway* CRoadway::CreateFromRawData(const tagRoadwayData& rd) {

	//1.创建中线
	CRoadCenterLine* centerLine = new CRoadCenterLine(rd.roadLineID);
	centerLine->SetNoModelIntervals(rd.allNoModelInervals);	//读取不建模型区间
	AcArray<tagCorrectStake> correctStakes;
	AcArray<tagLineInterval> modelSegments = CreateDivideSegement(rd, correctStakes);
	centerLine->SetModelSegments(modelSegments);

	centerLine->Create(rd.fitPoints);
	
	//2.
	CRoadway* pRoayway = new CRoadway();
	pRoayway->roadName_ = rd.RoadLine_Name;
	if (rd.isNewRoad) {
		pRoayway->roadwayKind_ = roadway_kind_new;
	}
	else {
		pRoayway->roadwayKind_ = roadway_kind_old;
	}
	
	pRoayway->SetCenterLine(centerLine);
	pRoayway->SetCorrectStakes(correctStakes);

	//3.创建截面
	size_t section_num = rd.sections.length();
	acedSetStatusBarProgressMeter(L"创建道路截面 ", 0,section_num);
	for (int index = 0; index < section_num; index++) {
		if (rd.startStake <= rd.sections[index].stakeNo && rd.sections[index].stakeNo <= rd.endStake) {
			CSideSection* pSection = new CSideSection(index, rd.sections[index]);
			pSection->Create();
			pRoayway->AppendSection(pSection);
			logInfo.Format(L"创建截面 %d/%d  桩号 %.2f  %s\n", 
				index + 1, 
				section_num, 
				rd.sections[index].stakeNo,
				rd.sections[index].isLeft ? L"左侧":L"右侧"); 	
			RDTRACE
		}
		acedSetStatusBarProgressMeterPos(index);
	}
	acedRestoreStatusBar();

	//4.生成道路实体
	pRoayway->Create3DSolid();

	//5. 
	#ifdef _DEBUG
	pRoayway->RemoveSectionObject();
	//pRoayway->RemoveAdiedEntities();
	#endif

	return pRoayway;
}

//在|IDs|的道路体中，查询以stake为中心，前后扩展gOptions.searchRange长度范围的道路体
AcArray<AcDbObjectId> GetRangeBody(const AcArray<AcDbObjectId>& IDs, double startStake,double endStake) {
	AcArray<AcDbObjectId> result;
	for (AcDbObjectId id : IDs) {
		double ss = GetBodyXData(id).startStake;
		double es = GetBodyXData(id).endStake;
		if (startStake - gOptions.searchRange <= ss && es <= endStake + gOptions.searchRange)
		{
			result.append(id);
		}
	}
	return result;
}

//使用 IDs1中的实体去“减掉” 与IDs2中的相交的部分
//相减结果反应在IDs1中，IDs2中原实体保持不变
void CRoadway::SubstractRoadwayBodies(const AcArray<AcDbObjectId>& IDs1, const AcArray<AcDbObjectId>& IDs2) {
	int count = 1;
	int s = 0;
	DWORD st = GetTickCount();  //取得系统运行时间(ms)
	AcArray<AcDbObjectId> IDs3;
	acedSetStatusBarProgressMeter(TEXT("两组实体的求差...:"), 0, IDs1.length());
	for (AcDbObjectId id1 : IDs1) {
		double ss = GetBodyXData(id1).startStake;
		double es = GetBodyXData(id1).endStake;
		IDs3 = GetRangeBody(IDs2, ss,es);
		int a = IDs2.length() - IDs3.length();
		s += a;
		logInfo.Format(L"第%d/%d号体 与 %d个体求差,少算%d个体\n", count++, IDs1.length(), IDs3.length(),a); RDTRACE
		SubtractSolids(id1, IDs3);
		acedSetStatusBarProgressMeterPos(count);
	}
	acedRestoreStatusBar();
	double t = s * 1.0 / (IDs1.length() * IDs2.length()) * 100;
	DWORD et = GetTickCount();  //取得系统运行时间(ms)
	logInfo.Format(L"求差 优化率 百分之 %.2f 算耗时: %.1fs \n", t, (et - st)*1.0 / 1000);  RDTRACE
}

AcArray<AcDbObjectId> CRoadway::IntersectRoadwayBodies(const AcArray<AcDbObjectId>& IDs1, const AcArray<AcDbObjectId>& IDs2) {
	AcArray<AcDbObjectId> result;
	int a = 0,s = 0;
	int count = 1;
	DWORD st = GetTickCount();//取得系统运行时间(ms)            
	AcArray<AcDbObjectId> IDs3;
	acedSetStatusBarProgressMeter(TEXT("两路段求交...:"), 0, IDs1.length());
	for (AcDbObjectId id1 : IDs1) {
		double ss = GetBodyXData(id1).startStake;
		double es = GetBodyXData(id1).endStake;
		IDs3 = GetRangeBody(IDs2,ss,es);
		int a = IDs2.length() - IDs3.length();
		s += a;
		logInfo.Format(L"第%d/%d号体 与 %d个体求交,少算%d个体 \n", count++, IDs1.length(), IDs3.length(),a); RDTRACE
		AcArray<AcDbObjectId> IDs4 = IntersectSolids(id1, IDs3, layer_intersect_road);
		if (!IDs4.isEmpty()) {
			result.append(IDs4);
		}															 
		acedSetStatusBarProgressMeterPos(count);
	}
	acedRestoreStatusBar();
	double t = s * 1.0 / (IDs1.length() * IDs2.length()) * 100;
	DWORD et = GetTickCount();  //取得系统运行时间(ms)
	logInfo.Format(L"求交 优化率 百分之 %.2f 算耗时: %.1fs \n", t, (et - st)*1.0 / 1000);  RDTRACE
	return result;
}

//根据新老路，创建改扩建道路体
//道路A,道路B
//改扩建路 扩 = 新+（老-新∩老） 
CRoadway* CRoadway::CreateFromeExtendRoad(CRoadway* newRoad, CRoadway* oldRoad,double newRoadStartStake, double newRoadEndStake) {
	
	//1.新∩老
	AcArray<AcDbObjectId> sNew2 = CloneEntities(newRoad->GetBodies(), layer_new_road);
	//截取只定范围内的新路体
	AcArray<AcDbObjectId> sNew;
	for (AcDbObjectId& id : sNew2) {
		if (newRoadStartStake <= GetBodyXData(id).startStake && GetBodyXData(id).endStake <= newRoadEndStake) {
			sNew.append(id);
		}
	}
	sNew2.removeAll();

	AcArray<AcDbObjectId> sOld = CloneEntities(oldRoad->GetBodies(), layer_old_road);
	MoveEntitiesToLayer(sNew, gOptions.layers[layer_extend_road]);
	MoveEntitiesToLayer(sOld, gOptions.layers[layer_extend_road]);
	AcArray<AcDbObjectId> sNew_I_Old = IntersectRoadwayBodies(sNew,sOld);
	//对于求交体，其sideKind属性统一改为section_side_kind_intersect类型
	for (int index = 0; index < sNew_I_Old.length(); index++) {
		tagBodyXData xd = GetBodyXData(sNew_I_Old[index]);
		xd.sideKind = section_side_kind_intersect;
		SetBodyXData(sNew_I_Old[index], xd);
	}

	AcArray<AcDbObjectId> sExtend;
	if (!gOptions.onlyBuildIntersectBody) {
		//2. 老-新∩老
		SubstractRoadwayBodies(sOld, sNew_I_Old);

		//3.新+（老-新∩老）
		sExtend.append(sNew);
		sExtend.append(sOld);
	}

	//4.产生扩建道路实体
	CRoadway* pRoayway = new CRoadway();
	pRoayway->roadName_ = newRoad->roadName_ + L"Over" + oldRoad->roadName_;
	pRoayway->roadwayKind_ = roadway_kind_extend;
	pRoayway->centerLine_  = newRoad->centerLine_->PartialCopy();
	pRoayway->AppendBodies(sExtend);
	pRoayway->AppendIntersectBodies(sNew_I_Old);
	return pRoayway;
}

void GetRangeBodies(double startStake, double endStake,
			const AcArray<AcDbObjectId>& srcBodies, CRoadCenterLine*  centerLine,
			AcArray<AcDbObjectId>& boyiesInRange, AcArray<AcDbObjectId>& tempBodies) {
	//1.找出指定桩号范围之内的体
	for (int index = 0; index < srcBodies.length(); index++) {
		tagBodyXData info = GetBodyXData(srcBodies[index]);
		if (info.startStake < 0 || info.endStake < 0) {
			logInfo.Format(L"实体附加属性提取失败!\n");
			RDTRACE
			continue;
		}
		//如果这段体在指一定范围之内，就直接加入
		if (startStake <= info.startStake  && info.endStake <= endStake) {
			boyiesInRange.append(srcBodies[index]);
		}
		//如果某一段首尾都超出了指定范围，那么就将首尾超出的部分切割掉
		else if (info.startStake < startStake &&  endStake < info.endStake) {
			AcDbObjectId id1 = SliceABody(srcBodies[index], centerLine->GetPoint(startStake), -centerLine->GetTagentDir(startStake));
			AcDbObjectId id2 = SliceABody(id1, centerLine->GetPoint(endStake), centerLine->GetTagentDir(endStake));
			boyiesInRange.append(id2);
			tempBodies.append(id2);
			RemoveEntityFromModelSpace(id1);
			//对于经过修剪的体要修改其起止桩号
			info.startStake = startStake;
			info.endStake = endStake;
			SetBodyXData(id2, info);
		}
		//如果与仅与起始处相交，就切割掉超出部分
		else if (info.startStake < startStake  && startStake < info.endStake)
		{
			AcDbObjectId id = SliceABody(srcBodies[index], centerLine->GetPoint(startStake), -centerLine->GetTagentDir(startStake));
			boyiesInRange.append(id);
			tempBodies.append(id);
			//对于经过修剪的体要修改其起止桩号
			info.startStake = startStake;
			SetBodyXData(id, info);
		}
		//如果与仅与终止处相交，就切割掉超出部分
		else if (info.startStake < endStake  && endStake < info.endStake) {
			AcDbObjectId id = SliceABody(srcBodies[index], centerLine->GetPoint(endStake), centerLine->GetTagentDir(endStake));
			boyiesInRange.append(id);
			tempBodies.append(id);
			//对于经过修剪的体要修改其起止桩号
			info.endStake = endStake;
			SetBodyXData(id, info);
		}
	}
}

void CRoadway::StatisticsBodiesInfo(AcArray<AcDbObjectId> boyiesInRange) {
	AcArray<tagBodyXData>  allInfos;
	const int boyiesNum = boyiesInRange.length();
	acedSetStatusBarProgressMeter(TEXT("写入体积材料信息...:"), 0, boyiesNum);
	wchar_t* sectionKind[] = { L"类型未设置！",L"1.老路",L"2.新路扩建侧",L"3.新路加铺区域（内侧）",L"4.新路加铺区域（外侧）",L"5.新路加铺区域（内侧和外侧）",L"6.新老路相交部分" };
	const int showInfoRowNum = 5;
	for (int index = 0; index < boyiesNum; index++) {
		acedSetStatusBarProgressMeterPos(index);
		tagBodyXData xData = GetBodyXData(boyiesInRange[index]);
		xData.volumn = CalculateBodyVolumn(boyiesInRange[index]);
		if (xData.volumn <= 0.0) continue;
		allInfos.append(xData);
		if (index < showInfoRowNum) {
			logInfo.Format(L"%d/%d)  材料号: %d  体类型: %s  体积: %.2f  %s  %s [%.2f---%.2f]\n",
				index + 1, boyiesNum,
				xData.materialID,
				sectionKind[(int)(xData.sideKind)],
				xData.volumn,
				xData.isLeft ? L"左侧" : L"右侧",
				xData.name.c_str(),
				xData.startStake, xData.endStake);
			RDTRACE
		}
		else if (index == showInfoRowNum) {
			logInfo.Format(L"还有%d行 写入的信息 省略显示...\n", boyiesNum - showInfoRowNum);
			RDTRACE
		}
	}
	acedRestoreStatusBar();

	//4.将结果写入到数据中去
	CDataBaseOpr db;
	if (db.Open()) {
		std::string Roadline_ID = centerLine_->GetRoadlineID();
		db.SaveVolumnMaterialInfoToTable(allInfos, Roadline_ID);
	}
	else {
		assert(false);
	}
}

//统计指定范围内道路体的体积和材料信息，结果保存数据库中
void CRoadway::StatisticsMaterialVolume(double startStake, double endStake) {
	
	AcArray<AcDbObjectId> boyiesInRange;
	AcArray<AcDbObjectId> tempBodies;  //记录为了统计而切割产生的临时三维体，计算完成后需要删除掉
	GetRangeBodies(startStake, endStake, solidBodies_,centerLine_, boyiesInRange, tempBodies);
	GetRangeBodies(startStake, endStake, solidIntersectBodies_, centerLine_, boyiesInRange, tempBodies);

	StatisticsBodiesInfo(boyiesInRange);

	//删除由于计算需要而产生的切割体
	for (int index = 0; index < tempBodies.length(); index++) {
		RemoveEntityFromModelSpace(tempBodies[index]);
	}
	tempBodies.removeAll();
}

void CRoadway::StatisticsMaterialVolume() {

	AcArray<AcDbObjectId> boyiesInRange;
	boyiesInRange.append(solidBodies_);
	boyiesInRange.append(solidIntersectBodies_);

	StatisticsBodiesInfo(boyiesInRange);
}

std::wstring CRoadway::name()
{
	return roadName_;
}