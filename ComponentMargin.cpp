﻿#include "pch.h"
#include "ComponentMargin.h"
#include "CreateRegionTool.h"
#include "BooleanOperation.h"
#include "EntityXData.h"
#include "global.h"

ComponentMargin::ComponentMargin(const tagComponentParameter& cp, CSideSectionAtom* owner_section,bool isLeft):
CSectionComponent(cp, owner_section,isLeft) {
	assert(cp.componentKind == component_kind_margin);
	assert(0 < cp.stepCount && cp.stepCount <= 4);
	colorIndex_ = 30; //土红色
}

ComponentMargin::~ComponentMargin() {

}


AcDbObjectId ComponentMargin::CreatePositiveComponet() {
	if (!cp_.grading) return AcDbObjectId();

	AcArray<AcDbObjectId> result;
	double deltaH = CalcStartDeltahHeight(cp_.horiOffset, owner_section_->GetPavementWidth());
	double os = owner_section_->GetOutwardSlop();
	//先按左侧描述关键点的位置，如果是右侧就再反向(x方向乘以-1)
	AcGePoint2d pos[50];
	int num = 0;
	if (cp_.stepCount == 1) {
		pos[0].x = -cp_.horiOffset;
		pos[0].y = cp_.anchorY + deltaH;
		pos[1] = pos[0];
		pos[1].y -= cp_.h1;
		pos[2] = pos[1];
		pos[2].x -= cp_.w1;
		pos[2].y += fabs(pos[2].x - pos[1].x) * os; //
		pos[3] = pos[2];
		pos[3].y -= cp_.h2;
		if (cp_.n2 > 0) {
			pos[3].x -= cp_.h2 * cp_.n2;
			pos[3].y += fabs(pos[3].x - pos[2].x) * os; //
		}
		pos[4] = pos[3];
		pos[4].x = -(owner_section_->OriginalWidth() + (cp_.h1 + cp_.h2) * cp_.n1);
		pos[4].y += fabs(pos[4].x - pos[3].x) * os; //
		pos[5] = pos[0];
		pos[5].x = -owner_section_->OriginalWidth();
		num = 6;
	}
	else if (cp_.stepCount == 2) {
		pos[0].x = -cp_.horiOffset;
		pos[0].y = cp_.anchorY + deltaH;
		pos[1] = pos[0];
		pos[1].y -= cp_.h1;
		pos[2] = pos[1];
		pos[2].x -= cp_.w2;
		pos[2].y += fabs(pos[2].x - pos[1].x) * os;  //
		pos[3] = pos[2];
		pos[3].y -= cp_.h2;
		if (cp_.n2 > 0) {
			pos[3].x -= cp_.h2 * cp_.n2;
			pos[3].y += fabs(pos[3].x - pos[2].x) * os;  //
		}
		pos[4] = pos[3];
		pos[4].x -= cp_.w3;
		pos[4].y += fabs(pos[4].x - pos[3].x) * os; //
		pos[5] = pos[4];
		pos[5].y -= cp_.h3;
		if (cp_.n3 > 0) {
			pos[5].x -= cp_.h3 * cp_.n3;
			pos[5].y += fabs(pos[5].x - pos[4].x) * os;  //
		}
		pos[6] = pos[5];
		pos[6].x = -(owner_section_->OriginalWidth() + (cp_.h1 + cp_.h2 + cp_.h3) * cp_.n1);
		pos[6].y += fabs(pos[6].x - pos[5].x) * os; //
		pos[7] = pos[0];
		pos[7].x = -owner_section_->OriginalWidth();
		num = 8;
	}
	else if (cp_.stepCount == 3) {
		pos[0].x = -cp_.horiOffset;
		pos[0].y = cp_.anchorY + deltaH;
		pos[1] = pos[0];
		pos[1].y -= cp_.h1;
		pos[2] = pos[1];
		pos[2].x -= cp_.w1;
		pos[2].y += fabs(pos[2].x - pos[1].x) * os; //
		pos[3] = pos[2];
		pos[3].y -= cp_.h2;
		if (cp_.n2 > 0) {
			pos[3].x -= cp_.h2 * cp_.n2;
			pos[3].y += fabs(pos[3].x - pos[2].x) * os;  //
		}
		pos[4] = pos[3];
		pos[4].x -= cp_.w2;
		pos[4].y += fabs(pos[4].x - pos[3].x) * os;  //
		pos[5] = pos[4];
		pos[5].y -= cp_.h3;
		if (cp_.n3 > 0) {
			pos[5].x -= cp_.h3 * cp_.n3;
			pos[5].y += fabs(pos[5].x - pos[4].x) * os;  //
		}
		pos[6] = pos[5];
		pos[6].x -= cp_.w3;
		pos[6].y += fabs(pos[6].x - pos[5].x) * os;  //
		pos[7] = pos[6];
		pos[7].y -= cp_.h4;
		if (cp_.n4 > 0)
		{
			pos[7].x -= cp_.h4 * cp_.n4;
			pos[7].y += fabs(pos[7].x - pos[6].x) * os; //
		}
		pos[8] = pos[7];
		pos[8].x = -(owner_section_->OriginalWidth() + (cp_.h1 + cp_.h2 + cp_.h3 + cp_.h4) * cp_.n1);
		pos[8].y += fabs(pos[8].x - pos[7].x) * os;
		pos[9] = pos[0];
		pos[9].x = -owner_section_->OriginalWidth();
		num = 10;
	}
	else {
		assert(false);
	}
	//在Y轴向上方向扩大一个范围，以包围进可能存在的路面结构上表面
	pos[0].y += owner_section_->Totalthickness();
	pos[num - 1].y = pos[0].y;

	CreateRegionTool regionTool;
	regionTool.Init();
	for (int index = 0; index < num; index++) {
		if (!isLeft_) {
			pos[index].x *= -1;
		}
		regionTool.AppendKeyPoint(pos[index]);
	}
	AcDbObjectId entId = regionTool.CreateRegion(colorIndex_);
	
	//通过与路面结构求交，去掉多余部分
	AcArray<AcDbObjectId> srcIDs = owner_section_->GetSideRoadStructRegions();
	IntersectRegions(entId,srcIDs);
	SetRegionXData(entId, tagRegionXData{ cp_.materialID,isLeft_,colorIndex_,owner_section_->GetSideKind(),cp_.Name,owner_section_ ->stakeNo(),-1 });

	return entId;
}

//创建一个“负面积”添加到路面结构中（即要从原路面结构中减去这个面域）
AcDbObjectId ComponentMargin::CreateNegaiveComponet() {
	if (cp_.grading) return AcDbObjectId();

	AcArray<AcDbObjectId> result;
	double deltaH = CalcStartDeltahHeight(cp_.horiOffset, owner_section_->GetPavementWidth());
	double os = owner_section_->GetOutwardSlop();
	//先按左侧描述关键点的位置，如果是右侧就再反向(x方向乘以-1)
	AcGePoint2d pos[50];
	int num = 0;
	if (cp_.stepCount == 1) {
		pos[0].x = -cp_.horiOffset;
		pos[0].y = cp_.anchorY + deltaH;
		pos[1] = pos[0];
		pos[1].y -= cp_.h1;
		pos[2] = pos[1];
		pos[2].x -= cp_.w1;
		pos[2].y += fabs(pos[2].x - pos[1].x) * os; //
		pos[3] = pos[2];
		pos[3].y -= cp_.h2;
		if (cp_.n1 > 0) {
			pos[3].x -= cp_.h2 * cp_.n1;
			pos[3].y += fabs(pos[3].x - pos[2].x) * os; //
		}
		pos[4] = pos[3];
		pos[4].y -= owner_section_->OriginalHeight();
		pos[5] = pos[4];
		pos[5].x -= owner_section_->OriginalWidth();
		pos[6] = pos[5];
		num = 7;
	}
	else if (cp_.stepCount == 2) {
		pos[0].x = -cp_.horiOffset;
		pos[0].y = cp_.anchorY + deltaH;
		pos[1] = pos[0];
		pos[1].y -= cp_.h1;
		pos[2] = pos[1];
		pos[2].x -= cp_.w2;
		pos[2].y += fabs(pos[2].x - pos[1].x) * os;  //
		pos[3] = pos[2];
		pos[3].y -= cp_.h2;
		if (cp_.n1 > 0) {
			pos[3].x -= cp_.h2 * cp_.n1;
			pos[3].y += fabs(pos[3].x - pos[2].x) * os;  //
		}
		pos[4] = pos[3];
		pos[4].x -= cp_.w3;
		pos[4].y += fabs(pos[4].x - pos[3].x) * os; //
		pos[5] = pos[4];
		pos[5].y -= cp_.h3;
		if (cp_.n2 > 0) {
			pos[5].x -= cp_.h3 * cp_.n2;
			pos[5].y += fabs(pos[5].x - pos[4].x) * os;  //
		}
		pos[6] = pos[5];
		pos[6].y -= owner_section_->OriginalHeight();
		pos[7] = pos[6];
		pos[7].x -= owner_section_->OriginalWidth();
		pos[8] = pos[7];
		num = 9;
	}
	else if (cp_.stepCount == 3) {
		pos[0].x = -cp_.horiOffset;
		pos[0].y = cp_.anchorY + deltaH;
		pos[1] = pos[0];
		pos[1].y -= cp_.h1;
		pos[2] = pos[1];
		pos[2].x -= cp_.w1;
		pos[2].y += fabs(pos[2].x - pos[1].x) * os;
		pos[3] = pos[2];
		pos[3].y -= cp_.h2;
		if (cp_.n1 > 0) {
			pos[3].x -= cp_.h2 * cp_.n1;
			pos[3].y += fabs(pos[3].x - pos[2].x) * os;
		}
		pos[4] = pos[3];
		pos[4].x -= cp_.w2;
		pos[4].y += fabs(pos[4].x - pos[3].x) * os;  
		pos[5] = pos[4];
		pos[5].y -= cp_.h3;
		if (cp_.n2 > 0) {
			pos[5].x -= cp_.h3 * cp_.n2;
			pos[5].y += fabs(pos[5].x - pos[4].x) * os;  
		}
		pos[6] = pos[5];
		pos[6].x -= cp_.w3;
		pos[6].y += fabs(pos[6].x - pos[5].x) * os;  
		pos[7] = pos[6];
		pos[7].y -= cp_.h4;
		if (cp_.n3 > 0)
		{
			pos[7].x -= cp_.h4 * cp_.n3;
			pos[7].y += fabs(pos[7].x - pos[6].x) * os; 
		}
		pos[8] = pos[7];
		pos[8].y -= owner_section_->OriginalHeight();
		pos[9] = pos[8];
		pos[9].x -= owner_section_->OriginalWidth();
		pos[10] = pos[9];
		num = 11;
	}
	else {
		assert(false);
	}
	//在Y轴向上方向扩大一个范围，以包围进可能存在的路面结构上表面
	pos[0].y += owner_section_->Totalthickness();
	pos[num - 1].y = pos[0].y;

	CreateRegionTool regionTool;
	regionTool.Init();
	for (int index = 0; index < num; index++) {
		if (!isLeft_) {
			pos[index].x *= -1;
		}
		regionTool.AppendKeyPoint(pos[index]);
	}
	AcDbObjectId entId = regionTool.CreateRegion(colorIndex_);
	return entId;
}