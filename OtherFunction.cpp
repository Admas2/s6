﻿#pragma once
#include "pch.h"
#include "acestext.h"
#include "PickRoadEdgeDlg.h"
#include "global.h"
#include "acadi.h"

//计算两向量间的夹角【0~180】之间
double CalculateAngleOfTwoVector(const AcGeVector3d& a, const AcGeVector3d& b)
{
	AcGeVector3d c = a;
	AcGeVector3d d = b;
	c.normalize();
	d.normalize();
	double t = c.dotProduct(d);
	double cita = acos(t);
	return cita / 3.1415926 * 180;
}

void CommandTestVector() {
	
	//1. 点+向量 => 新点
	AcGePoint3d pnt1(20, 20, 0);
	AcGeVector3d v(20, 20, 0);
	AcGePoint3d pnt2(pnt1 + v);  //得到点(40,40,0)
  
	//2.两个点位置计算得到其向量
	AcGePoint3d pnt3(10, 10, 0), pnt4(20, 20, 0);
	AcGeVector3d v2(pnt4 - pnt3); 
	v2 = v2.normalize();

	//3.dot product  and  cross product
	//通过dot product求两向量间夹角
	acutPrintf(L"\n (1) Angle of two vector: %5.3f\n", CalculateAngleOfTwoVector(AcGeVector3d(1, 0, 0), AcGeVector3d(1, 1, 0)));
	acutPrintf(L"\n (2) Angle of two vector: %5.3f\n", CalculateAngleOfTwoVector(AcGeVector3d(1, 0, 0), AcGeVector3d(0, 1, 0)));
	acutPrintf(L"\n (3) Angle of two vector: %5.3f\n", CalculateAngleOfTwoVector(AcGeVector3d(1, 0, 0), AcGeVector3d(-1, 1, 0)));
	acutPrintf(L"\n (4) Angle of two vector: %5.3f\n", CalculateAngleOfTwoVector(AcGeVector3d(1, 0, 0), AcGeVector3d(-1, 0, 0)));
	acutPrintf(L"\n (5) Angle of two vector: %5.3f\n", CalculateAngleOfTwoVector(AcGeVector3d(1, 0, 0), AcGeVector3d(-1, -1, 0)));
	acutPrintf(L"\n (6) Angle of two vector: %5.3f\n", CalculateAngleOfTwoVector(AcGeVector3d(1, 0, 0), AcGeVector3d(0, -1, 0)));

	//4.Rotate
}

void OnCommand(HWND hDlg, int nID, int nNotify)
{
	switch (nID)
	{
	case IDOK:
		::EndDialog(hDlg, IDOK);
		break;
	case IDCANCEL:
		::EndDialog(hDlg, IDCANCEL);
		break;
	}
}

INT_PTR CALLBACK DlgMsg(HWND hdlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_INITDIALOG:
		return TRUE; // ¶ÔÓÚ WM_INITDIALOG ±ØÐë·µ»ØÎª TRUE

	case WM_COMMAND:
		int nID = LOWORD(wParam);
		int nNotify = HIWORD(wParam);
		OnCommand(hdlg, nID, nNotify);
		break;
	}
	return FALSE;
}

void CommandMisc() {

	#if false
	AcGeVector3d vec(0, 1, 0);  	 	// 圆所在平面的法矢量 
	AcGePoint3d ptCenter1(30, 0, 0);  	// 圆心位置与半径的大小有关 
	AcDbCircle *pCircle1 = new AcDbCircle(ptCenter1, vec, 3);
	AcDbObjectId circleID = AppendEntityToModelSpace(pCircle1);
	AcDbHandle handle = circleID.handle();
	TRACE("%ld,%ld", handle.low(), handle.high());
	Adesk::UInt64 d = Adesk::UInt64(handle);
	#else
	//AcDbObjectIdArray IDs = PickEntities();
	//AcDbHandle handle3 = IDs[0].handle();
	//return;
	Adesk::UInt64 d = 7046;
	AcDbHandle handle2(d);
	AcDbObjectId circleID2;
	es = acdbHostApplicationServices()->workingDatabase()->getAcDbObjectId(circleID2,false, handle2);
	if (circleID2.isValid()) {
		if (!circleID2.isNull()) {
			AcDbEntity* pEntity = nullptr;
			es = acdbOpenAcDbEntity(pEntity, circleID2, AcDb::kForWrite);
			if (pEntity) pEntity->close();
			TRACE(L"ok\n");
		}
	}
	#endif
	return;

	//for (int i = 0; i < 5; i++)
	//{
	//	// 控制进度条当前进度位置  
	//	wchar_t buffer[200];
	//	swprintf_s(buffer, L"中华人民共和国  %d-------", i);
	//	acedSetStatusBarProgressMeter(buffer, 0, 0);
	//	::Sleep(500);
	//}
	//// 还原状态栏之前的样子  
	//acedRestoreStatusBar();

	acedSetStatusBarProgressMeter(TEXT("Do something...:"), 0, 5);
	for (int i = 0; i < 5; i++)
	{
		// 控制进度条当前进度位置    
		acedSetStatusBarProgressMeterPos(i + 1);
		::Sleep(1000);
	}
	// 还原状态栏之前的样子  
	acedRestoreStatusBar();

	//MySetStatusText(L"....");
	//for (int index = 0; index < 100; index++) {
	//	wchar_t buffer[200];
	//	swprintf_s(buffer, L"%d----------", index);
	//	MySetStatusText(buffer);
	//	::Sleep(1000);
	//}


	CWinApp*    pWinApp = acedGetAcadWinApp();
	CDocument*  pDoc = acedGetAcadDoc();
	CView* pDwgView = acedGetAcadDwgView();
	CMDIFrameWnd* pFrame = acedGetAcadFrame();
	CWnd* pDockCmdWnd = acedGetAcadDockCmdLine();
	CWnd* pTextWnd = acedGetAcadTextCmdLine();

	//CWinApp*    pWinApp2 = AfxGetApp();
	//LPCTSTR pName = AfxGetAppName();
	//CWnd*  pMainWnd = AfxGetMainWnd();

	//HINSTANCE save_hInstance = AfxGetResourceHandle();

	//HINSTANCE exe_hInstance = ::GetModuleHandle(NULL);
	//HINSTANCE dll_hInstance = ::GetModuleHandle(L"Arx-CPP-plug-in.arx");
	//AfxSetResourceHandle(dll_hInstance); //切换状态
	//HINSTANCE hInstance = AfxGetResourceHandle();
	//CPickRoadEdgeDlg dlg(NULL,hInstance);
	//INT_PTR r = dlg.DoModal();
	//int n = ::GetLastError();
	//AfxSetResourceHandle(exe_hInstance); //恢复状态
	//hInstance = AfxGetResourceHandle();

	//::DialogBox(dll_hInstance, MAKEINTRESOURCE(IDD_PICK_ROAD_EDGE_DLG), pFrame->GetSafeHwnd(), DlgMsg);

}

void CommandHelloWorld()
{
	acutPrintf(L"\nHello,World!\n");
}

Acad::ErrorStatus EntityMove(AcDbEntity *pEnt, AcGePoint3d ptBase, AcGePoint3d ptDest)
{
	AcGeMatrix3d xform;
	AcGeVector3d vec(ptDest.x - ptBase.x, ptDest.y - ptBase.y, ptDest.z - ptBase.z);  	
	xform.setToTranslation(vec);
	es = pEnt->transformBy(xform);
	return es;
}

Acad::ErrorStatus EntityScale(AcDbEntity *pEnt, AcGePoint3d ptBase, double scaleFactor)
{
	AcGeMatrix3d xform;
	xform.setToScaling(scaleFactor, ptBase);
	es = pEnt->transformBy(xform);
	return es;
}

Acad::ErrorStatus EntityRotate(AcDbEntity *pEnt, AcGePoint3d ptBase, AcGeVector3d vec,double rotation)
{
	AcGeMatrix3d xform;
	xform.setToRotation(rotation, vec, ptBase);
	es = pEnt->transformBy(xform);
	return es;
}

void ZffCHAP5AddCircle1()
{
	// 调用acedCommandS函数创建圆 
	ads_point ptCenter = { 0, 0, 0 }; // 圆心  	
	ads_real radius = 10;  	 	// 半径 
	acedCommandS(RTSTR, "Circle", RTPOINT, ptCenter, RTREAL, radius, RTNONE);
}

void ZffCHAP5ADDCIRCLE2()
{
	// 创建结果缓冲区链表 
	ads_point ptCenter = { 30, 0, 0 };
	ads_real radius = 10;
	resbuf *rb = acutBuildList(RTSTR, "Circle",RTPOINT, ptCenter,RTREAL, radius,RTNONE);

	// 创建圆 
	int rc = RTNORM; // 返回值 
	if (rb != NULL)
	{
		rc = acedCmdS(rb);
	}

	// 检验返回值 
	if (rc != RTNORM)
	{
		acutPrintf(L"\n创建圆失败!");
	}

	//释放结构体
	acutRelRb(rb);

	// 进行缩放 
	acedCommandS(RTSTR, "Zoom", RTSTR, "E", RTNONE);
}

void ZffCHAP5EntInfo()
{
	//1.提示用户选择实体  
	ads_name entName;
	ads_point pt;
	if (acedEntSel(L"\n选择实体:", entName, pt) != RTNORM) return;

	//2.从entName获得保存实体数据的结果缓冲区 
	resbuf* rbEnt = acdbEntGet(entName);
	resbuf* rb = rbEnt; // 用于遍历rbEnt的结果缓冲区 

	while (rb != NULL)
	{	//rb->restype 是DXF组码
		switch (rb->restype)
		{
		case -1: // 图元名 
			acutPrintf(L"\n图元名: %x", rb->resval.rstring);
			break;
		case 0: 	 // 图元类型 
			acutPrintf(L"\n图元类型: %s", rb->resval.rstring);
			break;
		case 8: 	 // 图层 
			acutPrintf(L"\n图层:%s", rb->resval.rstring);
			break;
		case 10: // 圆心 
			acutPrintf(L"\n圆心:(%.2f, %.2f, %.2f)", rb->resval.rpoint[X], rb->resval.rpoint[Y], rb->resval.rpoint[Z]);
			break;
		case 40: // 半径 
			acutPrintf(L"\n半径:%.4f", rb->resval.rreal);
			break;
		case 210: // 圆所在平面的法向矢量 
			acutPrintf(L"\n平面的法向矢量:(%.2f, %.2f, %.2f)", rb->resval.rpoint[X], rb->resval.rpoint[Y], rb->resval.rpoint[Z]);
			break;
		} 
		rb = rb->rbnext;  	// 切换到下一个节点 
	} 	// while 

	if (rbEnt != NULL)
	{
		//释放结构体
		acutRelRb(rbEnt);
	}
}

void ZffCHAP5AddPolyBasic()
{
	int index = 2;    // 当前输入点的次数  
	ads_point ptStart;   // 起点 
	if (acedGetPoint(NULL, L"\n输入第一点：", ptStart) != RTNORM)
		return;

	ads_point ptPrevious, ptCurrent; // 前一个参考点，当前拾取的点  acdbPointSet(ptStart, ptPrevious); 
	AcDbObjectId polyId;  	 	 			// 多段线的ID 
	while (acedGetPoint(ptPrevious, L"\n输入下一点：", ptCurrent) == RTNORM)
	{
		if (index == 2)
		{
			// 创建多段线 
			AcDbPolyline *pPoly = new AcDbPolyline(2);
			AcGePoint2d ptGe1, ptGe2; // 两个节点    
			ptGe1[X] = ptPrevious[X];
			ptGe1[Y] = ptPrevious[Y];
			ptGe2[X] = ptCurrent[X];
			ptGe2[Y] = ptCurrent[Y];
			pPoly->addVertexAt(0, ptGe1);
			pPoly->addVertexAt(1, ptGe2);

			// 添加到模型空间 
			polyId = AppendEntityToModelSpace(pPoly);
		}
		else if (index > 2)
		{
			// 修改多段线，添加后一个顶点 
			AcDbPolyline *pPoly;
			acdbOpenObject(pPoly, polyId, AcDb::kForWrite);

			AcGePoint2d ptGe;   // 增加的节点   
			ptGe[X] = ptCurrent[X];
			ptGe[Y] = ptCurrent[Y];

			pPoly->addVertexAt(index - 1, ptGe);

			pPoly->close();
		}
		index++;
		acdbPointSet(ptCurrent, ptPrevious);
	}
}

void ZffCHAP5GetPoint()
{
	int rc;  // 返回值  
	ACHAR kword[20];  // 关键字  	
	ads_point pt; 
	acedInitGet(RSG_NONULL, L"Keyword1 keyWord2");
	rc = acedGetPoint(NULL, L"输入一个点或[Keyword1/keyWord2]:", pt);

	switch (rc)
	{
	case RTKWORD: // 输入了关键字 
		if (acedGetInput(kword) != RTNORM)
			return;
		if ( CString(kword) == L"Keyword1")
			acedAlert(L"选择的关键字是Keyword1!");
		else if (CString(kword) == L"keyWord2") 
			acedAlert(L"选择的关键字是keyWord2!");  	 	
			break;  	
	case RTNORM:
		acutPrintf(L"输入点的坐标是(%.2f, %.2f, %.2f)", pt[X], pt[Y], pt[Z]);
	} 	// switch 
}

ads_real GetWidth()
{
	ads_real width = 0;
	if (acedGetReal(L"\n输入线宽:", &width) == RTNORM)
	{
		return width;
	}
	else
	{
		return 0;
	}
}
int GetColorIndex()
{
	int colorIndex = 0;

	if (acedGetInt(L"\n输入颜色索引值(0～256):", &colorIndex) != RTNORM)
		return 0;
		// 处理颜色索引值无效的情况 
		while (colorIndex < 0 || colorIndex > 256)
		{
			acedPrompt(L"\n输入了无效的颜色索引.");

				if (acedGetInt(L"\n输入颜色索引值(0～256):", &colorIndex) != RTNORM)
					return 0;

		}
	return colorIndex;
} 	

void ZffCHAP5AddPoly()
{
	int colorIndex = 0;  	 	// 颜色索引值 

	ads_real width = 0; 	 	 // 多段线的线宽 
	int index = 2; 	  	 	// 当前输入点的次数 
	ads_point ptStart;  	 	// 起点 

	// 提示用户输入起点 
	if (acedGetPoint(NULL, L"\n输入第一点:", ptStart) != RTNORM)
		return;

	ads_point ptPrevious, ptCurrent; // 前一个参考点，当前拾取的点  acdbPointSet(ptStart, ptPrevious); 
	AcDbObjectId polyId;  	 	 	// 多段线的ID 

	// 输入第二点 
	acedInitGet(NULL, L"W C O");
	int rc = acedGetPoint(ptPrevious, L"\n输入下一点 [宽度(W)/颜色(C)]<完成(O)>:", ptCurrent);
	while (rc == RTNORM || rc == RTKWORD)
	{
		if (rc == RTKWORD)  	// 如果用户输入了关键字 
		{
			ACHAR kword[20];
			if (acedGetInput(kword) != RTNORM)
				return;
			if (CString(kword) == L"W")
			{
				width = GetWidth();
			}
			else if (CString(kword) == L"C")
			{
				colorIndex = GetColorIndex();
			}
			else if (CString(kword) == L"O")
			{
				return;
			}
			else
			{
				acutPrintf(L"\n无效的关键字.");
			}
		}
		else if (rc == RTNORM) 	// 用户输入了点 
		{
			if (index == 2)
			{
				// 创建多段线 
				AcDbPolyline *pPoly = new AcDbPolyline(2);
				AcGePoint2d ptGe1, ptGe2; // 两个节点     
				ptGe1[X] = ptPrevious[X];
				ptGe1[Y] = ptPrevious[Y];
				ptGe2[X] = ptCurrent[X];
				ptGe2[Y] = ptCurrent[Y];
				pPoly->addVertexAt(0, ptGe1);
				pPoly->addVertexAt(1, ptGe2);

				// 修改多段线的颜色和线宽 
				pPoly->setConstantWidth(width);
				pPoly->setColorIndex(colorIndex);

				// 添加到模型空间 
				polyId = AppendEntityToModelSpace(pPoly);
			}
			else if (index > 2)
			{
				// 修改多段线，添加后一个顶点 
				AcDbPolyline *pPoly;
				acdbOpenObject(pPoly, polyId, AcDb::kForWrite);

				AcGePoint2d ptGe;   // 增加的节点     
				ptGe[X] = ptCurrent[X];
				ptGe[Y] = ptCurrent[Y];

				pPoly->addVertexAt(index - 1, ptGe);

				// 修改多段线的颜色和线宽 
				pPoly->setConstantWidth(width);
				pPoly->setColorIndex(colorIndex);

				pPoly->close();
			}

			index++;

			acdbPointSet(ptCurrent, ptPrevious);
		}

		// 提示用户输入新的节点 
		acedInitGet(NULL, L"W C O");
		rc = acedGetPoint(ptPrevious,L"\n输入下一点 [宽度(W)/颜色(C)]<完成(O)>:", ptCurrent);
	}
}

void ZffCHAP5SelectFile()
{
	const ACHAR* title = L"选择图形文件";
	const ACHAR* path =  L"C:\\";
	resbuf *fileName = acutNewRb(RTSTR);
	if (acedGetFileD(title, path, L"dwg;dxf", 0, fileName) == RTNORM)
	{
		acedAlert(fileName->resval.rstring);
	}
	//释放结构体
	acutRelRb(fileName);
}

void CommandCreateRegion()
{
	// 使用选择集，提示用户选择作为面域边界的对象  
	ads_name ss; 
	int rt = acedSSGet(NULL, NULL, NULL, NULL, ss);  	// 提示用户选择对象 
	AcDbObjectIdArray objIds;

	// 根据选择集中的对象构建边界曲线的ID数组 
	if (rt == RTNORM)
	{
		Adesk::Int32 length;
		acedSSLength(ss, &length);  // 获得选择集中的对象个数 
		for (int i = 0; i < length; i++)
		{
			ads_name ent;
			acedSSName(ss, i, ent);
			AcDbObjectId objId;
			acdbGetObjectId(objId, ent);
			objIds.append(objId);
		}
	}
	acedSSFree(ss);// 及时释放选择集 

	AcDbObjectIdArray regionIds;
	CreateRegion(objIds,regionIds);

	int number = regionIds.length();  	
	if (number > 0)
	{
		acutPrintf(L"\n已经创建%d个面域！", number);
	}
	else
	{
		acutPrintf(L"\n创建0个面域！");
	}
}

void ZffCHAP4NewUCS()
{
	// 2.将新建的UCS表记录添加到UCS表中 
	AcDbUCSTable *pUcsTbl = nullptr;
	acdbHostApplicationServices()->workingDatabase()->getUCSTable(pUcsTbl, AcDb::kForWrite);

	AcDbUCSTableRecord *pUcsTblRcd = new AcDbUCSTableRecord();
	es = pUcsTbl->add(pUcsTblRcd);
	assert(es == Acad::eOk);

	es = pUcsTblRcd->setName(L"NewUcs");  	
	assert(es == Acad::eOk);

	AcGePoint3d ptOrigin(0, 0, 0);
	AcGeVector3d vecXAxis(1, 1, 0);
	AcGeVector3d vecYAxis(-1, 1, 0);
	pUcsTblRcd->setOrigin(ptOrigin);  	
	pUcsTblRcd->setXAxis(vecXAxis);  	
	pUcsTblRcd->setYAxis(vecYAxis);
	  

	// 2.关闭对象 
	pUcsTblRcd->close();  	
	pUcsTbl->close();
}

void ZffCHAP4SetCurUcs()
{
	// 提示用户输入UCS的名称  
	ACHAR ucsName[40]; 
	if (acedGetString(NULL, L"\n输入用户坐标系的名称：", ucsName) !=RTNORM)
		return;

	// 获得指定的UCS表记录 
	AcDbUCSTable *pUcsTbl;
	acdbHostApplicationServices()->workingDatabase()->getUCSTable(pUcsTbl, AcDb::kForRead);  	
	assert(pUcsTbl->has(ucsName));
	AcDbUCSTableRecord *pUcsTblRcd = nullptr;
	pUcsTbl->getAt(ucsName, pUcsTblRcd, AcDb::kForRead);

	// 获得UCS的变换矩阵 
	AcGeMatrix3d mat;
	AcGeVector3d vecXAxis, vecYAxis, vecZAxis;  	
	vecXAxis = pUcsTblRcd->xAxis();  	
	vecYAxis = pUcsTblRcd->yAxis();  	
	vecZAxis = vecXAxis.crossProduct(vecYAxis);  	
	mat.setCoordSystem(pUcsTblRcd->origin(), vecXAxis,vecYAxis, vecZAxis);

	// 关闭UCS表和UCS表记录 
	pUcsTblRcd->close();  	
	pUcsTbl->close();

	// 设置当前的UCS 
	acedSetCurrentUCS(mat);
}

void ZffCHAP4AddEntInUcs()
{	
	#if false
	// 转换坐标系的标记 
	resbuf wcs, ucs;
	wcs.restype = RTSHORT;
	wcs.resval.rint = 0;
	ucs.restype = RTSHORT;
	ucs.resval.rint = 1;

	// 提示用户输入直线的起点和终点 
	ads_point pt1, pt2;
	if (acedGetPoint(NULL, L"拾取直线的起点：", pt1) != RTNORM)  	 	return;
	if (acedGetPoint(pt1, L"拾取直线的终点：", pt2) != RTNORM)  	 	return;

	// 将起点和终点坐标转换到WCS 
	acedTrans(pt1, &ucs, &wcs, 0, pt1);
	acedTrans(pt2, &ucs, &wcs, 0, pt2);

	// 创建直线 
	AcDbLine *pLine = new AcDbLine(asPnt3d(pt1), asPnt3d(pt2));

	AcDbBlockTable *pBlkTbl;
	acdbHostApplicationServices()->workingDatabase()->getBlockTable(pBlkTbl, AcDb::kForRead);
	AcDbBlockTableRecord *pBlkTblRcd = nullptr;
	pBlkTbl->getAt(ACDB_MODEL_SPACE, pBlkTblRcd, AcDb::kForWrite);
	pBlkTbl->close();
	pBlkTblRcd->appendAcDbEntity(pLine);
	pLine->close();
	#endif
}

void CommandTestMoveCurrentUCS(){
		AcGeMatrix3d mat;
		es = acedGetCurrentUCS(mat);
		AcGePoint3d ptOrigin;
		AcGeVector3d vecXAxis, vecYAxis, vecZAxis;
		mat.getCoordSystem(ptOrigin, vecXAxis, vecYAxis, vecZAxis);
		AcGeVector3d vec(33, 55, 71);
		ptOrigin += vec;
		mat.setCoordSystem(ptOrigin, vecXAxis, vecYAxis, vecZAxis);

		AcDbLine *pLine = new AcDbLine(AcGePoint3d(0, 0, 0), AcGePoint3d(100, 100, 0));
		pLine->transformBy(mat);

		AppendEntityToModelSpace(pLine);
}


void CommandTestQuveryAEntity() {

	//拾取截面图
	acutPrintf(L"\n 请拾一组图元\n");
	ads_name ss;
	int rt = acedSSGet(NULL, NULL, NULL, NULL, ss);  	// 提示用户选择对象 
	if (rt != RTNORM) return;
	Adesk::Int32 length;
	acedSSLength(ss, &length);
	AcDbEntity *pFirestEntity = nullptr;
	for (int index = 0; index < length; index++) {
		ads_name entityName;
		acedSSName(ss, index, entityName);
		AcDbObjectId entityID;
		acdbGetObjectId(entityID, entityName);
		AcDbEntity *pEntity = nullptr;
		acdbOpenAcDbEntity(pEntity, entityID, AcDb::kForWrite);
		acutPrintf(L"\n (%3d) picked is %s", index+1,pEntity->isA()->name());
		if (index == 0) pFirestEntity = pEntity;
	}

	if (length != 1) return;
	if (pFirestEntity->isKindOf(AcDbSpline::desc())) {
		AcDbSpline* pSpline = AcDbSpline::cast(pFirestEntity);
		for (int index = 0; index < pSpline->numFitPoints(); index++) {
			AcGePoint3d point;
			pSpline->getFitPointAt(index, point);
			acutPrintf(L"\n %d:  (%8.2f,%8.2f,%8.2f)\n", index, point.x, point.y, point.z);
		}
	}


	if (pFirestEntity->isKindOf(AcDbRegion::desc())) {
		AcDbRegion* pSection = AcDbRegion::cast(pFirestEntity);

		const AcGePoint3d  origin;
		const AcGeVector3d xAxis;
		const AcGeVector3d yAxis;
		double             perimeter;
		double             area;
		AcGePoint2d        centroid;
		double              momInertia[2];
		double             prodInertia;
		double              prinMoments[2];
		AcGeVector2d        prinAxes[2];
		double              radiiGyration[2];
		AcGePoint2d        extentsLow;
		AcGePoint2d        extentsHigh;

		es = pSection->getAreaProp(
			origin,
			xAxis,
			yAxis,
			perimeter,
			area,
			centroid,
			momInertia,
			prodInertia,
			prinMoments,
			prinAxes,
			radiiGyration,
			extentsLow,
			extentsHigh);
		AcGePlane regionPlane;
		es = pSection->getPlane(regionPlane);
		Adesk::UInt32 n = pSection->numChanges();
		acutPrintf(L"\n AcDbRegion prop: %d\n", n);

#if false
		AcGeMatrix3d mat;
		mat.setCoordSystem(AcGePoint3d(0, 0, 0), AcGeVector3d(0, 0, 1), AcGeVector3d(0, 1, 0), AcGeVector3d(-1, 0, 0));
		pSection->transformBy(mat);
		pSection->close();
#endif
	}
}

AcDbObjectIdArray PickEntities() {
	AcDbObjectIdArray result;
	ads_name ss;
	int rt = acedSSGet(NULL, NULL, NULL, NULL, ss);  	// 提示用户选择对象 
	if (rt == RTNORM)
	{
		Adesk::Int32 length;
		acedSSLength(ss, &length);  // 获得选择集中的对象个数 
		for (int i = 0; i < length; i++)
		{
			ads_name ent;
			acedSSName(ss, i, ent);
			AcDbObjectId objId;
			acdbGetObjectId(objId, ent);
			result.append(objId);
		}
	}
	acedSSFree(ss);
	return result;
}

void CommandEntityCopy() {
	
	//1.拾取
	AcDbObjectIdArray IDs = PickEntities();
	if (IDs.length() == 0) return;

	//2.复制
	AcDbEntity *pSrcEnt = NULL;
	acdbOpenObject(pSrcEnt, IDs[0], AcDb::kForRead);
	AcDbEntity *pCopyEnt = AcDbEntity::cast(pSrcEnt->clone());
	AcDbObjectId copyEntId = AppendEntityToModelSpace(pCopyEnt);

	//3.移动
	AcGeMatrix3d mat;
	mat.setToTranslation(AcGeVector3d(20, 20, 0));
	es = acdbOpenObject(pCopyEnt, copyEntId, AcDb::kForWrite);
	assert(es == Acad::eOk);
	es = pCopyEnt->transformBy(mat);

	//4.关闭
	pCopyEnt->close();
	pSrcEnt->close();
}

void MirrorBodies(AcArray<AcDbObjectId>&  acdbRegions) {
		AcDbObjectIdArray newEntities;
		for (int index = 0; index < acdbRegions.length(); index++) {
			AcDbEntity *pSrcEnt = NULL;
			es = acdbOpenObject(pSrcEnt, acdbRegions[index], AcDb::kForWrite);
			if (es == Acad::eOk) {
				AcDbEntity *pCopyEnt = AcDbEntity::cast(pSrcEnt->clone());
				AcDbObjectId  copyEntId = AppendEntityToModelSpace(pCopyEnt, layer_Sections);
				es = acdbOpenObject(pCopyEnt, copyEntId, AcDb::kForWrite);
				if (es == Acad::eOk) {
					AcGeMatrix3d mat;
					mat.setToMirroring(AcGeLine3d(AcGePoint3d(0, 0, 0), AcGeVector3d(0, 1, 0)));
					es = pCopyEnt->transformBy(mat);
					assert(es == Acad::eOk);
					newEntities.append(copyEntId);
					pCopyEnt->close();
					pSrcEnt->close();
				}
			}
		}

		if (newEntities.length() > 0)
			acdbRegions.append(newEntities);
}


void CommandEntityMirror() {

	//1.拾取
	AcDbObjectIdArray IDs = PickEntities();
	if (IDs.length() == 0) return;

	//2.复制
	AcDbEntity *pSrcEnt = NULL;
	acdbOpenObject(pSrcEnt, IDs[0], AcDb::kForWrite);
	AcDbEntity *pCopyEnt = AcDbEntity::cast(pSrcEnt->clone());
	AcDbObjectId copyEntId = AppendEntityToModelSpace(pCopyEnt);

	//3.镜像
	AcGeMatrix3d mat;
	mat.setToMirroring(AcGeLine3d(AcGePoint3d(0,0,0), AcGeVector3d(0,1,0)));
	// or  mat.setToRotation(180, AcGeVector3d::kZAxis, AcGePoint3d(0, 0, 0));
	es = acdbOpenObject(pCopyEnt, copyEntId, AcDb::kForWrite);
	assert(es == Acad::eOk);
	es = pCopyEnt->transformBy(mat);

	//4.合并
	if (pSrcEnt->isKindOf(AcDbRegion::desc())) {
		AcDbRegion::cast(pSrcEnt)->booleanOper(AcDb::kBoolUnite, AcDbRegion::cast(pCopyEnt));
	}

	//5.关闭
	pCopyEnt->close();
	pSrcEnt->close();
}

void CommandTestAddInfoToEnttiy()
{
	//1.拾取实体
	AcDbObjectIdArray entities = PickEntities();
	if (entities.length() == 0) return;

	//2.添加附加数据
	AcGePoint3d pickPoint(1, 2, 3);
	ACHAR* appName = L"RoadeDesign";
	acdbRegApp(appName);
	
	//3.查询附加信息是否存在
	AcDbEntity *pEnt = nullptr;
	acdbOpenAcDbEntity(pEnt, entities[0], AcDb::kForWrite);
	resbuf* pTemp = pEnt->xData(appName);

	if (pTemp != NULL) 	 	
	{
		//首先要跳过数据名称这一项
		pTemp = pTemp->rbnext;
		acutPrintf(TEXT("\n字符串类型的扩展数据是:%s"), pTemp->resval.rstring);

		pTemp = pTemp->rbnext;
		acutPrintf(_T("\n整数类型的扩展数据是:%d"), pTemp->resval.rint);

		pTemp = pTemp->rbnext;
		acutPrintf(TEXT("\n实数类型的扩展数据是:%.2f"), pTemp->resval.rreal);

		pTemp = pTemp->rbnext;
		acutPrintf(TEXT("\n点坐标的数据类型的扩展数据是:(%.2f,%.2f,%.2f)"),
							pTemp->resval.rpoint[X], 
							pTemp->resval.rpoint[Y],
							pTemp->resval.rpoint[Z]);
		acutRelRb(pTemp);
		acutPrintf(L"\n所选择的实体已经包含扩展数据！");
	}
	else
	{
		acutPrintf(L"\n  为此实体添加信息 %s", appName);
		//如果不存在就添加
		resbuf* pRb = acutBuildList(
			AcDb::kDxfRegAppName, appName,									// 应用程序名称
			AcDb::kDxfXdAsciiString, L"道路中心线",					// 字符串 
			AcDb::kDxfXdInteger32, 2,												// 整数 		
			AcDb::kDxfXdReal, 3.14,	 	 											// 实数
			AcDb::kDxfXdWorldXCoord, asDblArray(pickPoint),	//点坐标值
			RTNONE);
			pEnt->setXData(pRb);
			acutRelRb(pRb);
	}
	pEnt->close();
}

