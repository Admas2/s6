﻿#pragma once
#include "pch.h"
#include "RoadCenterLine.h"
#include "SideSection.h"
#include <map>
class CRoadway
{
private:
	CRoadCenterLine*  centerLine_ = nullptr;
	AcArray<CSideSection*>  sections_;

	AcArray<AcDbObjectId>   sectionRegions_;
	AcArray<AcDbObjectId>   solidBodies_;
	AcArray<AcDbObjectId>   solidIntersectBodies_;  //新老路求交体，只有针对扩建道路才有用
	AcArray<tagCorrectStake> correctStakes_;
	std::wstring  roadName_;
	enum eRoadwayKind {
		roadway_kind_null = 0,
		roadway_kind_old = 1,
		roadway_kind_new = 2,
		roadway_kind_extend = 3
	};
	eRoadwayKind   roadwayKind_ = roadway_kind_null;
public:
	static CRoadway* CreateFromRawData(const tagRoadwayData& rd);
	static CRoadway* CreateFromeExtendRoad(CRoadway* pOldRoad, CRoadway* pNewRoad,double newRoadStartStake, double newRoadEndStake);
	CRoadway();
	virtual ~CRoadway();
	std::wstring name();
	void SetCenterLine(CRoadCenterLine*  centerLine);
	CRoadCenterLine* GetCenterLine() { return centerLine_; }
	void AppendSection(CSideSection* sections);
	void Create3DSolid();
	void RemoveAdiedEntities();
	void RemoveSectionObject();
	AcArray<AcDbObjectId>  GetBodies();
	void AppendBodies(AcArray<AcDbObjectId> bodies);
	void AppendIntersectBodies(AcArray<AcDbObjectId> bodies);
	void StatisticsMaterialVolume();
	void StatisticsMaterialVolume(double startStake, double endStake);
	void StatisticsBodiesInfo(AcArray<AcDbObjectId> boyiesInRange);
	void SetCorrectStakes(const AcArray<tagCorrectStake>& correctStakes);

	static AcArray<AcDbObjectId> IntersectRoadwayBodies(const AcArray<AcDbObjectId>& IDs1, const AcArray<AcDbObjectId>& IDs2);
	static void  SubstractRoadwayBodies(const AcArray<AcDbObjectId>& IDs1, const AcArray<AcDbObjectId>& IDs2);
private:
	AcDbRegion* CreateComponentRegion(AcDbObjectId regionID);
	void DivideToComponentsGroup(const AcArray<CSideSection*>& sections, AcArray<AcArray<AcDbObjectId>>& componentsGroup);
	void DivideToRoadSegement(AcArray<AcArray<CSideSection*>>& segmentSectionGroup);
	
	eLayerID GetBodyLayer();
	void CorrectStake(double& stakeNo);
	
	void LoftComponentsGroup(AcArray<AcArray<AcDbObjectId>>& componentsGroup);
	bool LoftBody(const AcArray<AcDbObjectId>& componets, int componentInde, int totalNum, bool useGuideLine, AcArray<AcDbEntity*>& sectionRegions,AcDbObjectId& result);
	bool GraduallyStepModelBody(const AcArray<AcDbObjectId>& componets, int componentIndex, int totalNum, AcArray<AcDbEntity*>& sectionRegions);

	void ShowInfoForModelResult(const AcArray<AcDbObjectId>& componets, int index, int num, bool isSuccess, AcArray<AcDbEntity*>& sectionRegions);
};

