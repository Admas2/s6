#pragma once
#include "SectionComponent.h"
class CComponentDivider : public CSectionComponent
{
public:
	CComponentDivider(const tagComponentParameter& cp, CSideSectionAtom* owner_section, bool isLeft);
	~CComponentDivider();
	AcDbObjectId CreatePositiveComponet() override;
	AcDbObjectId CreateNegaiveComponet() override;
};

