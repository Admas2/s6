﻿#include "pch.h"
#include "ComponentDivider.h"
#include "SideSection.h"
#include "CreateRegionTool.h"
#include "EntityXData.h"
#include "SideSectionAtom.h"

CComponentDivider::CComponentDivider(const tagComponentParameter& cp, CSideSectionAtom* owner_section,bool isLeft):
	CSectionComponent(cp, owner_section, isLeft) {
	assert(cp.componentKind == component_kind_divider);
	assert(cp.stepCount <= 4);
	colorIndex_ = 2;//Yellow
}

CComponentDivider::~CComponentDivider() {

}

AcDbObjectId CComponentDivider::CreateNegaiveComponet() {
	if ( cp_.anchorY == 0) return AcDbObjectId();

	AcGePoint2d pos[50];
	pos[0].x = 0;
	pos[0].y = 0;
	pos[1].x = cp_.w1;
	pos[1].y = 0;
	pos[2].x = cp_.w1;
	pos[2].y = cp_.anchorY;
	pos[3].x = 0;
	pos[3].y = cp_.anchorY;

	AcArray<AcDbObjectId> result;
	CreateRegionTool regionTool;
	regionTool.Init();
	for (int index = 0; index < 4; index++) {
		if (isLeft_) {
			pos[index].x *= -1;
		}
		regionTool.AppendKeyPoint(pos[index]);
	}
	AcDbObjectId entId = regionTool.CreateRegion(colorIndex_);
	
	return entId;
}

AcDbObjectId  CComponentDivider::CreatePositiveComponet() {
	AcArray<AcDbObjectId> result;
	AcGePoint2d pos[50];
	int num = 0;
	if (cp_.stepCount == 1) {
		pos[0].x = 0;
		pos[0].y = cp_.anchorY;
		pos[1] = pos[0];
		pos[1].x -= cp_.w1;
		pos[2] = pos[1];
		pos[2].y -= cp_.h1;
		pos[3] = pos[2];
		pos[3].x += cp_.w2;
		pos[4] = pos[3];
		if (cp_.n1 > 0) { // 处理有斜坡台面的地方
			pos[4].x += cp_.h2 * cp_.n1;
		}
		pos[4].y -= cp_.h2;
		pos[5] = pos[4];
		pos[5].x = 0;
		num = 6;
	}
	else if (cp_.stepCount == 2) {
		pos[0].x = 0;
		pos[0].y = cp_.anchorY;
		pos[1] = pos[0];
		pos[1].x -= cp_.w1;
		pos[2] = pos[1];
		pos[2].y -= cp_.h1;
		pos[3] = pos[2];
		pos[3].x += cp_.w2;
		pos[4] = pos[3];
		if (cp_.n1 > 0) { // 处理有斜坡台面的地方
			pos[4].x += cp_.h2 * cp_.n1;
		}
		pos[4].y -= cp_.h2;
		pos[5] = pos[4];
		pos[5].x += cp_.w3;
		pos[6] = pos[5];
		if (cp_.n2 > 0) {
			pos[6].x += cp_.h3 * cp_.n2;
		}
		pos[6].y -= cp_.h3;
		pos[7] = pos[6];
		pos[7].x = 0;
		num = 8;
	}
	else if (cp_.stepCount == 3) {
		pos[0].x = 0;
		pos[0].y = cp_.anchorY;
		pos[1] = pos[0];
		pos[1].x -= cp_.w1;
		pos[2] = pos[1];
		pos[2].y -= cp_.h1;
		pos[3] = pos[2];
		pos[3].x += cp_.w2;
		pos[4] = pos[3];
		if (cp_.n1 > 0) { // 处理有斜坡台面的地方
			pos[4].x += cp_.h2 * cp_.n1;
		}
		pos[4].y -= cp_.h2;
		pos[5] = pos[4];
		pos[5].x += cp_.w3;
		pos[6] = pos[5];
		if (cp_.n2 > 0) {
			pos[6].x += cp_.h3 * cp_.n2;
		}
		pos[6].y -= cp_.h3;
		pos[7] = pos[6];
		pos[7].x += cp_.w4;
		pos[8] = pos[7];
		if (cp_.n3 > 0) {
			pos[8].x += cp_.h4 * cp_.n3;
		}
		pos[8].y -= cp_.h4;
		pos[9] = pos[8];
		pos[9].x = 0;
		num = 10;
	}
	else {
		assert(false);
	}
	CreateRegionTool regionTool;
	regionTool.Init();
	for (int index = 0; index < num; index++) {
		if (!isLeft_) {
			pos[index].x *= -1;
		}
		regionTool.AppendKeyPoint(pos[index]);
	}
	AcDbObjectId entId = regionTool.CreateRegion(colorIndex_);
	SetRegionXData(entId, tagRegionXData{cp_.materialID,isLeft_,colorIndex_,owner_section_->GetSideKind(),cp_.Name,owner_section_->stakeNo(),-1 });
	return entId;
}


