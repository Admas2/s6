﻿#pragma once
#include "SectionComponent.h"
class CSideSection
{
public:
	int index_ = -1;
	tagSideSectionData  ssd_;
private:
	AcArray<AcDbObjectId>  acdbRegions_;			//组成截面的面域
public:
	CSideSection(int index, const tagSideSectionData&  ssd);
	~CSideSection();
	double stake_no() const { return ssd_.stakeNo; }
	void correctStake(double stakeNo) { ssd_.stakeNo = stakeNo; }
	void Create();
	void RemoveEntities();
	AcArray<AcDbObjectId> GetRegions() const;
	bool isLeft() const { return ssd_.isLeft; }
	AcDbObjectId GetRegionByIndex(int index) const;
	AcDbObjectId GetRegionByXData(const tagRegionXData& xData) const;
private:
	void BuildDoubleWidening();
	void BuildSingleWidening();
	void BuildTransitionalWidening();
	void BuildNew();
public:
	bool AddLevelingLayer(AcArray<AcDbObjectId>& rects, double totalW, double totalH, eSectionSideKind sideKind);
};


