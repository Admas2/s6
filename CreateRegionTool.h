﻿#pragma once
#include "pch.h"
class CreateRegionTool
{
private:
	AcArray<AcGePoint2d> keyPoints_;
	double stakeNo_;  //桩号
	std::wstring componentName_; //截面组件的名称
	bool lookCurve_ = false;
public:
	void Init(double stakeNo = -1, const std::wstring& componentName = std::wstring());
	void lookCurveForTest() { lookCurve_ = true; }
	void AppendKeyPoint(double x, double y);
	void AppendKeyPoint(AcGePoint2d pos);
	void AppendKeyPoint(AcGePoint3d pos);
	AcDbObjectId CreateRegion(int colorIndex = 7);
};

