﻿#include "global.h"
#include "RawModeDataCheck.h"

bool CheckComponentDivider(const tagComponentParameter& cp,double originalWidth,double totalthickness) {
	if (!(cp.horiOffset >= 0)) {
		logInfo.Format(L"中央分隔带参数不合理：插入点水平方向偏移%.2f 要求>=0", cp.horiOffset);	 MESSAGE_BOX
		return false;
	}

	if (!(cp.horiOffset < originalWidth)) {
		logInfo.Format(L"中央分隔带参数不合理：插入点水平方向偏移%f 超出了路幅%.2f", cp.horiOffset, originalWidth);	 MESSAGE_BOX
		return false;
	}
	return true;
}

bool CheckComponentBench(const tagComponentParameter& cp,double originalWidth,double totalthickness) {
	if (!(cp.horiOffset > 0)) {
		logInfo.Format(L"路面内部台阶参数不合理：插入点水平方向偏移%.2f 要求>0", cp.horiOffset);	 MESSAGE_BOX
			return false;
	}

	if (!(cp.horiOffset < originalWidth)) {
		logInfo.Format(L"路面内部台阶参数不合理：插入点水平方向偏移%f 超出了路幅%.2f", cp.horiOffset, originalWidth);	 MESSAGE_BOX
		return false;
	}
	return true;
}


bool CheckComponentMargin(const tagComponentParameter& cp,double originalWidth,double totalthickness) {

	if (!(cp.horiOffset > 0)) {
		logInfo.Format(L"边部构造组件参数不合理：插入点水平方向偏移%.2f 要求大于0",cp.horiOffset);	 MESSAGE_BOX
		return false;
	}

	if (!(cp.horiOffset < originalWidth)) {
		//tagComponentParameter* cp2 = const_cast<tagComponentParameter*>(&cp);
		//cp2->horiOffset = originalWidth * 0.9;
		logInfo.Format(L"边部构造组件参数不合理：插入点水平方向偏移%.2f 超出了路幅%.2f", cp.horiOffset, originalWidth);	 MESSAGE_BOX
		return false;
	}

	double testH = fabs(cp.anchorY + cp.h1 + cp.h2 + cp.h3 + cp.h4);
	if (!(testH <= totalthickness)) {
		logInfo.Format(L"边部构造组件参数不合理：竖直方向会超出路面结构");	 MESSAGE_BOX
		return false;
	}
	return true;
}

bool CheckComponents(const tagComponentParameter& cp, double originalWidth, double totalthickness) {
	switch (cp.componentKind) {
		case component_kind_divider:		
			if (!CheckComponentDivider(cp, originalWidth, totalthickness)) return false;
			break;
		case component_kind_bench:		
			if (!CheckComponentBench(cp, originalWidth, totalthickness)) return false;
			break;
		case component_kind_margin:		
			if (!CheckComponentMargin(cp, originalWidth, totalthickness)) return false;
			break;
		default: {
			logInfo.Format(L"不可识别的截面组件类型 %d", cp.componentKind); MESSAGE_BOX
			return false;
		}
	}
	return true;
}

bool CheckNewRoadSection(const tagSideSectionData& ssd) {
	switch (ssd.wideningType) {
	case widening_type_double_widening:					//双侧拼宽
			if (!(ssd.dividerBenchParameters.length() > 0)) {
				logInfo.Format(L"双侧过渡拼宽时，没有指定开挖台阶 桩号:%.2f", ssd.stakeNo);		MESSAGE_BOX
				return false;
			}
			if (!(ssd.newRoadType1 != new_road_type_null)) {
				logInfo.Format(L"双侧拼宽时，加铺方式没有指定 桩号:%.2f", ssd.stakeNo);		MESSAGE_BOX
				return false;
			}
			if (!(ssd.overlapArea1.layers.length() > 0)) {
				logInfo.Format(L"双侧拼宽时，没有指定加铺层结构 桩号:%.2f", ssd.stakeNo);		MESSAGE_BOX
				return false;
			}
		break;
	case widening_type_transitional_widening:		//过渡拼宽
		if (ssd.overlapSame) {
			if (!(ssd.dividerBenchParameters.length() == 2)) {
				logInfo.Format(L"同侧过渡拼宽时，没有指定两条开挖台阶 桩号:%.2f", ssd.stakeNo);	MESSAGE_BOX
				return false;
			}
			if (!(ssd.newRoadType1 != new_road_type_null && ssd.newRoadType2 != new_road_type_null)) {
				logInfo.Format(L"同侧过渡拼宽时，要同时指定两个加铺方式 桩号: %d", ssd.stakeNo);	MESSAGE_BOX
				return false;
			}
			if (!(ssd.overlapArea1.layers.length() > 0 && ssd.overlapArea2.layers.length() > 0)) {
				logInfo.Format(L"过渡拼宽同侧，没有指定两个加铺层结构 桩号:%.2f", ssd.stakeNo);		MESSAGE_BOX
					return false;
			}
		}
		else {
			if (!(ssd.dividerBenchParameters.length() == 1)) {
				logInfo.Format(L"异侧过渡拼宽时，只能指定一条开挖台阶 桩号:%.2f",ssd.stakeNo);	 MESSAGE_BOX
				return false;
			}
			if (!(ssd.newRoadType1 != new_road_type_null)) {
				logInfo.Format(L"过渡拼宽时，加铺方式1 桩号:%.2f", ssd.stakeNo);		MESSAGE_BOX
					return false;
			}
			if (!(ssd.overlapArea1.layers.length() > 0)) {
				logInfo.Format(L"过渡拼宽异侧，没有指定一个加铺层结构 桩号:%.2f", ssd.stakeNo);		MESSAGE_BOX
				return false;
			}
		}
		break;
	case widening_type_single_widening:					//单侧拼宽
		if (ssd.overlapSame) {
			if (!(ssd.dividerBenchParameters.length() > 0)) {
				logInfo.Format(L"同侧单侧拼宽时，没有指定开挖台阶 桩号:%.2f", ssd.stakeNo);	MESSAGE_BOX
				return false;
			}
			if (!(ssd.newRoadType1 != new_road_type_null && ssd.newRoadType2 != new_road_type_null)) {
				logInfo.Format(L"同侧单侧拼宽时，要同时指定两个加铺方式 桩号: %d", ssd.stakeNo);	MESSAGE_BOX
				return false;
			}
			if (!(ssd.overlapArea1.layers.length() > 0 && ssd.overlapArea2.layers.length() > 0)) {
				logInfo.Format(L"过同侧单侧拼宽时，没有指定两个加铺层结构 桩号:%.2f", ssd.stakeNo);		MESSAGE_BOX
					return false;
			}
		}
		break;
	case widening_type_build_new:								//建新路
		break;
	default:	assert(false);
	}
	return true;
}

bool CheckSection(const tagSideSectionData& ssd) {
	if (!(ssd.componentParameters.length() <= 3)) {
		logInfo.Format(L"桩号:%.2f的截面中有多于3种的组件，目前截面中只支持最多三个组件", ssd.stakeNo); MESSAGE_BOX
		return false;
	}
	if (!(ssd.pavementWidth.length() > 0)) {
		logInfo.Format(L"桩号:%.2f的截面中缺少路幅信息", ssd.stakeNo);	 MESSAGE_BOX
		return false;
	}

	if (!(ssd.TotalThickness() > 0 && ssd.OriginalWidth() > 0)) {
		logInfo.Format(L"路幅信息不正确");	MESSAGE_BOX
		return false;
	}
	for (int index = 0; index < ssd.pavementWidth.length(); index++) {
		if (ssd.isLeft) {
			if (!(ssd.pavementWidth[index].SortID < 0)) {
				logInfo.Format(L"左侧路幅信息不正确 SortID: %d", ssd.pavementWidth[index].SortID);	MESSAGE_BOX
				return false;
			}
		}
		else {
			if (!(ssd.pavementWidth[index].SortID > 0)) {
				logInfo.Format(L"右侧路幅信息不正确 SortID: %d", ssd.pavementWidth[index].SortID); MESSAGE_BOX
				return false;
			}
		}
	}

	if (!(ssd.pavamentStruct.length() > 0)) {
		logInfo.Format(L"桩号:%.2f的截面中缺少路面结构信息", ssd.stakeNo);	MESSAGE_BOX
		return false;
	}
	
	for (int index = 0; index < ssd.componentParameters.length(); index++) {
		if (!CheckComponents(ssd.componentParameters[index],ssd.OriginalWidth(),ssd.TotalThickness() )) return false;
	}
	
	if (ssd.isNewRoad) {
		if (!CheckNewRoadSection(ssd)) return false;
	}
	
	return true;
}

bool CheckRawModelData(const tagRoadwayData& rd) {
	//1.
	int pt_num = rd.fitPoints.length();
	if (!(pt_num > 1)) {
		logInfo.Format(L"只有%4d个桩号,太少,至少要两个桩号", pt_num); MESSAGE_BOX
		return false;
	}

	for (int index = 0; index < pt_num; index++) {
		if (rd.fitPoints[index].vecTagent.length() == 0) {
			logInfo.Format(L"程序内部错误： 没有正确计算出中线切向方向");	 MESSAGE_BOX
			return false;
		}
	}

	//2.检查桩号范围
	if (!(rd.startStake < rd.endStake)) {
		logInfo.Format(L"指定桩号范围不合理:起始桩号:%.2f   终止桩号::%.2f", rd.startStake, rd.endStake);	 MESSAGE_BOX
		return false;
	}
	
	if (!(rd.fitPoints[0].stakeNo <= rd.startStake && rd.startStake <= rd.fitPoints[pt_num-1].stakeNo)) {
		logInfo.Format(L"起始桩号:%.2f不在线路范围[%.2f,%.2f]中", rd.startStake, rd.fitPoints[0].stakeNo, rd.fitPoints[pt_num-1].stakeNo);		MESSAGE_BOX
		return false;
	}

	if (!(rd.fitPoints[0].stakeNo <= rd.endStake && rd.endStake <= rd.fitPoints[pt_num - 1].stakeNo)) {
		logInfo.Format(L"结束桩号:%.2f不在线路范围[%.2f,%.2f]中", rd.endStake, rd.fitPoints[0].stakeNo, rd.fitPoints[pt_num - 1].stakeNo);	MESSAGE_BOX
		return false;
	}

	//3.检查截面参数
	if (rd.sections.length() == 0) {
		logInfo.Format(L"没有指定道路截面");	 MESSAGE_BOX
		return false;
	}

	for (int index = 0; index < rd.sections.length(); index++) {
		if (!CheckSection(rd.sections[index])) {
			return false;
		}
	}
	return true;
}