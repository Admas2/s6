﻿#include "pch.h"
#include "Roadway.h"
#include "CreateRegionTool.h"
#include "BooleanOperation.h"
#include "DataBaseOpr.h"
#include "global.h"
#include "EntityXData.h"
#include "RawModeDataCheck.h"
#include "LayerOperation.h"
#include "StringTransfer.h"


Acad::ErrorStatus es;
AcArray<AcArray<tagPavementLayer>>	gPredefinedPavementStructureList; //预定义路面结构集
AcArray<AcArray<tagPavementSegment>> gPredefinedPavementWidthList; //预定义路面结构集
tagOptions  gOptions;
CString logInfo;


AcDbObjectId CreateLoftedBodyWidthCircleSection(double radius,AcGePoint3dArray path) {
	assert(path.length() > 3);

	//1.创建每一个圆截面
	AcGeVector3d vec = path[0] - path[1];
	AcDbCircle *pFirstCircle = new AcDbCircle(path[0], vec, radius);
	AppendEntityToModelSpace(pFirstCircle, layer_Sections);

	//2.创建最后一个圆截面
	int last_index = path.length() - 1;
	vec = path[last_index - 1] - path[last_index];
	AcDbCircle *pLastCircle = new AcDbCircle(path[last_index], vec, radius);
	AppendEntityToModelSpace(pLastCircle, layer_Sections);

	//3.创建放样路径样条线
	AcDbEntity* pPathCurve = new AcDbSpline(path, 4, 0.0);
	AppendEntityToModelSpace(pPathCurve, layer_Sections);

	//4.创建放样体
	AcArray<AcDbEntity*> sectionRegions;
	sectionRegions.append(pFirstCircle);
	sectionRegions.append(pLastCircle);
	AcArray<AcDbEntity*> guideCurves;
	AcDbLoftOptions loftOptions;
	AcDb3dSolid *pSolid = new AcDb3dSolid();
	es = pSolid->createLoftedSolid(sectionRegions, guideCurves, pPathCurve, loftOptions);
	assert(es == Acad::eOk);
	AcDbObjectId result = AppendEntityToModelSpace(pSolid, layer_Other);
	return result;
}


//在指定的位置指定朝向上创建指定区域对象
AcDbRegion* CreateRegionAtDirectionPosition(AcGePoint3d position, AcGeVector3d normal, AcGeVector3d xAixs, AcDbRegion* pRefRegion) {

	//1.复制一个截面实例
	AcRxObject* pObject = pRefRegion->clone();
	assert(pObject->isKindOf(AcDbRegion::desc()));
	AcDbRegion* pSection = AcDbRegion::cast(pObject);

	//2. 设置截面方向
	AcGeVector3d yAixs = normal.crossProduct(xAixs);
	AcGeMatrix3d mat;
	mat.setCoordSystem(position, xAixs, yAixs, normal);
	pSection->transformBy(mat);

	return pSection;
}

AcDbObjectId AppendEntityToModelSpace(AcDbEntity* pEntity) {
	AcDbBlockTable *pBlockTable;
	es = acdbHostApplicationServices()->workingDatabase()->getBlockTable(pBlockTable, AcDb::kForRead);
	assert(es == Acad::eOk);
	AcDbBlockTableRecord *pBlockTableRecord;
	es = pBlockTable->getAt(ACDB_MODEL_SPACE, pBlockTableRecord, AcDb::kForWrite);
	assert(es == Acad::eOk);
	AcDbObjectId entId;
	es = pBlockTableRecord->appendAcDbEntity(entId, pEntity);
	//assert(es == Acad::eOk);
	pEntity->close();
	pBlockTable->close();
	pBlockTableRecord->close();
	return entId;
}

AcDbObjectId AppendEntityToModelSpace(AcDbEntity* pEntity, eLayerID layerIndex)
{
	Acad::ErrorStatus es;
	if (0 <= layerIndex && layerIndex < gOptions.layers.length()) {
		es = pEntity->setLayer(gOptions.layers[layerIndex]);
		assert(es == Acad::eOk);
	}
	else {
		acutPrintf(L"");
	}
	
	return AppendEntityToModelSpace(pEntity);
}


void RemoveEntityFromModelSpace(AcDbObjectId entityID) {
	if (entityID.isNull()) return;
	AcDbEntity *pEntity = nullptr;
	es = acdbOpenAcDbEntity(pEntity, entityID, AcDb::kForWrite);
	if (es == Acad::eOk && pEntity) {
		pEntity->erase();
		pEntity->close();
	}
}

//对于面域就是计算面积和包围盒
//对于三维实体就是计算体积和包围盒
Acad::ErrorStatus CalculateEntityAreaOrVolumeExtents(AcDbEntity* pEntity, double& areaOrVolume, AcDbExtents& extents) {
	es = Acad::eNullEntityPointer;
	if (pEntity == nullptr) return es;
	
	if (pEntity->isKindOf(AcDbRegion::desc())) {
		AcDbRegion* pRegion = AcDbRegion::cast(pEntity);
		pRegion->getArea(areaOrVolume);
		pRegion->getGeomExtents(extents);
	}

	if (pEntity->isKindOf(AcDb3dSolid::desc()))
	{
		AcDb3dSolid* pSolid = AcDb3dSolid::cast(pEntity);
		AcGePoint3d centroid;				//质心
		double momInertia[3];				//X,Y,Z三轴的惯性矩
		double prodInertia[3];			//X,Y,Z三轴的惯性积
		double prinMoments[3];			//主力矩
		double radiiGyration[3];		//旋转半径
		AcGeVector3d prinAxes[3];	  //旋转主轴
		es = pSolid->getMassProp(areaOrVolume, centroid, momInertia, prodInertia, prinMoments, prinAxes, radiiGyration, extents);
	}

	return es;
}

AcGePoint3d GetNearistPt(AcGePoint3d ptS, const AcGePoint3dArray& pts) {
	assert(pts.length() > 0);
	double dist = 1000000000;
	AcGePoint3d result = pts[0];
	for (AcGePoint3d pt : pts) {
		if (ptS.distanceTo(pt) < dist) {
			dist = ptS.distanceTo(pt);
			result = pt;
		}
	}
	return result;
}

void CalculateGuideLinesOf2Region(AcDbEntity* pEntity1, AcDbEntity* pEntity2, AcArray<AcDbEntity*>& guideCurves) {
	assert(pEntity1->isKindOf(AcDbRegion::desc()));
	assert(pEntity2->isKindOf(AcDbRegion::desc()));
	AcDbRegion* pRegion1 = AcDbRegion::cast(pEntity1);
	AcDbRegion* pRegion2 = AcDbRegion::cast(pEntity2);
	AcGePoint3dArray ptsSmall(50, 50);
	AcGePoint3dArray ptsMuch(50, 50);
	AcGePoint3dArray ptsMid(50, 50);
	es = pRegion1->getStretchPoints(ptsSmall);
	es = pRegion2->getStretchPoints(ptsMuch);
	if (ptsSmall.length() > ptsMuch.length()) {
		ptsMid.removeAll();
		ptsMid.append(ptsSmall);
		ptsSmall.removeAll();
		ptsSmall.append(ptsMuch);
		ptsMuch.removeAll();
		ptsMuch.append(ptsMid);
	}

	for (AcGePoint3d ptStart : ptsMuch) {
		AcGePoint3d ptEnd = GetNearistPt(ptStart, ptsSmall);
		AcDbLine* pLine = new AcDbLine(ptStart, ptEnd);
		guideCurves.append(pLine);
	}
}

double CalculateBodyVolumn(AcDbObjectId entityID) {
	if (entityID.isNull()) return 0.0;

	double volume = 0.0;				//体积
	AcDbExtents extents;			  //边界框（实体包含矩形盒，包围盒）
	AcDbEntity* pEnt = nullptr;
	es = acdbOpenAcDbEntity(pEnt, entityID, AcDb::kForRead);
	
	if (es == Acad::eOk) {
		AcDb3dSolid* pSolid = AcDb3dSolid::cast(pEnt);
		es = CalculateEntityAreaOrVolumeExtents(pSolid, volume,extents);
		pEnt->close();
	}
	return volume;
}
void SaveAsFBX(ACHAR* file_name, AcArray<AcDbObjectId> bodies) {
	//pSolid->stlOut(file_name,true);
	AcDbEntity* pEnt = nullptr;
	for (int index = 0; index < bodies.length(); index++) {
		acdbOpenAcDbEntity(pEnt, bodies[index], AcDb::kForRead);
		AcDb3dSolid* pSolid = AcDb3dSolid::cast(pEnt);

		pSolid->close();
	}
}

void MoveToViewCenter(AcArray<tagFitPoint>& fitPoints) {
	double xMin, xMax, yMin, yMax, zMin, zMax;
	xMin = xMax = fitPoints[0].x;
	yMin = yMax = fitPoints[0].y;
	zMin = zMax = fitPoints[0].z;
	for (int index = 1; index < fitPoints.length(); index++) {
		xMax = max(fitPoints[index].x, xMax);
		xMin = min(fitPoints[index].x, xMin);
		yMax = max(fitPoints[index].y, yMax);
		yMin = min(fitPoints[index].y, yMin);
		zMax = max(fitPoints[index].z, zMax);
		zMin = min(fitPoints[index].z, zMin);
	}
	double xRef = (xMax + xMin) / 2.0;
	double yRef = (yMax + yMin) / 2.0;
	double zRef = (zMax + zMin) / 2.0;
	for (int index = 0; index < fitPoints.length(); index++) {
		fitPoints[index].x -= xRef;
		fitPoints[index].y -= yRef;
		fitPoints[index].z -= zRef;
	}
}

bool ReadRoadDataFromDB(tagRoadwayData& rd ) {

	CDataBaseOpr db;
	bool r = db.Open();
	if (!r) return false;

	rd.roadLineID = db.QueryRoadLineID(Unicode2Utf8(rd.RoadLine_Name).c_str());
	if (rd.roadLineID.empty()) {
		logInfo.Format(L"线路%s没有查到ID号", rd.RoadLine_Name.c_str()); MESSAGE_BOX
		return false;
	}
	rd.fitPoints = db.QueryRoadLineCoordinate(rd.roadLineID, rd.startStake, rd.endStake);
	if (rd.fitPoints.isEmpty()) {
		logInfo.Format(L"线路%s没有查到坐标值", rd.RoadLine_Name.c_str()); MESSAGE_BOX
		return false;
	}
	
	// 移动到视图中心，仅为了方便观察
	if (gOptions.isMoveToViewCenter) {
		MoveToViewCenter(rd.fitPoints); 
	}


	rd.allNoModelInervals = db.QueryRoadNoModelInervals(rd.roadLineID, rd.startStake, rd.endStake);
	rd.modelSegments = db.QueryRoadModelSegments(rd.roadLineID, rd.startStake, rd.endStake);
	if (rd.isNewRoad) {
		rd.sections = db.QueryNewRoadSections(rd.roadLineID, rd.startStake, rd.endStake);
	}
	else {
		rd.sections = db.QueryOldRoadSections(rd.roadLineID, rd.startStake, rd.endStake);
	}
	if (rd.sections.isEmpty()) {
		logInfo.Format(L"线路%s [%.2f---%.2f]没有查到截面信息", rd.RoadLine_Name.c_str(), rd.startStake, rd.endStake); MESSAGE_BOX
		return false;
	}
	return CheckRawModelData(rd);
}

void calcRoadLineVecTagent(AcArray<tagFitPoint>& roadLine) {
	if (roadLine.length() < 2) return;
	AcGePoint3dArray positions;
	for (int index = 0; index < roadLine.length(); index++) {
		positions.append(AcGePoint3d(roadLine[index].x, roadLine[index].y, roadLine[index].z));
	}
	for (int index = 0; index < positions.length(); index++) {
		if (index == 0)
		{
			roadLine[index].vecTagent = positions[0] - positions[1];
		}
		else {
			roadLine[index].vecTagent = positions[index - 1] - positions[index];
		}
		roadLine[index].vecTagent = roadLine[index].vecTagent.normalize();
	}
}

// 创建多横条矩形带，再对每一段按不同斜率作坚向错切变形
// 输入参数： |segmentNum| 段数，|slops| 记录了每一段的横向坡率
//           |segmentWidth| 每一段的原始宽度
//           |rowNum| 行数  |isLeft| 是在Y轴左还是在右
AcArray<AcDbObjectId> CreateMultiRectWithSlopRegion(int segmentNum, int rowNum, double segmentWidth[], double rowHeight[],int rowMaterialID[], double slops[], 
																										bool isLeft,int colorIndexStart, eSectionSideKind  sideKind,double stakeNo) {
	AcArray<AcDbObjectId> result;
	for (int j = 0; j < rowNum; j++) {
		double deltaH = 0.0;
		double hp = 0.0;
		for (int s = 0; s < j; s++) {
			hp += rowHeight[s];
		}
		AcDbObjectId rowRegionID;
		for (int i = 0; i < segmentNum; i++) {
			double wp = 0.0;
			for (int s = 0; s < i; s++) {
				wp += segmentWidth[s];
			}
			AcGePoint2d p1(wp, -hp);
			AcGePoint2d p2(wp + segmentWidth[i], -hp);
			AcGePoint2d p3(wp + segmentWidth[i], -hp - rowHeight[j]);
			AcGePoint2d p4(wp, -hp - rowHeight[j]);
			if (isLeft) {
				p1.x *= -1; p2.x *= -1; p3.x *= -1; p4.x *= -1;
			}

			p1.y += deltaH;
			p4.y += deltaH;
			deltaH += segmentWidth[i] * slops[i];
			p2.y += deltaH;
			p3.y += deltaH;

			CreateRegionTool regionTool;
			regionTool.Init();
			regionTool.AppendKeyPoint(p1);
			regionTool.AppendKeyPoint(p2);
			regionTool.AppendKeyPoint(p3);
			regionTool.AppendKeyPoint(p4);
			AcDbObjectId regionID = regionTool.CreateRegion(colorIndexStart + j);
			if(!regionID.isNull()) UniteRegion(rowRegionID, regionID);
		}
		if (!rowRegionID.isNull()) 	result.append(rowRegionID);
		CString name;
		name.Format(L"路面结构层,第%d层", j + 1);
		SetRegionXData(rowRegionID, tagRegionXData{ rowMaterialID[j],isLeft,colorIndexStart + j,sideKind,name,stakeNo,rowHeight[j]});
	}
	return result;
}

//切割一个体，原体保留
AcDbObjectId SliceABody(AcDbObjectId bodyID,AcGePoint3d point, AcGeVector3d dir) {
	AcDbEntity* pEntity = nullptr;
	es = acdbOpenAcDbEntity(pEntity, bodyID, AcDb::kForWrite);
	AcDb3dSolid *pSolid2 = AcDb3dSolid::cast(pEntity->clone());
	AcGePlane plane(point, dir);
	Adesk::Boolean getNegHalfToo = true;
	AcDb3dSolid* negHalfSolid = nullptr;
	es = pSolid2->getSlice(plane, getNegHalfToo, negHalfSolid);
	AcDbObjectId id = AppendEntityToModelSpace(pSolid2, layer_Other);
	pEntity->close();
	return id;
}

bool MySetStatusText(LPCTSTR lpszText)
{
	int minWidth = 0;
	int maxWidth = 0;
	if (lpszText != NULL)
	{
		CPaintDC dc(acedGetAcadFrame());
		CSize size = dc.GetTextExtent(lpszText);
		maxWidth = size.cx;
	}

	AcApStatusBar* pStatusBar = acedGetApplicationStatusBar();
	AcPane* pPane = pStatusBar->GetPane(2);
	if (lpszText == NULL)
	{
		pPane->SetVisible(FALSE);
	}
	else
	{
		pPane->SetVisible(TRUE);
		pPane->SetMinWidth(minWidth);
		pPane->SetMaxWidth(maxWidth);
		pPane->SetText(lpszText);
	}
	pStatusBar->Update();

	return true;
}

double CalcStartDeltahHeight(double horiOffset, const AcArray<tagPavementSegment>& pw) {
	if (pw.isEmpty()) return 0.0;
	assert(horiOffset > 0);
	double  s = 0.0;
	double  deltaH = 0.0;
	for (int index = 0; index < pw.length(); index++) {
		s += pw[index].width;
		if (horiOffset > s) {
			deltaH += pw[index].width *	pw[index].CrossSlop;
		}
		else {
			s -= pw[index].width;
			deltaH += (horiOffset - s) * pw[index].CrossSlop;
			break;
		}
	}
	return deltaH;
}

void TagAPoint(AcGePoint3d pt, int colorIndex, double radius) {
	AcDbCircle *pCircle = new AcDbCircle(pt, AcGeVector3d(0, 0, 1), radius);
	pCircle->setColorIndex(colorIndex);
	AppendEntityToModelSpace(pCircle, layer_Sections);
}

//向点集中添加一个点，但如果已经存在就不加了
void AddPtWithUniqueness(AcGePoint3dArray& points, AcGePoint3d pt) {
	for (int index = 0; index < points.length(); index++) {
		if (points[index].distanceTo(pt) < 0.0001)
			return;
	}
	points.append(pt);
}

void AddPtWithUniqueness(AcGePoint3dArray& dest, const AcGePoint3dArray& src) {
	for (int index = 0; index < src.length(); index++) {
		AddPtWithUniqueness(dest, src[index]);
	}
}

void LineInstersectRects(AcDbObjectId lineID, AcArray<AcDbObjectId> rects, AcGePoint3dArray& result) {
	AcGePoint3dArray points(50,50);
	AcDbEntity* pEntity1 = nullptr;
	es = acdbOpenAcDbEntity(pEntity1, lineID, AcDb::kForWrite);
	AcDbLine* pLine = AcDbLine::cast(pEntity1);
	for (int index = 0; index < rects.length(); index++) {
		AcDbEntity* pEntity2 = nullptr;
		es = acdbOpenAcDbEntity(pEntity2, rects[index], AcDb::kForWrite);
		points.removeAll();
		es = pLine->intersectWith(pEntity2, AcDb::Intersect::kOnBothOperands, points);
		if (es == Acad::eOk) {
			AddPtWithUniqueness(result, points);
			pEntity2->close();
		}
	}
	pEntity1->close();
}

AcGePoint3d GetMaxYPoint(AcGePoint3dArray points) {
	assert(points.length() > 0);
	int pMaxIndex = 0;
	for (int index = 1; index < points.length(); index++) {
		if (points[index].y > points[pMaxIndex].y) {
			pMaxIndex = index;
		}
	}
	return points[pMaxIndex];
}

int qsortCompare2(const void * elem1, const void * elem2) {
	tagPointPair* ps1 = (tagPointPair*)(elem1);
	tagPointPair* ps2 = (tagPointPair*)(elem2);
	return (fabs(ps1->pt1.x) - fabs(ps2->pt1.x));
}

void RemoveEntityFromModelSpace(AcArray<AcDbObjectId> IDs) {
	for (int index = 0; index < IDs.length(); index++) {
		RemoveEntityFromModelSpace(IDs[index]);
	}
}

//从点集中查询距离最大的两个点，结果记录在|ptFarOfOrigin|和|ptNearOfOrigin|
//其中|ptFarOfOrigin|离原点远，|ptNearOfOrigin|离原点近
bool GetMaxDistancePt(const AcGePoint3dArray& points, AcGePoint3d& ptFarOfOrigin, AcGePoint3d& ptNearOfOrigin) {
	if (points.length() < 2) return false;

	double maxDist = 0.0;
	int ri = 0;
	for (int index = 0; index < points.length() - 1; index++) {
		double d = points[index].distanceTo(points[index + 1]);
		if (d > maxDist) {
			maxDist = d;
			ri = index;
		}
		//TRACE("(%.2f,%.2f) --- (%.2f,%.2f)  dist: %.2f\n", points[index].x, points[index].y);
	}

	ptFarOfOrigin = points[ri];
	ptNearOfOrigin = points[ri + 1];
	if (ptFarOfOrigin.distanceTo(AcGePoint3d(0, 0, 0)) < ptNearOfOrigin.distanceTo(AcGePoint3d(0, 0, 0))) {
		swap(ptFarOfOrigin, ptNearOfOrigin);
	}
	return true;
}

AcGePoint3d GetExtentsCenter(AcDbExtents extents) {
	AcGePoint3d centerPoint;
	centerPoint.x = (extents.maxPoint().x + extents.minPoint().x) / 2.0;
	centerPoint.y = (extents.maxPoint().y + extents.minPoint().y) / 2.0;
	centerPoint.z = (extents.maxPoint().z + extents.minPoint().z) / 2.0;
	return centerPoint;
}

double GetExtentsRadius(AcDbExtents extents) {
	double r = GetExtentsCenter(extents).distanceTo(extents.minPoint());
	return r;
}

bool DetermineNonIntersect(AcDbExtents extents1, AcDbExtents extents2) {
	double r1 = GetExtentsRadius(extents1);
	double r2 = GetExtentsRadius(extents2);
	double d = GetExtentsCenter(extents1).distanceTo(GetExtentsCenter(extents2));
	return (r1 + r2) < d;
}


