﻿#include "pch.h"
#include <math.h>
#include "global.h"


Acad::ErrorStatus CreateRegion(const AcDbObjectIdArray& curveIds,AcDbObjectIdArray& regionIds)
{
		Acad::ErrorStatus es;
		{
			//注意： 本来应该 定义为：AcDbVoidPtrArray regions
			//  但是 regions对象在析构时会发生崩溃，原因一直没有找到，这里暂时用这种避免析构的方式
			AcDbVoidPtrArray& regions = *(new AcDbVoidPtrArray);
			{
				//1.取出输入的区域的边界曲线=> curves
				AcDbVoidPtrArray curves;
				for (int i = 0; i < curveIds.length(); i++)
				{
					AcDbEntity *pEnt = nullptr;
					acdbOpenAcDbEntity(pEnt, curveIds.at(i), AcDb::kForRead);
					if (pEnt->isKindOf(AcDbCurve::desc()))
					{
						curves.append(static_cast<void*>(pEnt));
					}
				}

				//2.从曲线集中创建一个区域
				es = AcDbRegion::createFromCurves(curves, regions);

				//3.释放掉这个curves
				for (int i = 0; i < curves.length(); i++)
				{
					AcDbEntity *pEnt = nullptr;
					pEnt = static_cast<AcDbEntity*>(curves[i]);
					pEnt->close();
				}
				curves.removeAll();
			}

			//3.将创建的区域加入数据库中去
			for (int i = 0; i < regions.length(); i++)
			{
				AcDbRegion *pRegion = static_cast<AcDbRegion*>(regions[i]);
				pRegion->setDatabaseDefaults();  //取得当前数据库的线型，着色等默认值
				AcDbObjectId regionId;
				regionId = AppendEntityToModelSpace(pRegion);
				regionIds.append(regionId);
			}
		}
		return es;
}

double f(double x) {
	return 77 * sin(x);
}

double g(double x) {
	return 33 * cos(x);
}

void CommandCreateLoft2() {

	//1. 生成一条螺旋样条曲线作为路径
	AcGePoint3dArray points;
	double x = 0.0;
	const double dbStep = 3.1415926 / 8;
	
	AcGePoint3d ptCenter;
	for (int index = 0; index < 50; index++) {
		x += dbStep;
		double y = 77 * sin(15 * x);
		double z = 33 * cos(15 * x);
		ptCenter.set(15 * x, y, z);
		points.append(ptCenter);
	}
	int order = 4;
	double fitTolerance = 0.0;
	AcDbSpline* pSpline = new AcDbSpline(points, order, fitTolerance);
	AppendEntityToModelSpace(pSpline);

	//2.
	static double angle = 5.5;
	AcArray<AcDbEntity*> sectionRegions;
	AcDbCurve* pCurve = nullptr;
	es = pSpline->toPolyline(pCurve);
	assert(es == Acad::eOk);
	if (pCurve->isKindOf(AcDb3dPolyline::desc())) {
		acutPrintf(L"\n AcDb3dPolyline::desc()!");
		AcDb3dPolyline* p3dPolyline = AcDb3dPolyline::cast(pCurve);
		int index = 1;
		AcGePoint3d prePoint;
		AcGeVector3d preVec;
		for (AcDbObjectIterator* it = p3dPolyline->vertexIterator(); !it->done(); it->step()) {
			AcDbEntity* pEnt = it->entity();
			AcRxClass*  rx_class = pEnt->isA();
			const ACHAR* s = rx_class->name();
			//acutPrintf(L"\n AcDbPolyline::desc()! %s ", s);
			if (pEnt->isKindOf(AcDb3dPolylineVertex::desc())) {
				AcDb3dPolylineVertex* vertex = AcDb3dPolylineVertex::cast(pEnt);
				if (index > 1) {
					AcGeVector3d vec = prePoint - vertex->position();
					vec = vec.normalize();
					if (index > 2) {
						double t = CalculateAngleOfTwoVector(preVec, vec);
						if (t > angle) {
							//acutPrintf(L"\n (%d) (%5.3f,%5.3f,%5.3f) %d \n", index, vec.x, vec.y, vec.z);
							AcDbCircle *pCircle = new AcDbCircle(vertex->position(), vec, 10);
							AcDbObjectId circleId = AppendEntityToModelSpace(pCircle);
							sectionRegions.append(pCircle);
							acutPrintf(L"\n (%d)--(%d) Angle of two vector: %6.2f\n", index, sectionRegions.length(), t);
						}
					}
					preVec = vec;
				}
				prePoint = vertex->position();
					
				index++;
			}
		}
	}
	AppendEntityToModelSpace(pCurve);

	//生成放样体
	AcArray<AcDbEntity*> guideCurves;
	AcDbEntity* pPathCurve = nullptr;
	AcDbLoftOptions loftOptions;

	AcDb3dSolid *pSolid = new AcDb3dSolid();
	es = pSolid->createLoftedSolid(sectionRegions, guideCurves, pPathCurve, loftOptions);
	if (es == Acad::eOk) {
		AppendEntityToModelSpace(pSolid);
	}
	else {
		acutPrintf(L"\n create loft failre");
	}
}


void CommandCreateLoft3() {

	//1. 生成一条螺旋样条曲线作为路径
	AcGePoint3dArray points;
	double x = 0.0;
	const double dbStep = 3.1415926 / 8;
	AcGePoint3d ptCenter;
	for (int index = 0; index < 50; index++) {
		x += dbStep;
		double y = 77 * sin(15 * x);
		double z = 33 * cos(15 * x);
		ptCenter.set(15 * x, y, z);
		points.append(ptCenter);
	}

	#if true
		CreateLoftedBodyWidthCircleSection(10, points);
	#else
	//2. 拟合成样条线
	int order = 4;
	double fitTolerance = 0.0;
	AcDbSpline* pSpline = new AcDbSpline(points, order, fitTolerance);
	AppendEntityToModelSpace(pSpline);

	//3.生成一个截面圆
	int index = 0;
	AcGeVector3d vec = points[index + 1] - points[index];
	AcDbCircle *pCircle = new AcDbCircle(points[index], vec, 10);
	AppendEntityToModelSpace(pCircle);

	index = 15;
	AcGeVector3d vec2 = points[index + 1] - points[index];
	AcDbCircle *pCircle2 = new AcDbCircle(points[index], vec, 10);
	AppendEntityToModelSpace(pCircle2);

	//4.生成放样体
	AcArray<AcDbEntity*> sectionRegions;
	sectionRegions.append(pCircle);
	sectionRegions.append(pCircle2);
	AcArray<AcDbEntity*> guideCurves;
	AcDbEntity* pPathCurve = nullptr;
	pPathCurve = pSpline;
	AcDbLoftOptions loftOptions;

	AcDb3dSolid *pSolid = new AcDb3dSolid();
	espSolid->createLoftedSolid(sectionRegions, guideCurves, pPathCurve, loftOptions);
	if (es == Acad::eOk) {
		AppendEntityToModelSpace(pSolid);
	}
	else {
		acutPrintf(L"\n create loft failre");
	}
	#endif
}

void CommandCreateLoft() {

	AcGeVector3d vec(0, 1, 0);  	 	// 圆所在平面的法矢量 
	AcGePoint3d ptCenter1(30, 0, 0);  	// 圆心位置与半径的大小有关 
	AcDbCircle *pCircle1 = new AcDbCircle(ptCenter1, vec, 3);
	AcDbObjectId circleId1 = AppendEntityToModelSpace(pCircle1);

	AcGePoint3d ptCenter2(30, 6, 0);  
	AcDbCircle *pCircle2 = new AcDbCircle(ptCenter2, vec, 7);
	AcDbObjectId circleId2 = AppendEntityToModelSpace(pCircle2);

	AcGePoint3d ptCenter3(30, 21, 0);
	AcDbCircle *pCircle3 = new AcDbCircle(ptCenter3, vec, 4);
	AcDbObjectId circleId3 = AppendEntityToModelSpace(pCircle3);

	AcArray<AcDbEntity*> sectionRegions;
	sectionRegions.append(pCircle1);
	sectionRegions.append(pCircle2);
	sectionRegions.append(pCircle3);
	AcArray<AcDbEntity*> guideCurves;
	AcDbEntity* pPathCurve = nullptr;
	AcDbLoftOptions loftOptions;

	AcDb3dSolid *pSolid = new AcDb3dSolid();
	es = pSolid->createLoftedSolid(sectionRegions, guideCurves, pPathCurve, loftOptions);
	if (es == Acad::eOk) {
		AppendEntityToModelSpace(pSolid);
	}
	else {
		acutPrintf(L"\n create loft failre");
	}
}

void CommandCreateSpire()
{
	//指定创建螺旋线的参数 
	double radius, deltaVertical; // 半径和每一周在垂直方向的增量  
	double number, segment;			// 螺旋线的旋转圈数和组成一圈的分段数
	radius = 30, deltaVertical = 12;
	number = 5, segment = 30;

	// 计算点的个数和角度间隔 
	int n = (int)(number * segment);						// 点的个数实际上是n+1  	  	
	double angle = 8 * atan(1) / segment; // 两点之间的旋转角度 

	// 计算控制点的坐标 
	AcGePoint3dArray points; // 控制点坐标数组  
	for (int i = 0; i < n + 1; i++)
	{
		AcGePoint3d vertex;
		vertex[X] = radius * cos(8 * i * atan(1) / segment);
		vertex[Y] = radius * sin(8 * i * atan(1) / segment);
		vertex[Z] = i * deltaVertical / segment;
		points.append(vertex);
	}

	// 创建螺旋线路径 
	AcDb3dPolyline *p3dPoly = new	AcDb3dPolyline(AcDb::k3dSimplePoly, points);

	// 将路径添加到模型空间 
	AcDbObjectId spireId = AppendEntityToModelSpace(p3dPoly);

	// 创建一个圆作为拉伸的截面 
	AcGeVector3d vec(0, 1, 0);  	 	// 圆所在平面的法矢量 
	AcGePoint3d ptCenter(30, 0, 0);  	// 圆心位置与半径的大小有关 
	AcDbCircle *pCircle = new AcDbCircle(ptCenter, vec, 3);
	AcDbObjectId circleId = AppendEntityToModelSpace(pCircle);

	// 根据圆创建一个面域 
	AcDbObjectIdArray boundaryIds;
	boundaryIds.append(circleId);
	AcDbObjectIdArray regionIds;
	CreateRegion(boundaryIds,regionIds);

	// 打开拉伸截面和拉伸路径 
	AcDbRegion *pRegion = nullptr;
	acdbOpenObject(pRegion, regionIds.at(0), AcDb::kForRead);
	AcDb3dPolyline *pPoly = nullptr;
	acdbOpenObject(pPoly, spireId, AcDb::kForRead);

	// 进行拉伸操作 
	AcDb3dSolid *pSolid = new AcDb3dSolid();
	pSolid->extrudeAlongPath(pRegion, pPoly);
	AppendEntityToModelSpace(pSolid);

	pPoly->close();
	pRegion->close();
}

void CommandCreateCylinder() {
	AcDb3dSolid* pSolid = new AcDb3dSolid();
	pSolid->createFrustum(/*height*/ 30,/*xRadius*/  10, /*yRadius*/ 10, /*topXRadius*/ 0);
	AppendEntityToModelSpace(pSolid);
}

void CommandBooleanOperation()
{
	// 创建两个长方体 
	AcDb3dSolid *pSolid1 = new AcDb3dSolid();
	pSolid1->createBox(40, 50, 30);
	AcDb3dSolid *pSolid2 = new AcDb3dSolid();
	pSolid2->createBox(40, 50, 30);

	// 使用几何变换矩阵移动长方体 
	AcGeMatrix3d xform;
	AcGeVector3d vec(30, 55, 45);
	xform.setToTranslation(vec);
	pSolid1->transformBy(xform);

	// 将长方体添加到模型空间 
	AcDbObjectId solidId1 = AppendEntityToModelSpace(pSolid1);
	AcDbObjectId solidId2 = AppendEntityToModelSpace(pSolid2);
	AcArray<AcDbObjectId> bodiesA, bodiesB;
	bodiesA.append(solidId1);
	bodiesB.append(solidId2);
	AcArray<AcDbObjectId> bodiesC = BoydiesBooleanOpr(bodiesA, bodiesB, AcDb::kBoolSubtract);
	AcDbEntity* pEntity3 = nullptr;
	es = acdbOpenObject(pEntity3, bodiesC[0], AcDb::kForWrite);
	if (es == Acad::eOk && pEntity3) {
		pEntity3->setColorIndex(3);
		pEntity3->close();
	}
	return;

	// 进行布尔运算，生成新的实体 
	acdbOpenObject(pSolid1, solidId1, AcDb::kForWrite);
	acdbOpenObject(pSolid2, solidId2, AcDb::kForWrite);
	pSolid1->setColorIndex(1);
	pSolid2->setColorIndex(2);


	AcDb::BoolOperType operation = AcDb::kBoolSubtract;
	es = pSolid1->booleanOper(operation, pSolid2);
	if (pSolid1->isNull()) {
		acutPrintf(L"pSolid1->isNull\n");
	}
	if (pSolid2->isNull()) {

	}	acutPrintf(L"pSolid2->isNull\n");

	//pSolid2->erase(); // 将其删除 
	pSolid2->close(); // 删除之后还是需要关闭该实体  	
	pSolid1->close(); 

	//----------------------------------------------------------------------
	AcDbEntity* pEntity1 = nullptr;
	es = acdbOpenAcDbEntity(pEntity1, solidId1, AcDb::kForWrite);
	if (es == Acad::eOk) {
		acutPrintf(L"pSolid1->isNull\n");
		pEntity1->close();
	}
	AcDbEntity* pEntity2 = nullptr;
	es = acdbOpenAcDbEntity(pEntity2, solidId2, AcDb::kForWrite);
	if (es == Acad::eOk) {
		acutPrintf(L"pSolid2->isNull\n");
		pEntity2->close();
	}
}

void CommandCreateRevolveBody()
{
	// 设置顶点的坐标 
	AcGePoint3d vertex[5];
	vertex[0] = AcGePoint3d(15, 0, 0);
	vertex[1] = AcGePoint3d(45, 0, 0);
	vertex[2] = AcGePoint3d(35, 9, 0);
	vertex[3] = AcGePoint3d(41, 18, 0);
	vertex[4] = AcGePoint3d(15, 18, 0);
	AcGePoint3dArray points;
	for (int i = 0; i <= 4; i++)
	{
		points.append(vertex[i]);
	}

	// 创建作为旋转截面的多段线 
	AcDb3dPolyline *p3dPoly = new AcDb3dPolyline(AcDb::k3dSimplePoly, points, true);
	AcDbObjectId polyId = AppendEntityToModelSpace(p3dPoly);

	// 将闭合的多段线转化成面域 
	AcDbObjectIdArray boundaryIds;
	boundaryIds.append(polyId);
	AcDbObjectIdArray regionIds;
	CreateRegion(boundaryIds, regionIds);

	// 进行旋转操作 
	AcDbRegion *pRegion;
	es = acdbOpenObject(pRegion, regionIds.at(0), AcDb::kForRead);
	AcDb3dSolid *pSolid = new AcDb3dSolid();
	es = pSolid->revolve(pRegion, AcGePoint3d::kOrigin, AcGeVector3d(0, 1, 0), 8 * atan(1));
	AppendEntityToModelSpace(pSolid);
	pRegion->close();
}

void CommandCreateBox() {
	AcDb3dSolid *pSolid = new AcDb3dSolid();
	es = pSolid->createBox(40, 50, 30);
	if (es != Acad::eOk)
	{
			acedAlert(L"create fail!");
			delete pSolid;
			return;
	}

	// 使用几何变换矩阵移动长方体
	AcGeVector3d vec(100, 100, 100);
	AcGeMatrix3d xform;
	xform.setToTranslation(vec);
	pSolid->transformBy(xform);

	// 将长方体添加到模型空间
	AppendEntityToModelSpace(pSolid);
}


