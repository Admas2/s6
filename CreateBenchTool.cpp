﻿#include "CreateBenchTool.h"
#include "CreateRegionTool.h"
#include "global.h"

CreateBenchTool::CreateBenchTool(const tagParam& param):
	param_(param)
{
	assert(param_.count <= 4);	// 目前最多只处理4级台阶
	assert(param_.count <= MAX_ELEMENT_NUM);
	assert(param_.count <= MAX_ELEMENT_NUM);
	assert(param.count > 0);
}

AcDbObjectId CreateBenchTool::CreateRegion() {
	if (!param_.stepIsLeft) {
		for (int index = 0; index < MAX_ELEMENT_NUM; index++) {
			param_.w[index] = - param_.w[index];
		}
	}

	AcGePoint2d pos[50];
	int posNum;
	if (param_.kind == bench_kind_vertial_through) {
		posNum = CreateVertialArray(pos);
	}
	else {
		posNum = CreateHorizontalArray(pos);
	}
	
	CreateRegionTool regionTool;
	regionTool.Init(param_.stakeNo, param_.componentName);
	for (int index = 0; index < posNum; index++) {
		regionTool.AppendKeyPoint(pos[index]);
	}
	AcDbObjectId id = regionTool.CreateRegion(param_.colorIndex);
	return id;
}

int CreateBenchTool::CreateHorizontalArray(AcGePoint2d pos[]) {
	//对于水平延伸台阶，居Y轴左侧，只能向左延伸，反之亦然
	assert(param_.isLeft == param_.extendInward);
	int posNum = 0;
  if (param_.count == 1) {
		pos[0].x = - param_.width;
		pos[0].y = param_.sy;

		pos[1].x = param_.sx;
		pos[1].y = pos[0].y;

		pos[2] = pos[1];
		pos[2].y -= param_.h[0];
		pos[3] = pos[2];
		pos[3].x -= param_.w[0];

		pos[4] = pos[3];
		pos[4].y -= param_.h[1];

		pos[5] = pos[4];
		pos[5].x = -param_.width;
		posNum = 6;
	}
	else if (param_.count == 2) {
		pos[0].x = -param_.width;
		pos[0].y = param_.sy;

		pos[1].x = param_.sx;
		pos[1].y = pos[0].y;

		pos[2] = pos[1];
		pos[2].y -= param_.h[0];
		
		pos[3] = pos[2];
		pos[3].x -= param_.w[0];

		pos[4] = pos[3];
		pos[4].y -= param_.h[1];

		pos[5] = pos[4];
		pos[5].x -= param_.w[1];

		pos[6] = pos[5];
		pos[6].y -= param_.h[2];

		pos[7] = pos[6];
		pos[7].x = -param_.width;
		posNum = 8;
	}
	else if (param_.count == 3) {
		pos[0].x = -param_.width;
		pos[0].y = param_.sy;

		pos[1].x = param_.sx;
		pos[1].y = pos[0].y;

		pos[2] = pos[1];
		pos[2].y -= param_.h[0];

		pos[3] = pos[2];
		pos[3].x -= param_.w[0];

		pos[4] = pos[3];
		pos[4].y -= param_.h[1];

		pos[5] = pos[4];
		pos[5].x -= param_.w[1];

		pos[6] = pos[5];
		pos[6].y -= param_.h[2];

		pos[7] = pos[6];
		pos[7].x -= param_.w[2];

		pos[8] = pos[7];
		pos[8].y -= param_.h[3];

		pos[9] = pos[8];
		pos[9].x = -param_.width;
		posNum = 10;
	}
	else {
		assert(false);
	}

	
	return posNum;
}

int CreateBenchTool::CreateVertialArray(AcGePoint2d pos[]) {
	int posNum = 0;
	if (param_.count == 1) {
		pos[0].x = param_.sx;
		pos[0].y = 0;
		pos[1] = pos[0];
		pos[1].y += param_.sy - param_.h[0];
		pos[1].y += CalcStartDeltahHeight(fabs(pos[1].x), param_.pw);

		pos[2] = pos[1];
		pos[2].x -= param_.w[0];
		pos[2].y += CalcStartDeltahHeight(fabs(pos[2].x), param_.pw) - CalcStartDeltahHeight(fabs(pos[1].x), param_.pw);

		pos[3] = pos[2];
		pos[3].y -= param_.h[1];

		pos[4] = pos[3];
		pos[4].x -= param_.w[1];
		pos[4].y += CalcStartDeltahHeight(fabs(pos[4].x), param_.pw) - CalcStartDeltahHeight(fabs(pos[3].x), param_.pw);

		pos[5] = pos[4];
		pos[5].y -= param_.height;
		posNum = 6;
	}
	else if (param_.count == 2) {
		pos[0].x = param_.sx;
		pos[0].y = 0;

		pos[1] = pos[0];
		pos[1].y = param_.sy - param_.h[0];
		pos[1].y += CalcStartDeltahHeight(fabs(pos[1].x), param_.pw);

		pos[2] = pos[1];
		pos[2].x -= param_.w[0];
		pos[2].y += CalcStartDeltahHeight(fabs(pos[2].x), param_.pw) - CalcStartDeltahHeight(fabs(pos[1].x), param_.pw);

		pos[3] = pos[2];
		pos[3].y -= param_.h[1];

		pos[4] = pos[3];
		pos[4].x -= param_.w[1];
		pos[4].y += CalcStartDeltahHeight(fabs(pos[4].x), param_.pw) - CalcStartDeltahHeight(fabs(pos[3].x), param_.pw);

		pos[5] = pos[4];
		pos[5].y -= param_.h[2];					  

		pos[6] = pos[5];
		pos[6].x -= param_.w[2];
		pos[6].y += CalcStartDeltahHeight(fabs(pos[6].x), param_.pw) - CalcStartDeltahHeight(fabs(pos[5].x), param_.pw);

		pos[7] = pos[6];
		pos[7].y -= param_.height;
		posNum = 8;
	}
	else if (param_.count == 3) {
		pos[0].x = param_.sx;
		pos[0].y = 0;

		pos[1] = pos[0];
		pos[1].y = param_.sy - param_.h[0];
		pos[1].y += CalcStartDeltahHeight(fabs(pos[1].x), param_.pw);

		pos[2] = pos[1];
		pos[2].x -= param_.w[0];
		pos[2].y += CalcStartDeltahHeight(fabs(pos[2].x), param_.pw) - CalcStartDeltahHeight(fabs(pos[1].x), param_.pw);

		pos[3] = pos[2];
		pos[3].y -= param_.h[1];

		pos[4] = pos[3];
		pos[4].x -= param_.w[1];
		pos[4].y += CalcStartDeltahHeight(fabs(pos[4].x), param_.pw) - CalcStartDeltahHeight(fabs(pos[3].x), param_.pw);

		pos[5] = pos[4];
		pos[5].y -= param_.h[2];

		pos[6] = pos[5];
		pos[6].x -= param_.w[2];
		pos[6].y += CalcStartDeltahHeight(fabs(pos[6].x), param_.pw) - CalcStartDeltahHeight(fabs(pos[5].x), param_.pw);

		pos[7] = pos[6];
		pos[7].y -= param_.h[3];
		
		pos[8] = pos[7];
		pos[8].x -= param_.w[2];
		pos[8].y += CalcStartDeltahHeight(fabs(pos[8].x), param_.pw) - CalcStartDeltahHeight(fabs(pos[7].x), param_.pw);

		pos[9] = pos[8];
		pos[9].y -= param_.height;
		posNum = 10;
	}
	else {
		assert(false);
	}

	//在Y轴正负两个方向上，扩展范围，以生成一个足够大的面域去截面 拼宽路面
	if (param_.isExpandY) {
		pos[0].y += param_.height;
		pos[posNum - 1].y -= param_.height;
	}

	posNum += 2;
	

	if (param_.extendInward) {
		pos[posNum - 2] = pos[posNum - 3];
		pos[posNum - 2].x = 0;
		pos[posNum - 1] = pos[0];
		pos[posNum - 1].x = 0;
	}
	else {
		
		if (param_.isLeft) {
			pos[posNum - 2] = pos[posNum - 3];
			pos[posNum - 2].x = -param_.width;
			pos[posNum - 1] = pos[0];
			pos[posNum - 1].x = -param_.width;
		}
		else {
			pos[posNum - 2] = pos[posNum - 3];
			pos[posNum - 2].x = +param_.width;
			pos[posNum - 1] = pos[0];
			pos[posNum - 1].x = +param_.width;
		}
	}

	return posNum;
}

