﻿#include "pch.h"
#include "SideSection.h"
#include "ComponentDivider.h"
#include "ComponentBench.h"
#include "ComponentMargin.h"
#include "CreateRegionTool.h"
#include "BooleanOperation.h"
#include "SideSectionAtom.h"
#include "global.h"
#include "EntityXData.h"
#include "LayerOperation.h"

CSideSection::CSideSection(int index, const tagSideSectionData&  ssd_) :
	ssd_(ssd_),
	index_(index)
{

}

CSideSection::~CSideSection() {
	RemoveEntities();
}

void CSideSection::RemoveEntities() {
	//logInfo.Format(L"删除截面实体,桩号:%.2f \n", ssd_.stakeNo); RDTRACE
	RemoveEntityFromModelSpace(acdbRegions_);
	acdbRegions_.removeAll();
}

AcArray<AcDbObjectId> CSideSection::GetRegions() const {
	return acdbRegions_;
}

//1: 双侧拼宽
void CSideSection::BuildDoubleWidening() {
	//扩建区域
	CSideSectionAtom  extensionSideSection(ssd_.stakeNo,ssd_.isLeft, ssd_.pavementWidth, ssd_.pavamentStruct, ssd_.componentParameters, section_side_kind_new_road_expand);
	extensionSideSection.Create(ssd_.dividerBenchParameters[0], true, new_road_type_direct_overlap);
	AcArray<AcDbObjectId> extensionRegions = extensionSideSection.GetRegions();

	//2.加铺区域				  
	CSideSectionAtom  overlapSideSection(ssd_.stakeNo, ssd_.isLeft, ssd_.pavementWidth, ssd_.overlapArea1.layers, section_side_kind_new_road_overlap_inner);
	overlapSideSection.SetGroundLine(ssd_.groundLine);
	overlapSideSection.Create(ssd_.dividerBenchParameters[0], false, ssd_.newRoadType1);
	AcArray<AcDbObjectId> overlapRegions = overlapSideSection.GetRegions();

	//3.合并
	SubtractRegions(overlapRegions, extensionRegions);
	acdbRegions_.append(extensionRegions);
	acdbRegions_.append(overlapRegions);

	//4.加调平层
	if (ssd_.newRoadType1 == new_road_type_add_leveling_layer) {
		bool r = AddLevelingLayer(acdbRegions_, extensionSideSection.OriginalWidth(), extensionSideSection.OriginalHeight(), section_side_kind_new_road_overlap_inner);
		if (!r) {
			logInfo.Format(L"生成调平层失败！检查地面线是与新路路面结构有交点! %.2f %s", ssd_.stakeNo, ssd_.isLeft ? L"左侧" : L"右侧");
		}
	}
}

//增加调平层
//输入参数：新路路面结构 rects，宽totalW,高totalH 
bool CSideSection::AddLevelingLayer(AcArray<AcDbObjectId>& rects, double totalW, double totalH, eSectionSideKind sideKind) {
	//1.创建两条老路地面线
	AcDbLine* pLine = new AcDbLine(ssd_.groundLine[0], ssd_.groundLine[1]);
	AcDbObjectId groudLineID1 = AppendEntityToModelSpace(pLine, layer_Sections);

	pLine = new AcDbLine(ssd_.groundLine[2], ssd_.groundLine[3]);
	AcDbObjectId groudLineID2 = AppendEntityToModelSpace(pLine, layer_Sections);

	//2.求老路地面线与与路面结构的交点
	AcGePoint3dArray& points = theApp.BorrowGlobalPt3dAry();
	LineInstersectRects(groudLineID1, rects, points);
	LineInstersectRects(groudLineID2, rects, points);
	if (points.length() < 2) {
		logInfo.Format(L"老路地面线 与 新路没有交点，没有生成调平层 桩号:%.2f\n", ssd_.stakeNo); RDTRACE
		#if true
			RemoveEntityFromModelSpace(groudLineID1);
			RemoveEntityFromModelSpace(groudLineID2);
		#else
			//将地面线移动other层以供调试
			MoveEntityToLayer(groudLineID1, gOptions.layers[layer_Other]);
			MoveEntityToLayer(groudLineID2, gOptions.layers[layer_Other]);
		#endif
		theApp.ReturnGlobalPt3dAry(&points);
		return false;
	}
	else {
		RemoveEntityFromModelSpace(groudLineID1);
		RemoveEntityFromModelSpace(groudLineID2);
	}
	
	//3.
	AcGePoint3d ptFarOfOrigin, ptNearOfOrigin;
	GetMaxDistancePt(points, ptFarOfOrigin, ptNearOfOrigin);
	//TagAPoint(ptFarOfOrigin,  2, 0.1);
	//TagAPoint(ptNearOfOrigin, 3, 0.1);

	//4.作辅助直线求一个交点
	AcGePoint3d start = ptFarOfOrigin;
	start.y += 0.001;
	AcGePoint3d end = start;
	end.y += 2 * totalH;
	pLine = new AcDbLine(start, end);
	AcDbObjectId testLineID = AppendEntityToModelSpace(pLine, layer_Sections);
	points.removeAll();
	LineInstersectRects(testLineID, rects, points);
	if (points.isEmpty()) {
		theApp.ReturnGlobalPt3dAry(&points);
		return false;
	}
	AcGePoint3d maxYPt = GetMaxYPoint(points);
	//TagAPoint(maxYPt, 4, 0.1);

	// 5.生成一个辅助截面
	CreateRegionTool tool;
	tool.AppendKeyPoint(ptFarOfOrigin);
	tool.AppendKeyPoint(maxYPt);
	tool.AppendKeyPoint(ptNearOfOrigin);
	AcDbObjectId triangleRegionID = tool.CreateRegion(4);
	if (triangleRegionID.isNull()) {
		theApp.ReturnGlobalPt3dAry(&points);
		return false;
	}

	//6.通过布尔运算 计算出"调平层"截面
	AcArray<AcDbObjectId> rects2 = CloneEntities(rects, layer_Sections);
	AcDbObjectId aPolygen = UniteRegions(rects2);
	AcDbObjectId aPolygen2 = CloneEntity(aPolygen, layer_Sections);
	UniteRegion(aPolygen, triangleRegionID);
	SubtractRegion(aPolygen, aPolygen2);
	if (aPolygen.isValid()) {
		const int levelingLayerColorIndex = 30;

		//设置附加信息
		SetRegionXData(aPolygen, tagRegionXData{ ssd_.overlapArea1.levelingLayerMaterialID,ssd_.isLeft,levelingLayerColorIndex,sideKind,L"调平层",ssd_.stakeNo,-1 });
										  
		//7.加入调平层
		rects.append(aPolygen);
	}

	//8.
	RemoveEntityFromModelSpace(aPolygen2);
	RemoveEntityFromModelSpace(testLineID);
	theApp.ReturnGlobalPt3dAry(&points);
	return true;
}

//单侧拼宽
void CSideSection::BuildSingleWidening() {
	if (ssd_.overlapSame) {
		//加铺同侧: 本侧只有两个加铺区域,没有扩建区域
		const tagComponentParameter& dividerBenchInward = ssd_.dividerBenchParameters[0];

		//1.加铺区域(内侧)			  
		CSideSectionAtom  overlaAreaInward(ssd_.stakeNo, ssd_.isLeft, ssd_.pavementWidth, ssd_.overlapArea1.layers, section_side_kind_new_road_overlap_inner);
		overlaAreaInward.SetGroundLine(ssd_.groundLine);
		overlaAreaInward.Create(dividerBenchInward, false, ssd_.newRoadType1);
		AcArray<AcDbObjectId> overlapRegionsInward = overlaAreaInward.GetRegions();

		//2.加铺区域(外侧)
		CSideSectionAtom  overlaAreaOutward(ssd_.stakeNo, ssd_.isLeft, ssd_.pavementWidth, ssd_.overlapArea2.layers, section_side_kind_new_road_overlap_outter);
		overlaAreaOutward.SetGroundLine(ssd_.groundLine);
		overlaAreaOutward.Create(dividerBenchInward, true, ssd_.newRoadType2);
		AcArray<AcDbObjectId> overlapRegionsOutward = overlaAreaOutward.GetRegions();

		//3.合并
		acdbRegions_.append(overlapRegionsInward);
		acdbRegions_.append(overlapRegionsOutward);

		//4.加调平层
		if (ssd_.newRoadType1 == new_road_type_add_leveling_layer || ssd_.newRoadType2 == new_road_type_add_leveling_layer) {
			CSideSectionAtom  extensionArea(ssd_.stakeNo, ssd_.isLeft, ssd_.pavementWidth, ssd_.pavamentStruct, ssd_.componentParameters, section_side_kind_new_road_expand);
			AddLevelingLayer(acdbRegions_, extensionArea.OriginalWidth(), extensionArea.OriginalHeight(), section_side_kind_new_road_overlap_inner_and_outter);
		}
	}
	else {
		//加铺异侧
		BuildNew();
	}
}

//过渡拼宽
void CSideSection::BuildTransitionalWidening() {
	if (ssd_.overlapSame) {
		//加铺同侧
		const tagComponentParameter& dividerBenchInward = ssd_.dividerBenchParameters[0];
		const tagComponentParameter& dividerBenchOutward = ssd_.dividerBenchParameters[1];

		//1.扩建区域
		CSideSectionAtom  extensionArea(ssd_.stakeNo, ssd_.isLeft, ssd_.pavementWidth, ssd_.pavamentStruct, ssd_.componentParameters, section_side_kind_new_road_expand);
		extensionArea.Create(dividerBenchOutward, true, new_road_type_direct_overlap);
		AcArray<AcDbObjectId> extensionRegions = extensionArea.GetRegions();

		//2.加铺区域(内侧）				  
		CSideSectionAtom  overlapAreaInward(ssd_.stakeNo, ssd_.isLeft, ssd_.pavementWidth, ssd_.overlapArea1.layers, section_side_kind_new_road_overlap_inner);
		overlapAreaInward.SetGroundLine(ssd_.groundLine);
		overlapAreaInward.Create(dividerBenchInward, false, ssd_.newRoadType1);
		AcArray<AcDbObjectId> overlapRegionsInward = overlapAreaInward.GetRegions();

		//3.加铺区域（外侧）
		CSideSectionAtom  overlapAreaOutward(ssd_.stakeNo, ssd_.isLeft, ssd_.pavementWidth, ssd_.overlapArea2.layers, section_side_kind_new_road_overlap_outter);
		overlapAreaOutward.SetGroundLine(ssd_.groundLine);
		assert(ssd_.newRoadType2 != new_road_type_null);
		overlapAreaOutward.Create(dividerBenchInward, dividerBenchOutward, ssd_.newRoadType2);
		AcArray<AcDbObjectId> overlapRegionsOutward = overlapAreaOutward.GetRegions();

		////4.合并
		SubtractRegions(overlapRegionsInward, extensionRegions);
		SubtractRegions(overlapRegionsOutward, extensionRegions);
		acdbRegions_.append(extensionRegions);
		acdbRegions_.append(overlapRegionsInward);
		acdbRegions_.append(overlapRegionsOutward);

		//5.加调平层
		if (ssd_.newRoadType1 == new_road_type_add_leveling_layer || ssd_.newRoadType2 == new_road_type_add_leveling_layer) {
			AddLevelingLayer(acdbRegions_, extensionArea.OriginalWidth(), extensionArea.OriginalHeight(), section_side_kind_new_road_overlap_inner_and_outter);
		}
	}
	else {
		//加铺异侧
		//1.扩建侧
		CSideSectionAtom  extensionArea(ssd_.stakeNo, ssd_.isLeft, ssd_.pavementWidth, ssd_.pavamentStruct, ssd_.componentParameters, section_side_kind_new_road_expand);
		extensionArea.Create(ssd_.dividerBenchParameters[0], true, new_road_type_direct_overlap);
		AcArray<AcDbObjectId> extensionRegions = extensionArea.GetRegions();

		//2.加铺侧				  
		CSideSectionAtom  overlapArea(ssd_.stakeNo, ssd_.isLeft, ssd_.pavementWidth, ssd_.overlapArea1.layers, section_side_kind_new_road_overlap_inner);
		overlapArea.SetGroundLine(ssd_.groundLine);
		overlapArea.Create(ssd_.dividerBenchParameters[0],false,ssd_.newRoadType1);
		AcArray<AcDbObjectId> overlapRegions = overlapArea.GetRegions();

		//3.合并
		SubtractRegions(overlapRegions, extensionRegions);
		acdbRegions_.append(extensionRegions);
		acdbRegions_.append(overlapRegions);

		//4.加调平层
		if (ssd_.newRoadType1 == new_road_type_add_leveling_layer || ssd_.newRoadType2 == new_road_type_add_leveling_layer) {
			AddLevelingLayer(acdbRegions_, extensionArea.OriginalWidth(), extensionArea.OriginalHeight(), section_side_kind_new_road_overlap_inner);
		}
	}

}

//创建新路
void CSideSection::BuildNew() {
	CSideSectionAtom  section(ssd_.stakeNo, ssd_.isLeft, ssd_.pavementWidth, ssd_.pavamentStruct, ssd_.componentParameters, 
		ssd_.isNewRoad ? section_side_kind_new_road_expand : section_side_kind_old_road);
	section.Create(new_road_type_direct_overlap);
	AcArray<AcDbObjectId> r = section.GetRegions();
	acdbRegions_.append(r);
}

void CSideSection::Create() {
	assert(acdbRegions_.length() == 0);
	if (ssd_.isNewRoad) {
		switch (ssd_.wideningType) {
			case widening_type_double_widening:					//双侧拼宽
				BuildDoubleWidening();				
				break;	
			case widening_type_transitional_widening:		//过渡拼宽
				BuildTransitionalWidening();	
				break;	
			case widening_type_single_widening:					//单侧拼宽
				BuildSingleWidening();				
				break;	
			case widening_type_build_new:								//建新路
				BuildNew();										
				break;	
			default:	assert(false);
		}
	}
	else {
		BuildNew();
	}

	//过滤掉为空的面域
	AcArray<AcDbObjectId>  acdbRegions(acdbRegions_);
	acdbRegions_.removeAll();
	for (AcDbObjectId id : acdbRegions) {
		if (id.isValid()) {
			AcDbEntity* pEntity = nullptr;
			es = acdbOpenAcDbEntity(pEntity, id, AcDb::kForRead);
			if (es == Acad::eOk) {
				AcDbRegion* pReg = AcDbRegion::cast(pEntity);
				double area = 0.0;
				pReg->getArea(area);
				if (!pReg->isNull() && area > 0.0) {
					acdbRegions_.append(id);
				}
				pEntity->close();
			}
		}
	}
}

AcDbObjectId CSideSection::GetRegionByIndex(int index) const {
	if (0 <= index && index < acdbRegions_.length()) {
		return acdbRegions_[index];
	}
	return AcDbObjectId();
}

AcDbObjectId CSideSection::GetRegionByXData(const tagRegionXData& xData) const {
	for (AcDbObjectId id : acdbRegions_) {
		tagRegionXData xData2 = GetSectionXData(id);
		if (xData2.stakeNo > -1.0 && 
				xData2.name == xData.name && 
			  xData2.sideKind == xData.sideKind &&
			  xData2.isLeft		== xData.isLeft) {
			return id;
		}
	}
	return AcDbObjectId();
}