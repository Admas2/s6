﻿#include "pch.h"

//面域相关布尔运算
void MirrorRegions(AcArray<AcDbObjectId>&  acdbRegions);
AcDbObjectId CloneEntity(AcDbObjectId srcID, eLayerID layerIndex);
AcArray<AcDbObjectId> CloneEntities(const AcArray<AcDbObjectId>& srcIDs, eLayerID layerIndex);
void UniteRegion(AcDbObjectId& destID, AcDbObjectId srcID);
AcDbObjectId UniteRegions(const AcArray<AcDbObjectId>& srcIDs);

void SubtractRegions(const AcArray<AcDbObjectId>& destIDs, const AcArray<AcDbObjectId>& sliceIDS);
void SubtractRegion(const AcDbObjectId& destID, const AcDbObjectId& sliceID);
void IntersectRegions(AcDbObjectId& destID, const AcArray<AcDbObjectId>& srcIDs);
void IntersectRegions(AcArray<AcDbObjectId>& destIDs, const AcArray<AcDbObjectId>& srcIDs);
void SubtractSolid(const AcDbObjectId& destID, const AcDbObjectId& sliceID);
void SubtractSolids(const AcArray<AcDbObjectId>& destIDs, const AcArray<AcDbObjectId>& sliceIDs);
void SubtractSolids(AcDbObjectId destID, const AcArray<AcDbObjectId>& sliceIDs);
bool DetermineNonIntersect(AcDbExtents extents1, AcDbExtents extents2);

//三维实体相关布尔运算
AcDbObjectId IntersectSolid(AcDbObjectId id1, AcDbObjectId id2,eLayerID layerName);
AcArray<AcDbObjectId> IntersectSolids(const AcArray<AcDbObjectId>& opr1, const AcArray<AcDbObjectId>& opr2,eLayerID layerName);
AcArray<AcDbObjectId> IntersectSolids(AcDbObjectId id1, const AcArray<AcDbObjectId>& opr2, eLayerID layerName);