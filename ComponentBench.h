#pragma once
#include "pch.h"
#include "SectionComponent.h"
#include "SideSectionAtom.h"
class ComponentBench : public CSectionComponent
{
public:
	ComponentBench(const tagComponentParameter& cp, CSideSectionAtom* owner_section, bool isLeft);
	~ComponentBench();
	AcDbObjectId CreatePositiveComponet() override;
};

