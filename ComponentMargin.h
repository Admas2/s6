#pragma once
#include "pch.h"
#include "SectionComponent.h"
#include "SideSectionAtom.h"
class ComponentMargin :public CSectionComponent
{
public:
	ComponentMargin(const tagComponentParameter& cp, CSideSectionAtom* owner_section, bool isLeft);
	~ComponentMargin();
	AcDbObjectId CreatePositiveComponet() override;
	AcDbObjectId CreateNegaiveComponet() override;
};

