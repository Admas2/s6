//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Arx-CPP-plug-in.rc
//
#define IDD_TEST_PLATFORM_DLG           102
#define IDD_BENCH_DLG                   130
#define IDB_BITMAP1                     135
#define IDB_BITMAP2                     136
#define IDB_BITMAP3                     137
#define IDD_DIVIDER_DLG                 138
#define IDD_MARGIN_DLG                  140
#define IDC_COMPONET_DIVIER             1000
#define IDC_BUTTON1                     1000
#define IDC_PICK_EDGE                   1000
#define IDC_BUTTON_COMPONET_BENCH       1001
#define IDC_BUTTON_COMPONET_MARGIN      1002
#define IDD_PICK_ROAD_EDGE_DLG          1002
#define IDC_CHECK_MARGIN                1003
#define IDC_BUTTON_REBUILD_SECTION      1004
#define IDC_BUTTON_REBUILD_3D_SOLID     1006
#define IDC_BUTTON_CENTER_LINE          1007
#define IDC_BUTTON_REBUILD_CENTER_LINE  1007
#define IDC_LIST_ROAD_FLOOR_DETAIL      1008
#define IDC_CHECK_BENCH                 1009
#define IDC_EDIT_H1                     1009
#define IDC_CHECK_DIVIDER               1010
#define IDC_EDIT_H2                     1010
#define IDC_COMBO_ROAD_FLOOR            1011
#define IDC_EDIT_H3                     1011
#define IDC_COMBO_ROAD_WIDTH            1012
#define IDC_EDIT_B1                     1012
#define IDC_LIST_ROAD_WIDTH_DETAIL      1013
#define IDC_EDIT_B2                     1013
#define IDC_CHECK_COMPONET_MIRROR       1014
#define IDC_EDIT_B3                     1014
#define IDC_EDIT_N                      1015
#define IDC_EDIT_PAVEMNT_SLOP           1017

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        1004
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           1000
#endif
#endif
