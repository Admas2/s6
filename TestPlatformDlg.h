﻿#pragma once
// CTestPlatformDlg dialog
#include "Roadway.h"
#include "RoadSection.h"
#include "RoadCenterLine.h"

class CTestPlatformDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CTestPlatformDlg)

public:
	CTestPlatformDlg(CWnd* pParent = nullptr);   // standard constructor
	virtual ~CTestPlatformDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_TEST_PLATFORM_DLG };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
private:
	afx_msg void OnBnClickedComponetDivier();
	afx_msg void OnBnClickedButtonComponetBench();
	afx_msg void OnBnClickedButtonComponetMargin();
	afx_msg void OnBnClickedButtonRebuild3dSolid();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCheckDivider();
	afx_msg void OnBnClickedCheckBench();
	afx_msg void OnBnClickedCheckMargin();
	afx_msg void OnSelchangeComboRoadWidth();
	afx_msg void OnSelchangeComboRoadFloor();
	afx_msg void OnBnClickedButtonRebuildSection();
	
	virtual BOOL OnInitDialog();

private:
	CComboBox m_ctrlRoadWidth;	//路幅
	CComboBox m_ctrlRoadFloor;	//路层	
	CListCtrl m_ctrlWidthDetail;
	CListCtrl m_ctrlRoadFloorDetail;

	BOOL m_bSectionMirror;
	BOOL m_bHasDivider;
	BOOL m_bHasBench;
	BOOL m_bHasMargin;
	double m_dbPavementSlop = 0.0;
	double m_ASideWdth =0.0;

	tagComponentParameter m_floorCP;
	tagComponentParameter m_dividerCP;
	tagComponentParameter m_benchCP;
	tagComponentParameter m_marginCP;
	tagSectionData			m_sectionData;
	tagRoadwayData		m_ia;

	CRoadCenterLine			m_centerLine;
	CRoadSection				m_section;
	CRoadway						m_roadWay;
public:
	afx_msg void OnBnClickedButtonRebuildCenterLine();
	afx_msg void OnDestroy();
	afx_msg void OnClose();
};
