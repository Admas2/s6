﻿#include "CreateRegionTool.h"
#include "global.h"
void CreateRegionTool::Init(double stakeNo, const std::wstring& componentName) {
	stakeNo_ = stakeNo;
	componentName_ = componentName;
	keyPoints_.removeAll();
}
void CreateRegionTool::AppendKeyPoint(double x, double y) {
	AppendKeyPoint(AcGePoint2d(x, y));
}
void CreateRegionTool::AppendKeyPoint(AcGePoint2d pos) {
	keyPoints_.append(pos);
}

void CreateRegionTool::AppendKeyPoint(AcGePoint3d pos) {
	AppendKeyPoint(pos.x, pos.y);
}

AcDbObjectId CreateRegionTool::CreateRegion(int colorIndex) {
	AcDbObjectId entId;
	AcDbPolyline *pPolyline = new AcDbPolyline;
	for (int index = 0; index < keyPoints_.length(); index++) {
		pPolyline->addVertexAt(index, keyPoints_[index]);
	}
	if (lookCurve_) {
		AppendEntityToModelSpace(pPolyline, layer_Other);
		return AcDbObjectId();
	}
	pPolyline->setClosed(Adesk::kTrue);
	assert(pPolyline->isKindOf(AcDbCurve::desc()));
	AcDbVoidPtrArray curves;
	curves.append(static_cast<void*>(pPolyline));
	//AcDbVoidPtrArray& regions = *(new AcDbVoidPtrArray); // 这里有不明原因的释放崩溃，只能使用不放释放方式
	AcDbVoidPtrArray regions(10, 10);  //要指定一个初始空间范围，避免释放崩溃，具体原因不明
	es = AcDbRegion::createFromCurves(curves, regions);
	if (es == Acad::eOk) {
		assert(regions.length() == 1);
		AcDbRegion *pRegion = static_cast<AcDbRegion*>(regions[0]);
		pRegion->setColorIndex(colorIndex);
		entId = AppendEntityToModelSpace(pRegion, layer_Sections);
	}
	else {
		logInfo.Format(L"创建面域失败！ [%s] \n", componentName_.c_str());
		if (stakeNo_ != -1) {
			logInfo.Format(L"创建面域失败！[%s] 桩号:%.2f\n",componentName_.c_str(), stakeNo_);
		}
		RDTRACE
		//如果创建面域失败就将包围多义线加入图纸，以供调试察错
		AppendEntityToModelSpace(pPolyline, layer_Other);
		for (int index = 0; index < keyPoints_.length(); index++) {
			TRACE(L" %.2f,  %.2f\n", keyPoints_[index].x, keyPoints_[index].y);
		}
	}
	return entId;
}
