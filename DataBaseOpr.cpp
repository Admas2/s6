﻿#include "pch.h"
#include "DataBaseOpr.h"
#include <sqlite3.h>
#include <vector>
#include "global.h"
#include "../jsoncpp/json.h"
#include "StringTransfer.h"

CDataBaseOpr::CDataBaseOpr() {

}

CDataBaseOpr::~CDataBaseOpr() {
	Close();
}

bool CDataBaseOpr::Open(std::string dbName) {
	int r = sqlite3_open(dbName.c_str(), &pdb_);
	if (r != SQLITE_OK) {
		logInfo.Format(L"%s  库文件不存在",Utf82Unicode(dbName).c_str()); MESSAGE_BOX
		pdb_ = nullptr;
	}
	return (r == SQLITE_OK);
}

bool CDataBaseOpr::Open() {
	#ifdef S5_ARX2020_ARX
	int r = sqlite3_open(theApp.GetDatabasePath().c_str(), &pdb_);
	if (r != SQLITE_OK) {
		logInfo.Format(L"%s  库文件不存在", Utf82Unicode(theApp.GetDatabasePath()).c_str()); MESSAGE_BOX
		pdb_ = nullptr;
	}
	return (r == SQLITE_OK);
	#else
	return false;
	#endif
}

void CDataBaseOpr::Close() {
	if (pdb_) {
		sqlite3_close(pdb_);
		pdb_ = nullptr;
	}
	
}

int CDataBaseOpr::MaterialTableCallBack(void *data, int cNum, char *column_values[], char *column_names[]) //回调函数,处理sql语句执行结果
{
	static int colorIndex = 0;
	AcArray<tagMaterialKind>& result = * reinterpret_cast<AcArray<tagMaterialKind>*>(data);
	tagMaterialKind item;
	item.name = Utf82Unicode(std::string(column_values[0]));
	result.append(item);
	return 0;
}

AcArray<tagMaterialKind> CDataBaseOpr::QueryMaterialList() {
	assert(pdb_ != nullptr);
	AcArray<tagMaterialKind> result;
	const char *sql = "select name,color,materialQuality from Material;";
	int r = sqlite3_exec(pdb_, sql, MaterialTableCallBack, (void*)&result, NULL);
	return result;
}

int CDataBaseOpr::RoadLineTableCallBack(void *data, int cNum, char *column_values[], char *column_names[]) {
	std::string& result = *reinterpret_cast<std::string*>(data);
	result = column_values[0];
	return 0;
}

std::string  CDataBaseOpr::QueryRoadLineID(const char* RoadLine_Name) {
	assert(pdb_ != nullptr);
	std::string result;
	char sql[128] = { 0 };  
	sprintf_s(sql, "select RoadLine_id from RoadLine where RoadLine_Name == '%s';",RoadLine_Name );
	int r = sqlite3_exec(pdb_, sql, RoadLineTableCallBack, (void*)&result, NULL);
	return result;
}

int CDataBaseOpr::Model_ContStakeXYZTableCallBack(void *data, int cNum, char *column_values[], char *column_names[]) {
	AcArray<tagFitPoint>& result = *reinterpret_cast<AcArray<tagFitPoint>*>(data);
	tagFitPoint item;
	item.stakeNo = ::atof(column_values[0]);
	item.x = ::atof(column_values[1]);
	item.y = ::atof(column_values[2]);
	item.z = ::atof(column_values[3]);
	item.A = ::atof(column_values[4]);
	result.append(item);
	return 0;
}

AcArray<tagFitPoint>   CDataBaseOpr::QueryRoadLineCoordinate(std::string roadLineID, double startStake, double endStake) {
	assert(pdb_ != nullptr);
	AcArray<tagFitPoint> result;
	char sql[500] = { 0 };   //sql语句
	//sample: select ContStake, X, Y, Z, A from Model_ContStakeXYZ where DesignLineGUID == 'b14d27b7-7212-4e44-9b76-30095293feb2' and 74205 <= ContStake and ContStake <= 74240 order by ContStake asc
	sprintf_s(sql, "select DISTINCT ContStake,X,Y,Z,A from Model_ContStakeXYZ where DesignLineGUID == '%s' and %f <= ContStake and  ContStake <= %f order by ContStake asc;", roadLineID.c_str(),startStake,endStake);
	int r = sqlite3_exec(pdb_, sql, Model_ContStakeXYZTableCallBack, (void*)&result, NULL);

	//计算拟合点的切线方向
	calcRoadLineVecTagent(result);


	return result;
}

AcArray<tagFitPoint>   CDataBaseOpr::QueryRoadLineCoordinate(std::string roadLineID) {
	assert(pdb_ != nullptr);
	AcArray<tagFitPoint> result;
	char sql[500] = { 0 };   //sql语句
	sprintf_s(sql, "select DISTINCT ContStake,X,Y,Z,A from Model_ContStakeXYZ where DesignLineGUID =='%s';", roadLineID.c_str());
	int r = sqlite3_exec(pdb_, sql, Model_ContStakeXYZTableCallBack, (void*)&result, NULL);
	
	//计算拟合点的切线方向
	calcRoadLineVecTagent(result);


	return result;
}

int CDataBaseOpr::sectionInfoTableCallBack(void *data, int cNum, char *column_values[], char *column_names[]) 
{
	AcArray<tagLineInterval>& result = *reinterpret_cast<AcArray<tagLineInterval>*>(data);
	result.append(tagLineInterval{::atol(column_values[0]),::atol(column_values[1])});
	return 0;
}

AcArray<tagLineInterval> CDataBaseOpr::QueryRoadNoModelInervals(std::string roadLineID) {
	assert(pdb_ != nullptr);
	AcArray<tagLineInterval> result;
	char sql[500] = { 0 };   //sql语句
	//样例参考： select startStake,endStake,RoadLine_id from sectionInfo where  RoadLine_id=='b14d27b7-7212-4e44-9b76-30095293feb2' order by startStake
	sprintf_s(sql, "select startStake,endStake,RoadLine_id from sectionInfo where  RoadLine_id=='%s' order by startStake;", roadLineID.c_str());

	int r = sqlite3_exec(pdb_, sql, sectionInfoTableCallBack, (void*)&result, NULL);
	return result;
}

AcArray<tagLineInterval> CDataBaseOpr::QueryRoadNoModelInervals(std::string roadLineID, double startStake, double endStake) {
	assert(pdb_ != nullptr);
	AcArray<tagLineInterval> result;
	char sql[500] = { 0 };   //sql语句
	//样例参考： select startStake,endStake,RoadLine_id from sectionInfo where  RoadLine_id=='b14d27b7-7212-4e44-9b76-30095293feb2' and not( endStake <= 74200 or 74210 <= startStake) order by startStake
	sprintf_s(sql, "select startStake,endStake,RoadLine_id from sectionInfo where  RoadLine_id=='%s' and not( endStake <= %f or %f <= startStake) order by startStake;", roadLineID.c_str(),startStake,endStake);

	int r = sqlite3_exec(pdb_, sql, sectionInfoTableCallBack, (void*)&result, NULL);
	return result;
}

static int ModelSegmentsTableCallBack(void *data, int cNum, char *column_values[], char *column_names[])
{
	AcArray<tagLineInterval>& result = *reinterpret_cast<AcArray<tagLineInterval>*>(data);
	//result.append(tagLineInterval{ ::atol(column_values[0]),::atol(column_values[1]) });
	return 0;
}

AcArray<tagLineInterval> CDataBaseOpr::QueryRoadModelSegments(std::string roadLineID) {
	assert(pdb_ != nullptr);
	AcArray<tagLineInterval> result;
	char sql[500] = { 0 };   //sql语句

	//样例参考： select startStake,endStake,RoadLine_id from sectionInfo where  RoadLine_id=='b14d27b7-7212-4e44-9b76-30095293feb2' and not( endStake <= 74200 or 74210 <= startStake) order by startStake
	//sprintf_s(sql, "select startStake,endStake,RoadLine_id from sectionInfo where  RoadLine_id=='%s' and not( endStake <= %d or %d <= startStake) order by startStake;", roadLineID.c_str(), startStake, endStake);
	//int r = sqlite3_exec(pdb_, sql, ModelSegmentsTableCallBack, (void*)&result, NULL);

	return result;
}

AcArray<tagLineInterval> CDataBaseOpr::QueryRoadModelSegments(std::string roadLineID, double startStake, double endStake){
	assert(pdb_ != nullptr);
	AcArray<tagLineInterval> result;
	char sql[500] = { 0 };   //sql语句

	//样例参考： select startStake,endStake,RoadLine_id from sectionInfo where  RoadLine_id=='b14d27b7-7212-4e44-9b76-30095293feb2' and not( endStake <= 74200 or 74210 <= startStake) order by startStake
	//sprintf_s(sql, "select startStake,endStake,RoadLine_id from sectionInfo where  RoadLine_id=='%s' and not( endStake <= %d or %d <= startStake) order by startStake;", roadLineID.c_str(), startStake, endStake);
	//int r = sqlite3_exec(pdb_, sql, ModelSegmentsTableCallBack, (void*)&result, NULL);

	return result;
}

int CrossInfoTableCallBack(void *data, int cNum, char *column_values[], char *column_names[]) {
	AcArray<tagSideSectionData>& result = *reinterpret_cast<AcArray<tagSideSectionData>*>(data);
	tagSideSectionData ssd;
	ssd.stakeNo = ::atof(column_values[0]);
	//ssd.LeftScheme  = column_values[1]; //LeftScheme
	result.append(ssd);
	return 0;
}

int Temp_CrossMngTableCallBack(void *data, int cNum, char *column_values[], char *column_names[]) {
	std::vector<std::string>& r2 = *reinterpret_cast<std::vector<std::string>*>(data);
	r2.push_back(column_values[0]);
	r2.push_back(column_values[1]);
	return 0;
}

int Temp_Cross_PavementStructureModelTableCallBack(void *data, int cNum, char *column_values[], char *column_names[]) {
	assert(false);
	//AcArray<tagPavementLayer>& pavamentStruct = *reinterpret_cast<AcArray<tagPavementLayer>*>(data);
	//tagPavementLayer item;
	//item.LayerID		= ::atoi(column_values[0]);
	//item.name				= p;
	//free(p);
	//item.Material = p;
	//free(p);
	//item.thickness		= ::atof(column_values[3]) * 0.01; //原来厚度是cm 现在改为m
	//pavamentStruct.append(item);
	return 0;
}

int Temp_Cross_StandardRoadwayModelMngTableCallBack(void *data, int cNum, char *column_values[], char *column_names[]) {
	std::vector<std::string>& r2 = *reinterpret_cast<std::vector<std::string>*>(data);
	r2.push_back(column_values[0]);
	r2.push_back(column_values[1]);
	return 0;
}

int Temp_Cross_StandardRoadwayModelTableCallBack(void *data, int cNum, char *column_values[], char *column_names[]) {
	AcArray<tagPavementSegment>& pw = *reinterpret_cast<AcArray<tagPavementSegment>*>(data);
	tagPavementSegment item;
	item.CrossSlop = ::atof(column_values[0]);
	item.SortID = ::atoi(column_values[1]);
	//item.name = column_values[2];
	//item.type = column_values[3];
	item.width = ::atof(column_values[4]);
	pw.append(item);
	return 0;
}

int Temp_CrossTableCallBack(void *data, int cNum, char *column_values[], char *column_names[]) {
	AcArray<tagComponentParameter>&  componentParameters = *reinterpret_cast<AcArray<tagComponentParameter>*>(data);
	tagComponentParameter pavamentStruct;
	pavamentStruct.component_id = column_values[0];
	componentParameters.append(pavamentStruct);
	return 0;
}

int Temp_Cross_componentMngTableCallBack(void *data, int cNum, char *column_values[], char *column_names[]) {
	tagComponentParameter& pavamentStruct = *reinterpret_cast<tagComponentParameter*>(data);
	int type = ::atoi(column_values[0]);
  if(type == 0){  // 0.中央分隔带
		pavamentStruct.componentKind = component_kind_divider;
	}else if (type == 1) {	 //	1.台阶
		pavamentStruct.componentKind = component_kind_bench;
	}
	else if (type == 2) {		 //2.路面边部构造
		pavamentStruct.componentKind = component_kind_margin;
	}
	return 0;
}

int Temp_Cross_componentTableCallBack(void *data, int cNum, char *column_values[], char *column_names[]) {
	tagComponentParameter& pavamentStruct = *reinterpret_cast<tagComponentParameter*>(data);
	double value = ::atof(column_values[1]) * 0.01; // 原来是cm,这里换成m单位
	if (std::string("h_0") == column_values[0]) {
		pavamentStruct.h1 = value;
	}else if (std::string("h_1") == column_values[0]) {
		pavamentStruct.h2 = value;
	}else if (std::string("h_2") == column_values[0]) {
		pavamentStruct.h3 = value;
	}
	else if(std::string("h_3") == column_values[0]) {
		pavamentStruct.h4 = value;
	}
	else if (std::string("w1") == column_values[0]) {
		pavamentStruct.w1 = value;
	}
	else if(std::string("w2") == column_values[0]) {
		pavamentStruct.w2 = value;
	}else if (std::string("w3") == column_values[0]) {
		pavamentStruct.w3 = value;
	}else if (std::string("w4") == column_values[0]) {
		pavamentStruct.w4 = value;

	}
	else if (std::string("n2") == column_values[0]) {
		pavamentStruct.n2 = value;
	}
	return 0;
}

int Temp_Cross_StandardRoadwayModelTableCallBack2(void *data, int cNum, char *column_values[], char *column_names[]) {
	//通过要Temp_Cross_StandardRoadwayModel中字段LeftOrRight的内容的个数，来判断是否为双幅路
	eRoadRangeKind& rangeKind = *reinterpret_cast<eRoadRangeKind*>(data);
	int v = ::atoi(column_values[0]);
	if ( v == 1) {
		rangeKind = rand_kind_single;
	}
	else if (v == 2)
	{
		rangeKind = rand_kind_double;
	}
	return 0;
}

void CDataBaseOpr::QuerySectionsScheme(tagSideSectionData& ssd) {
	//char sql[500] = { 0 };   //sql语句
	//std::vector<std::string> buffer;
	//const char* schemeID = ssd.LeftScheme.c_str();
	//sprintf_s(sql, "select roadStuct,StandRoadway from Temp_CrossMng where Temp_crossMng_id = '%s'", schemeID);
	//int r = sqlite3_exec(pdb_, sql, Temp_CrossMngTableCallBack, (void*)&buffer, NULL);
	//const std::string roadStuct = buffer[0];
	//const std::string StandRoadway = buffer[1];

	//void* param =  (void*)&ssd.pavamentStruct;
	//sprintf_s(sql, "select LayerID,LayerName,Material,Ply from Temp_Cross_PavementStructureModel where PGUID == '%s';", roadStuct.c_str());
	//r = sqlite3_exec(pdb_, sql, Temp_Cross_PavementStructureModelTableCallBack, param, NULL);

	//buffer.clear();
	//sprintf_s(sql, "select Name,SumWidth from Temp_Cross_StandardRoadwayModelMng where GUID == '%s'", StandRoadway.c_str());
	//r = sqlite3_exec(pdb_, sql, Temp_Cross_StandardRoadwayModelMngTableCallBack, (void*)&buffer, NULL);
	//const std::string Name = buffer[0];
	//const double SumWidth = ::atof(buffer[1].c_str());

	//sprintf_s(sql, "select SortCrossSlop,SortID,SortName,SortType,SortWidth from Temp_Cross_StandardRoadwayModel where LeftOrRight=='Left' and PGUID == '%s'", StandRoadway.c_str());
	//r = sqlite3_exec(pdb_, sql, Temp_Cross_StandardRoadwayModelTableCallBack, (void*)&ssd.pavementWidth, NULL);

	////3.查询此截面的上的所有组件
	////3.1查询截面组件ID号
	//AcArray<tagComponentParameter>& arrayCP = ssd.componentParameters;
	//sprintf_s(sql, "select component_id from Temp_Cross where Temp_crossMng_id = '%s'", schemeID);
	//r = sqlite3_exec(pdb_, sql, Temp_CrossTableCallBack, (void*)&arrayCP, NULL);
	//for (int j = 0; j < arrayCP.length(); j++) {
	//	tagComponentParameter& cp = arrayCP[j];
	//	//3.2 查询截面组件类型 :componentKind
	//	sprintf_s(sql, "select Type from Temp_Cross_componentMng where Guid == '%s'", cp.component_id.c_str());
	//	r = sqlite3_exec(pdb_, sql, Temp_Cross_componentMngTableCallBack, (void*)&cp, NULL);
	//	if (cp.componentKind == component_kind_null) continue;
	//	
	//	//3.3 查询截面组件参数值
	//	sprintf_s(sql, "select parameterName,parameterValue from Temp_Cross_component where PGuid == '%s'", cp.component_id.c_str());
	//	r = sqlite3_exec(pdb_, sql, Temp_Cross_componentTableCallBack, (void*)&cp, NULL);
	//}
}

int NoUseCallBack(void *data, int cNum, char *column_values[], char *column_names[]) {
	return 0;
}

std::string GenerateGUID() {
	char buffer[200];
	sprintf_s(buffer, 200, "%x%x", rand() * 100000, rand() * 100000);
	std::string GUID(buffer);
	return GUID;
}

void CDataBaseOpr::SaveVolumnMaterialInfoToTable(const AcArray<tagBodyXData>& allInfo, std::string Roadline_ID) {
	if (allInfo.length() == 0) return;
	assert(pdb_ != nullptr);
	srand((unsigned)time(NULL));
	char sql[500] = { 0 };   //sql语句
	const tagBodyXData& info = allInfo[0];

	//先删除可能存在的本线路的原有记录
	sprintf_s(sql, "delete from StatisticsBody where roadLine == '%s' and startStake == %f and endStake == %f and place == '%s'", 
		Roadline_ID.c_str(), info.startStake, info.endStake,
		info.isLeft? "Left" : "Right");
	int r = sqlite3_exec(pdb_, sql, NoUseCallBack, NULL, NULL);

	//再插入结果记录记录
	for (int index = 0; index < allInfo.length(); index++) {
		const tagBodyXData& info = allInfo[index];

		//再添加新加入的
		sprintf_s(sql, "INSERT INTO StatisticsBody (GUID,roadLine,startStake,endStake,MaterialID,volumn,bodyKind,place) VALUES ('%s','%s',%f,%f,%d,%f,%d,'%s');",
			GenerateGUID().c_str(), Roadline_ID.c_str(), info.startStake, info.endStake, info.materialID, info.volumn, info.sideKind,info.isLeft? "Left" : "Right");

		r = sqlite3_exec(pdb_, sql, NoUseCallBack, NULL, NULL);
	}
}

void CDataBaseOpr::SaveToTable_OldRoadActualWidth(const AcArray<tagMeasureWidth>& mw,std::string Roadline_ID) {
	assert(pdb_ != nullptr);
	char sql[500] = { 0 };   //sql语句

	//先删除可能存在的本线路的原有记录
	sprintf_s(sql,"delete from OldRoadActualWidth where RoadLine_ID == '%s'",Roadline_ID.c_str());
	int r = sqlite3_exec(pdb_, sql, NoUseCallBack, NULL, NULL);

	//再插入结果记录
	for (int index = 0; index < mw.length(); index++) {
		sprintf_s(sql, "INSERT INTO OldRoadActualWidth (stakeNo, LeftWidth,RightWidth,RoadLine_ID) VALUES (%f,%f,%f,'%s');", mw[index].stakeNo,mw[index].LeftWidth,mw[index].RightWidth, Roadline_ID.c_str());
		r = sqlite3_exec(pdb_, sql, NoUseCallBack, NULL, NULL);
	}
}


int qsortCompare1(const void * elem1, const void * elem2) {
	tagPavementSegment* ps1 =(tagPavementSegment*)(elem1);
	tagPavementSegment* ps2 = (tagPavementSegment*)(elem2);
	return (fabs(ps1->SortID) - fabs(ps2->SortID));
}

static AcArray<tagPavementSegment> parasePavementWidth(const std::string& jsontext,bool isLeft) {
	JsonCpp::Reader reader;
	JsonCpp::Value value;
	AcArray<tagPavementSegment>     pavementWidth;
	bool r = reader.parse(jsontext, value);
	if (!r)
	{
		::OutputDebugString(L"路幅信息解析失败！");
		return pavementWidth;
	}
	
	for (JsonCpp::Value::UInt index = 0; index < value["List"].size(); index++) {
		tagPavementSegment ps;
		ps.SortID = value["List"][index]["SortID"].asInt();
		if ( (isLeft && ps.SortID < 0) ||  (!isLeft && ps.SortID > 0) ) {
			ps.CrossSlop = value["List"][index]["SortCrossSlop"].asDouble();
			ps.width = value["List"][index]["SortWidth"].asDouble();
			ps.SortName = Utf82Unicode(value["List"][index]["SortName"].asString());
			pavementWidth.append(ps);
		}
	}

	//要保证路幅信息中，各路段按|SortID|的绝对值排升度（由小到大）
	qsort(pavementWidth.asArrayPtr(), pavementWidth.length(), sizeof(tagPavementSegment), qsortCompare1);
	#if false
	::OutputDebugString(L"路幅信息\n");
	char buffer[200];
	double totalWidth = 0.0;
	for (int index = 0; index < pavementWidth.length(); index++) {
		const tagPavementSegment& ps = pavementWidth[index];
		totalWidth += ps.width;
		sprintf_s(buffer, "SortID: %d	  width:%.3f	slop: %.3f\n", ps.SortID, ps.width, ps.CrossSlop);
		::OutputDebugStringA(buffer);
	}
	sprintf_s(buffer, "totla width :  %.3f\n", totalWidth);
	::OutputDebugStringA(buffer);
	#endif

	return pavementWidth;
}

static AcArray<tagPavementLayer> parasePavementStruct(const std::string& jsontext) {
	AcArray<tagPavementLayer> pavamentStruct;
	if (jsontext.empty())  return pavamentStruct;

	JsonCpp::Reader reader;
	JsonCpp::Value value;
	bool r = reader.parse(jsontext, value);
	if (!r) {
		::OutputDebugString(L"路面结构信息解析失败！");
		return pavamentStruct;
	}
	for (JsonCpp::Value::UInt index = 0; index < value["List"].size(); index++) {
		tagPavementLayer pl;
		pl.thickness = value["List"][index]["Ply"].asDouble();
		pl.materialID = value["List"][index]["Material"].asInt();
		pl.LayerName = Utf82Unicode(value["List"][index]["LayerName"].asString());
		pavamentStruct.append(pl);
	}
	return pavamentStruct;
}

tagComponentParameter parseCPValue(const JsonCpp::Value& jsonValue,bool& isBoundary) {
	tagComponentParameter cp;
	int type = jsonValue["type"].asInt();
	if (type == 0) {
		cp.componentKind = component_kind_divider;
	}
	else if (type == 1) {
		cp.componentKind = component_kind_bench;
	}
	else if (type == 2) {
		cp.componentKind = component_kind_margin;
	}

	cp.stepCount = jsonValue["step"].asInt();
	
	isBoundary = jsonValue["IsBoundary"].asBool();
	if (jsonValue.isMember("Grading")) {
		cp.grading = jsonValue["Grading"].asBool();
	}

	cp.materialID = jsonValue["MaterialID"].asInt();
	cp.benchToOutward = (jsonValue["place"].asInt() == 1); // 0 朝内， 1  朝外
	cp.Name = Utf82Unicode(jsonValue["Name"].asString());

	auto& p = jsonValue["parameters"];
	for (JsonCpp::Value::UInt j = 0; j < p.size(); j++) {
		if (p[j]["parameterName"] == "h1") {
			cp.h1 = p[j]["parameterValue"].asDouble();
		}
		else if (p[j]["parameterName"] == "h2") {
			cp.h2 = p[j]["parameterValue"].asDouble();
		}
		else if (p[j]["parameterName"] == "h3") {
			cp.h3 = p[j]["parameterValue"].asDouble();
		}
		else if (p[j]["parameterName"] == "h4") {
			cp.h4 = p[j]["parameterValue"].asDouble();
		}
		else if (p[j]["parameterName"] == "w1") {
			cp.w1 = p[j]["parameterValue"].asDouble();
		}
		else if (p[j]["parameterName"] == "w2") {
			cp.w2 = p[j]["parameterValue"].asDouble();
		}
		else if (p[j]["parameterName"] == "w3") {
			cp.w3 = p[j]["parameterValue"].asDouble();
		}
		else if (p[j]["parameterName"] == "w4") {
			cp.w4 = p[j]["parameterValue"].asDouble();
		}
		else if (p[j]["parameterName"] == "n1") {
			cp.n1 = p[j]["parameterValue"].asDouble();
		}
		else if (p[j]["parameterName"] == "n2") {
			cp.n2 = p[j]["parameterValue"].asDouble();
		}
		else if (p[j]["parameterName"] == "n3") {
			cp.n3 = p[j]["parameterValue"].asDouble();
		}
		else if (p[j]["parameterName"] == "n4") {
			cp.n4 = p[j]["parameterValue"].asDouble();
		}
	}

	//水平方向偏移，确保为>=0
	cp.horiOffset = jsonValue["InsertDot"]["x"].asDouble();

	//竖直方向上，有正负，负值表示位路面以下
	cp.anchorY = jsonValue["InsertDot"]["y"].asDouble();
	
	//only for debug
	//if (!isBoundary) {
	//	cp.grading = false;
	//	cp.horiOffset *= 0.8;
	//}
	

	return cp;
}

static void paraseComponentParameters(const std::string& jsontext, AcArray<tagComponentParameter>& section_cp, AcArray<tagComponentParameter>& boundary_cp) {
	JsonCpp::Reader reader;
	JsonCpp::Value value;
	AcArray<tagComponentParameter> componentParameters;
	bool r = reader.parse(jsontext, value);
	if (!r) {
		::OutputDebugString(L"组件参数信息解析失败！");
		return;
	}
	
	for (JsonCpp::Value::UInt index = 0; index < value["List"].size(); index++) {
		bool isBoundary = false;
		const tagComponentParameter& cp = parseCPValue(value["List"][index], isBoundary);
		if (isBoundary) {
			boundary_cp.append(cp);
		}
		else {
			section_cp.append(cp);
		}
	}
	
	//按x方向插入点从小到大排序
	if (boundary_cp.length() == 2) {
		if (boundary_cp[0].horiOffset > boundary_cp[1].horiOffset) {
			swap(boundary_cp[0], boundary_cp[1]);
		}
	}
	return;
}

int oldRoadSectionList_TableCallBack(void *data, int cNum, char *column_values[], char *column_names[]) {
	AcArray<tagSideSectionData>& result = *reinterpret_cast<AcArray<tagSideSectionData>*>(data);
	tagSideSectionData ssd;
	ssd.isNewRoad = false;
	ssd.stakeNo = ::atof(column_values[0]);
	ssd.isLeft = std::string(column_values[4]) == "Left";
	ssd.pavementWidth = parasePavementWidth(column_values[1],ssd.isLeft);
	ssd.pavamentStruct = parasePavementStruct(column_values[2]);
	AcArray<tagComponentParameter> null_cp;
	paraseComponentParameters(column_values[3], ssd.componentParameters, null_cp);
	result.append(ssd);
	return 0;
}

AcArray<tagSideSectionData> CDataBaseOpr::QueryOldRoadSections(const std::string& roadLineID) {
	AcArray<tagSideSectionData> result;
	assert(pdb_ != nullptr);
	char sql[500] = { 0 };   //sql语句
	sprintf_s(sql, "select DISTINCT stakeNum,Roadway,RoadStruct,CrossSection, place from oldRoadSectionList where roadLine == '%s';", roadLineID.c_str());
	int r = sqlite3_exec(pdb_, sql, oldRoadSectionList_TableCallBack, (void*)&result, NULL);
	return result;
}

AcArray<tagSideSectionData> CDataBaseOpr::QueryOldRoadSections(const std::string& roadLineID, double startStake, double endStake) {
	AcArray<tagSideSectionData> result;
	assert(pdb_ != nullptr);
	char sql[500] = { 0 };   //sql语句
	//sample: select stakeNum,Roadway,RoadStruct,CrossSection, place from oldRoadSectionList where roadLine == 'b14d27b7-7212-4e44-9b76-30095293feb2' and 74200 <= StakeNum and StakeNum <= 74210 order by StakeNum
	sprintf_s(sql, "select DISTINCT stakeNum,Roadway,RoadStruct,CrossSection, place from oldRoadSectionList where roadLine == '%s' and %f <= StakeNum and StakeNum <= %f order by StakeNum;", roadLineID.c_str(),startStake,endStake);
	int r = sqlite3_exec(pdb_, sql, oldRoadSectionList_TableCallBack, (void*)&result, NULL);
	return result;
}

static tagOverlapArea paraseOverlayAra(const std::string& jsontext)
{
	tagOverlapArea ol;
	if (jsontext.empty())  return ol;

	JsonCpp::Reader reader;
	JsonCpp::Value value;
	bool r = reader.parse(jsontext, value);
	if (!r) {
		::OutputDebugString(L"路面结构信息解析失败！");
		return ol;
	}
	ol.levelingLayerMaterialID = value["LevelingRule_MaterialID"].asInt();
	for (JsonCpp::Value::UInt index = 0; index < value["List"].size(); index++) {
		tagPavementLayer pl;
		pl.thickness  = value["List"][index]["Value"].asDouble();
		pl.materialID = value["List"][index]["MaterialID"].asInt();
		pl.Explain = Utf82Unicode(value["List"][index]["Explain"].asString());
		assert(pl.thickness > 0);
		ol.layers.append(pl);
	}
	return ol;
}

//解析老路地面线(四个点）,结果按x方向上排升序（从小到大）
AcArray<AcGePoint3d> parseGroundLine(const std::string& jsontext){
	AcArray<AcGePoint3d> groundLine;
	AcArray<tagPavementLayer> pavamentStruct;
	if (jsontext.empty())  return groundLine;

	JsonCpp::Reader reader;
	JsonCpp::Value value;
	bool r = reader.parse(jsontext, value);
	if (!r) {
		TRACE(L"老路面地面线信息解析失败！");
		return groundLine;
	}

	int zero = 0;
	AcGePoint3d pt1(value["left"][zero]["x1"].asDouble(), value["left"][zero]["y1"].asDouble(),0);
	groundLine.append(pt1);
	AcGePoint3d pt2(value["left"][1]["x2"].asDouble(), value["left"][1]["y2"].asDouble(), 0);
	groundLine.append(pt2);
	AcGePoint3d pt3(value["right"][zero]["x3"].asDouble(), value["right"][zero]["y3"].asDouble(), 0);
	groundLine.append(pt3);
	AcGePoint3d pt4(value["right"][1]["x4"].asDouble(), value["right"][1]["y4"].asDouble(), 0);
	groundLine.append(pt4);
	
	return groundLine;
}

int newRoadSectionList_TableCallBack(void *data, int cNum, char *column_values[], char *column_names[]) {
	AcArray<tagSideSectionData>& result = *reinterpret_cast<AcArray<tagSideSectionData>*>(data);
	tagSideSectionData ssd;
	ssd.isNewRoad = true;
	ssd.stakeNo = ::atof(column_values[0]);
	ssd.isLeft = std::string(column_values[4]) == "Left";
	ssd.pavementWidth = parasePavementWidth(column_values[1], ssd.isLeft);
	ssd.pavamentStruct = parasePavementStruct(column_values[2]);
	std::wstring sf = Utf82Unicode(column_values[3]);
	paraseComponentParameters(column_values[3], ssd.componentParameters, ssd.dividerBenchParameters);
	int v = ::atol(column_values[7]);
	ssd.newRoadType1 = new_road_type_null;
	if (1 <= v && v <= 3) {
		ssd.newRoadType1 = (tagNewRoadType)(v);
	}

	v = ::atol(column_values[8]);
	ssd.newRoadType2 = new_road_type_null;
	if (1 <= v && v <= 3) {
		ssd.newRoadType2 = (tagNewRoadType)(v);
	}

	v = ::atol(column_values[9]);
	ssd.wideningType = widening_type_null;
	if (1 <= v && v <= 4) {
		ssd.wideningType = (tagWideningType)(v);
	}

	if (ssd.wideningType != widening_type_build_new) {
		ssd.overlapArea1 = paraseOverlayAra(column_values[5]);
		ssd.overlapArea2 = paraseOverlayAra(column_values[6]);
		ssd.overlapSame = (::atol(column_values[10]) == 1);	 // 1:表示同侧  0:表示异侧
		ssd.groundLine = parseGroundLine(column_values[11]); //解析两条路地面线
	}
	result.append(ssd);
	return 0;
}

AcArray<tagSideSectionData> CDataBaseOpr::QueryNewRoadSections(const std::string& roadLineID) {
	AcArray<tagSideSectionData> result;
	assert(pdb_ != nullptr);
	char sql[500] = { 0 };   //sql语句
	sprintf_s(sql, "select DISTINCT stakeNum,Roadway,RoadStruct,CrossSection, place,OverlayArea1,OverlayArea2,newRoadType,wideningType,overlapsampe,OldPoints from newRoadSectionList where roadLine == '%s';", roadLineID.c_str());
	int r = sqlite3_exec(pdb_, sql, newRoadSectionList_TableCallBack, (void*)&result, NULL);
	return result;
}

AcArray<tagSideSectionData> CDataBaseOpr::QueryNewRoadSections(const std::string& roadLineID, double startStake, double endStake) {
	AcArray<tagSideSectionData> result;
	assert(pdb_ != nullptr);
	char sql[500] = { 0 };   //sql语句
	//Sample: select DISTINCT stakeNum,Roadway,RoadStruct,CrossSection, place,OverlayArea1,OverlayArea2,newRoadType1,newRoadType2,wideningType,overlapsampe,OldPoints from newRoadSectionList where roadLine == 'b9eb1274-1aeb-4362-bd70-7208e25d4c19' and 98350 <= StakeNum and StakeNum <= 98440 order by StakeNum;
	sprintf_s(sql, "select DISTINCT stakeNum,Roadway,RoadStruct,CrossSection, place,OverlayArea1,OverlayArea2,newRoadType1,newRoadType2,wideningType,overlapsampe,OldPoints from newRoadSectionList where roadLine == '%s' and %f <= StakeNum and StakeNum <= %f order by StakeNum;", roadLineID.c_str(),startStake,endStake);
	int r = sqlite3_exec(pdb_, sql, newRoadSectionList_TableCallBack, (void*)&result, NULL);
	return result;
}