#pragma once
#include "pch.h"
bool CreateLayer(AcDbDatabase* pDbDatabase, const ACHAR* layerName);
void SetLayerColor();
void DeleteLayer();
void IterateALayer();
AcArray<AcDbObjectId>  GetEntitiesOnLayer(AcDbDatabase* pDbDatabase, const ACHAR* layerName, const AcRxClass* pClassKind);
void MoveEntitiesToLayer(AcArray<AcDbObjectId> entities, ACHAR* layer_name);
void MoveEntityToLayer(AcDbObjectId entity, ACHAR* layer_name);