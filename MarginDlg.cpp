// CMarginDlg.cpp : implementation file
//

#include "pch.h"
#include "MarginDlg.h"
#include "afxdialogex.h"


// CMarginDlg dialog

IMPLEMENT_DYNAMIC(CMarginDlg, CDialogEx)

CMarginDlg::CMarginDlg(tagComponentParameter cp,CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_MARGIN_DLG, pParent)
	, m_cp(cp)
{

}

CMarginDlg::~CMarginDlg()
{
}

void CMarginDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_B1, m_cp.w_1);
	DDX_Text(pDX, IDC_EDIT_B2, m_cp.w_2);
	DDX_Text(pDX, IDC_EDIT_B3, m_cp.w_3);
	DDX_Text(pDX, IDC_EDIT_H1, m_cp.h1);
	DDX_Text(pDX, IDC_EDIT_H2, m_cp.h2);
	DDX_Text(pDX, IDC_EDIT_H3, m_cp.h3);
	DDX_Text(pDX, IDC_EDIT_N,  m_cp.n_1);
}


BEGIN_MESSAGE_MAP(CMarginDlg, CDialogEx)
	ON_BN_CLICKED(IDOK, &CMarginDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CMarginDlg message handlers


void CMarginDlg::OnBnClickedOk()
{
	UpdateData(true);
	CDialogEx::OnOK();
}
