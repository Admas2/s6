﻿#pragma once
#include "pch.h"
#define  MAX_ELEMENT_NUM 10
class CreateBenchTool
{
public:
	enum bench_kind  {
		bench_kind_vertial_through = 0, // 竖向贯通,用于新路拼宽时使用
		bench_kind_horizontal_through = 1 //水平贯通，用道路内容台阶使用
	};
	struct tagParam {
		double sx = 0.0, sy = 0.0;   //台阶的起始点坐标 
		double h[MAX_ELEMENT_NUM] = { 0 };				 // 各级台阶的高
		double w[MAX_ELEMENT_NUM] = { 0 };				 // 各级台阶的宽
		int count = 0;								//台阶级数
		double width = 0.0, height = 0.0;	//路面结构的宽,高
		bench_kind kind = bench_kind_horizontal_through;
		bool isExpandY = false;					//是否扩展Y方向，用于新路拼宽时生成截面面域时

		bool extendInward  = true;		 //延伸形成包围面域是向内还是向外
		bool isLeft = true;						 //台阶是左Y轴的左侧还是右侧
		//当	kind == bench_kind_horizontal_through 时，extendInward属性设置无效
		//固定设置为，当为Y轴左边时，向左延伸成面域， 当在Y轴右边时，向右延伸成面域

		//台阶边界向左：	__|
		//						__|
		//				 __|
		//台阶边界向右：
		// |__
		//		|__		
		//			 |__
		bool stepIsLeft = true;//台阶边界是向左，还是向右
		int colorIndex;
		double stakeNo;
		std::wstring componentName;
		AcArray<tagPavementSegment> pw;
	};
private:
	tagParam param_;
public:
	CreateBenchTool(const tagParam& param);
	AcDbObjectId CreateRegion();
private:
	int CreateVertialArray(AcGePoint2d pos[]);
	int CreateHorizontalArray(AcGePoint2d pos[]);
};

