﻿#ifndef PCH_H
#define PCH_H

// add headers that you want to pre-compile here
#include "framework.h"
#include "resource.h"

#include <aced.h>
#include <rxregsvc.h>
#include <dbents.h>
#include <dbapserv.h>
#include <dbregion.h>
#include <dbsol3d.h>
#include <aced.h>
#include <rxregsvc.h>
#include <dbents.h>
#include <dbapserv.h>
#include <dbsol3d.h>
#include <aced.h>
#include <rxregsvc.h>
#include <dbents.h>
#include <dbapserv.h>
#include <dbregion.h>
#include <dbsol3d.h>
#include <dbspline.h>
#include <adscodes.h>
#include <acedCmdNF.h>
#include <afxcontrolbars.h>
#include <geassign.h>
#include <rxmfcapi.h>
#include "TypeDef.h"
#include <afxwin.h>
#include <fstream>
#include <iostream>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>


#define EXPORT_DEFINE   extern "C" __declspec(dllexport)

//自定义命令处理
void CommandCreateBox();
void CommandCreateCylinder();
void CommandCreateSpire();
void CommandBooleanOperation();
void CommandCreateRevolveBody();
void CommandCreateLine();
void CommandEntityCopy();
void CommandHelloWorld();
void CommandMisc();
void CommandCreateLoft();
void CommandCreateLoft2();
void CommandCreateLoft3();
void CommandCreateSpline();
void CommandCreatePolyline();
void CommandCreateRegion();
void CommandEntityMirror();
void CommandTestVector();
void CommandTestMoveCurrentUCS();
void CommandTestAddInfoToEnttiy();
void CommandTestQuveryAEntity();
AcDbObjectIdArray PickEntities();

double CalculateAngleOfTwoVector(const AcGeVector3d& a, const AcGeVector3d& b);
Acad::ErrorStatus EntityMove(AcDbEntity *pEnt, AcGePoint3d ptBase, AcGePoint3d ptDest);
Acad::ErrorStatus EntityScale(AcDbEntity *pEnt, AcGePoint3d ptBase, double scaleFactor);
Acad::ErrorStatus EntityRotate(AcDbEntity *pEnt, AcGePoint3d ptBase, AcGeVector3d vec, double rotation);
Acad::ErrorStatus CreateRegion(const AcDbObjectIdArray& curveIds, AcDbObjectIdArray& regionIds);
void ZffCHAP2AddLine();
void ZffCHAP2ChangeColor();


//模块导出接口
EXPORT_DEFINE int SetDatabasePath(wchar_t* dbName);
EXPORT_DEFINE int CalcActualWidthByPickLine(char* lineName);
EXPORT_DEFINE int CreateRoadModel(char* RoadLine_Name, double startStake, double endStake, int isNewRoad);
EXPORT_DEFINE int CreateExtendRoad(char* newRoadName, char* oldRoadName, double startStake, double endStake);

extern Acad::ErrorStatus es;
using namespace std;

#endif //PCH_H
