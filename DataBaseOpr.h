﻿#pragma once
#include "TypeDef.h"
#include <sqlite3.h>

class CDataBaseOpr
{
private:
	sqlite3 *pdb_ = nullptr;
private:
	int static MaterialTableCallBack(void *data, int cNum, char *column_values[], char *column_names[]);
	int static RoadLineTableCallBack(void *data, int cNum, char *column_values[], char *column_names[]);
	int static Model_ContStakeXYZTableCallBack(void *data, int cNum, char *column_values[], char *column_names[]);
	int static sectionInfoTableCallBack(void *data, int cNum, char *column_values[], char *column_names[]);
public:
	CDataBaseOpr();
	~CDataBaseOpr();
	bool Open(std::string dbName);
	bool Open();
	void Close();

	AcArray<tagMaterialKind> QueryMaterialList();
	std::string  QueryRoadLineID(const char* RoadLine_Name);
	AcArray<tagFitPoint>   QueryRoadLineCoordinate(std::string roadLineID);
	AcArray<tagFitPoint>   QueryRoadLineCoordinate(std::string roadLineID, double startStake, double endStake);
	AcArray<tagLineInterval> QueryRoadNoModelInervals(std::string roadLineID);
	AcArray<tagLineInterval> QueryRoadNoModelInervals(std::string roadLineID, double startStake, double endStake);
	AcArray<tagLineInterval>		QueryRoadModelSegments(std::string roadLineID, double startStake, double endStake);
	AcArray<tagLineInterval>		QueryRoadModelSegments(std::string roadLineID);
	void SaveToTable_OldRoadActualWidth(const AcArray<tagMeasureWidth>& mw, std::string Roadline_ID);
	void SaveVolumnMaterialInfoToTable(const AcArray<tagBodyXData>& allInfo, std::string Roadline_ID);
	AcArray<tagSideSectionData> QueryOldRoadSections(const std::string& roadLineID);
	AcArray<tagSideSectionData> QueryOldRoadSections(const std::string& roadLineID, double startStake, double endStake);
	AcArray<tagSideSectionData> QueryNewRoadSections(const std::string& roadLineID);
	AcArray<tagSideSectionData> QueryNewRoadSections(const std::string& roadLineID, double startStake, double endStake);
	
private:
	void QuerySectionsScheme(tagSideSectionData& sd);
};

