#pragma once
#include "rxevent.h"
#include "rxboiler.h"
class RDEventReactor : public AcRxEventReactor {

public:
	ACRX_DECLARE_MEMBERS(RDEventReactor);
	RDEventReactor();
	~RDEventReactor();
	void dwgFileOpened(AcDbDatabase*, ACHAR * fileName) override;
};

class RDDatabaseReactor : public AcDbDatabaseReactor {

public:
	ACRX_DECLARE_MEMBERS(RDDatabaseReactor);
	RDDatabaseReactor();
	~RDDatabaseReactor();
	void    objectAppended(const AcDbDatabase* dwg, const AcDbObject* obj) override;
};

class RDDocumentReactor : public AcApDocManagerReactor {

public:
	ACRX_DECLARE_MEMBERS(RDDocumentReactor);
	RDDocumentReactor();
	~RDDocumentReactor();
	void documentCreated(AcApDocument* pDocCreating) override;
	void documentToBeDestroyed(AcApDocument* pDocToDestroy) override;
};

class RDEditorReactor : public AcEditorReactor2 {

public:
	ACRX_DECLARE_MEMBERS(RDEditorReactor);
	RDEditorReactor();
	~RDEditorReactor();
	void    commandWillStart(const TCHAR* cmdStr) override;
	void    commandEnded(const TCHAR* cmdStr) override;
};

