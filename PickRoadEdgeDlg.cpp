// CPickRoadEdgeDlg.cpp : implementation file
//

#include "pch.h"
#include "PickRoadEdgeDlg.h"


// CPickRoadEdgeDlg dialog

IMPLEMENT_DYNAMIC(CPickRoadEdgeDlg, CAdUiBaseDialog)

CPickRoadEdgeDlg::CPickRoadEdgeDlg(CWnd* pParent, HINSTANCE hDialogResource)
	: CAdUiBaseDialog(IDD_PICK_ROAD_EDGE_DLG, pParent, hDialogResource)
{

}

CPickRoadEdgeDlg::~CPickRoadEdgeDlg()
{
}

void CPickRoadEdgeDlg::DoDataExchange(CDataExchange* pDX)
{
	CAdUiBaseDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CPickRoadEdgeDlg, CAdUiBaseDialog)
	ON_BN_CLICKED(IDC_PICK_EDGE, &CPickRoadEdgeDlg::OnBnClickedPickEdge)
END_MESSAGE_MAP()


// CPickRoadEdgeDlg message handlers


void CPickRoadEdgeDlg::OnBnClickedPickEdge()
{
	BeginEditorCommand();
	int r = CalcActualWidthByPickLine("K3");
	CompleteEditorCommand();
}
