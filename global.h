﻿#include "pch.h"
#include "ModuleApp.h"

#define MESSAGE_BOX ::MessageBox(acedGetAcadDwgView()->GetSafeHwnd(), logInfo.GetString(), L"道路改扩建", MB_OK);
#define RDTRACE  if(!gOptions.disableLog) { TRACE(logInfo); acutPrintf(logInfo);	 theApp.AppendInfoToLogFile(); }

AcDbObjectId CreateLoftedBodyWidthCircleSection(double radius, AcGePoint3dArray path);
bool ReadRoadDataFromDB(tagRoadwayData& rd);

AcDbObjectId AppendEntityToModelSpace(AcDbEntity* pEntity);
AcDbObjectId AppendEntityToModelSpace(AcDbEntity* pEntity, eLayerID layerIndex);
void RemoveEntityFromModelSpace(AcDbObjectId entityID);
void RemoveEntityFromModelSpace(AcArray<AcDbObjectId> entityIDs);
void RemoveEntityFromModelSpace(AcArray<AcDbObjectId> IDs);
AcDbRegion* CreateRegionAtDirectionPosition(AcGePoint3d position, AcGeVector3d normal, AcGeVector3d xAixs, AcDbRegion* pRefRegion);
AcArray<AcDbObjectId> BoydiesBooleanOpr(const AcArray<AcDbObjectId>& opr1, const AcArray<AcDbObjectId>& opr2, AcDb::BoolOperType type);
double CalculateBodyVolumn(AcDbObjectId entityID);
void SaveAsFBX(ACHAR* file_name, AcArray<AcDbObjectId> bodies);

AcArray<AcDbObjectId> CreateMultiRectWithSlopRegion(int segmentNum, int rowNum, double segmentWidth[], double rowHeight[], int rowMaterialID[],double slops[], bool isLeft, int colorIndexStart,eSectionSideKind  sideKind,double stakeNo);
void calcRoadLineVecTagent(AcArray<tagFitPoint>& roadLine);
AcDbObjectId SliceABody(AcDbObjectId bodyID, AcGePoint3d point, AcGeVector3d dir);
bool MySetStatusText(LPCTSTR lpszText);

AcGePoint3d GetMaxYPoint(AcGePoint3dArray points);
void LineInstersectRects(AcDbObjectId lineID, AcArray<AcDbObjectId> rects, AcGePoint3dArray& result);
void AddPtWithUniqueness(AcGePoint3dArray& dest, const AcGePoint3dArray& src);
void AddPtWithUniqueness(AcGePoint3dArray& points, AcGePoint3d pt);
void TagAPoint(AcGePoint3d pt, int colorIndex, double radius);
int qsortCompare2(const void * elem1, const void * elem2);
void RemoveEntityFromModelSpace(AcArray<AcDbObjectId> IDs);
bool GetMaxDistancePt(const AcGePoint3dArray& points, AcGePoint3d& ptFarOfOrigin, AcGePoint3d& ptNearOfOrigin);
Acad::ErrorStatus CalculateEntityAreaOrVolumeExtents(AcDbEntity* pEntity, double& areaOrVolume, AcDbExtents& extents);
void MoveToViewCenter(AcArray<tagFitPoint>& fitPoints);
double CalcStartDeltahHeight(double horiOffset, const AcArray<tagPavementSegment>& pw);
void CalculateGuideLinesOf2Region(AcDbEntity* pEntity1, AcDbEntity* pEntity2,AcArray<AcDbEntity*>& guideCurves);
AcGePoint3d GetExtentsCenter(AcDbExtents extents);
double GetExtentsRadius(AcDbExtents extents);

extern AcArray<AcArray<tagPavementLayer>> gPredefinedPavementStructureList; //预定义路面结构集
extern AcArray<AcArray<tagPavementSegment>> gPredefinedPavementWidthList; //预定义路面结构集
extern tagOptions  gOptions;
extern CModuleApp theApp;
extern CString logInfo;