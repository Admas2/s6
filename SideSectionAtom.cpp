﻿#include "pch.h"
#include "SideSectionAtom.h"
#include "ComponentDivider.h"
#include "ComponentBench.h"
#include "ComponentMargin.h"
#include "CreateRegionTool.h"
#include "CreateBenchTool.h"
#include "BooleanOperation.h"
#include "global.h"
#include "LayerOperation.h"
CSideSectionAtom::CSideSectionAtom(double stakeNo, bool isLeft,
	const AcArray<tagPavementSegment>& pavementWidth,
	const AcArray<tagPavementLayer>& pavamentStruct,
	const AcArray<tagComponentParameter>& componentParameters,
	eSectionSideKind  sideKind) :isLeft_(isLeft),
	stakeNo_(stakeNo),pavementWidth_(pavementWidth), pavamentStruct_(pavamentStruct), componentParameters_(componentParameters), sideKind_(sideKind)
{
	assert(pavementWidth_.length() > 0);
	assert(pavamentStruct_.length() > 0);
	CalculateOrginWidthHeight();
}

CSideSectionAtom::CSideSectionAtom(double stakeNo, bool isLeft,
	const AcArray<tagPavementSegment>& pavementWidth,
	const AcArray<tagPavementLayer>& pavamentStruct, eSectionSideKind  sideKind) : 
	stakeNo_(stakeNo), isLeft_(isLeft),
	pavementWidth_(pavementWidth),
	pavamentStruct_(pavamentStruct), sideKind_(sideKind)
{
	assert(pavementWidth_.length() > 0);
	assert(pavamentStruct_.length() > 0);
	CalculateOrginWidthHeight();
}
void CSideSectionAtom::CalculateOrginWidthHeight() {
	int rowNum = pavamentStruct_.length();
	originalHeight_ = 0.0;
	for (int index = 0; index < rowNum; index++) {
		originalHeight_ += pavamentStruct_[index].thickness;
	}

	int segmentNum = pavementWidth_.length();
	originalWidth_ = 0.0;
	for (int index = 0; index < segmentNum; index++) {
		originalWidth_ += pavementWidth_[index].width;
	}
}

double CSideSectionAtom::OriginalWidth() {
	return originalWidth_;
}

double CSideSectionAtom::OriginalHeight() {
	return originalHeight_;
}

CSideSectionAtom::~CSideSectionAtom() {

}

AcArray<AcDbObjectId> CSideSectionAtom::GetRegions() const {
	return acdbRegions_;
}

void CSideSectionAtom::appendComponent(CSectionComponent* component) {

	//1.创建组件正负两个面域
	AcDbObjectId postiveRegionID = component->CreatePositiveComponet();
	AcDbObjectId negativeRegionID = component->CreateNegaiveComponet();

	//2.从路面结构中选“减掉”此组件的两个面域
	AcArray<AcDbObjectId> sliceIDS;
	sliceIDS.append(postiveRegionID);
	sliceIDS.append(negativeRegionID);
	SubtractRegions(acdbRegions_, sliceIDS);

	//3.收集此组件及其域
	componets_.append(component);
	acdbRegions_.append(postiveRegionID);

	//4.删除掉无用的负面域
	RemoveEntityFromModelSpace(negativeRegionID);
}

//创建路面结构层
void CSideSectionAtom::CreatePavementStruct(tagNewRoadType newRoadType) {

	//1.如果存在“带放坡的边部构造”，那么创建时要在外侧放坡
	double slop_ratio = 0.0;
	for (int index = 0; index < componentParameters_.length(); index++) {
		if (componentParameters_[index].componentKind == component_kind_margin && componentParameters_[index].grading) {
			slop_ratio = componentParameters_[index].n1;
			break;
		}
	}

	//2.整理参数
	int segmentNum = pavementWidth_.length();
	int rowNum = pavamentStruct_.length();
	double segmentWidth[50] = { 0 }, slops[50] = { 0 }, rowHeight[50] = { 0 };
	int rowMaterialID[50] = { 0 };
	double deltaH1 = 0.0;  //这个变量记录了由于有横坡导致外侧边缘的变化总高度
	for (int index = 0; index < segmentNum; index++) {
		segmentWidth[index] = pavementWidth_[index].width;
		slops[index] = pavementWidth_[index].CrossSlop;
		deltaH1 += slops[index] * segmentWidth[index];
	}

	for (int index = 0; index < rowNum; index++) {
		rowMaterialID[index] = pavamentStruct_[index].materialID;
		rowHeight[index] = pavamentStruct_[index].thickness;
	}

	//3.如果存在外侧边坡，先要延伸矩形宽以包括进来
	if (slop_ratio > 0.0) {
		segmentWidth[segmentNum - 1] += slop_ratio * originalHeight_;
	}

	//4.创建路面结构
	AcArray<AcDbObjectId> r = CreateMultiRectWithSlopRegion(
		segmentNum,
		rowNum,
		segmentWidth,
		rowHeight,
		rowMaterialID,
		slops,
		isLeft_,
		colorIndexStart_, sideKind_,stakeNo_);

	//5.如果存在外侧边坡，要减去一个倒三角形面域
	if (slop_ratio > 0.0) {
		double deltaH2 = slop_ratio * originalHeight_ * slops[segmentNum - 1];

		//作一个倒三角形面域
		AcGePoint2d pos[3];

		pos[0].x = -originalWidth_;
		pos[0].y = 0;
		pos[0].y += deltaH1;

		pos[1] = pos[0];
		pos[1].x -= slop_ratio * originalHeight_;
		pos[1].y += deltaH2;

		pos[2] = pos[1];
		pos[2].y -= originalHeight_;

		CreateRegionTool  regionTool;
		regionTool.Init();
		for (int index = 0; index < 3; index++) {
			if (!isLeft_) {
				pos[index].x *= -1;
			}
			regionTool.AppendKeyPoint(pos[index]);
		}
		AcDbObjectId triangeID = regionTool.CreateRegion();

		//从现在路面结构中 减去这个面三角形面域，以形成外侧边坡形状
		AcArray<AcDbObjectId> sliceIDs;
		sliceIDs.append(triangeID);
		SubtractRegions(r, sliceIDs);
		RemoveEntityFromModelSpace(triangeID);
	}


	if (newRoadType == new_road_type_step_milling) {
		MillingWithStep(r, originalWidth_, originalHeight_);
	}

	acdbRegions_.append(r);
	sideRoadStructRegions_ = r;
}

AcArray<AcDbObjectId> CSideSectionAtom::GetSideRoadStructRegions() {
	return sideRoadStructRegions_;
}

//分台阶铣刨
void CSideSectionAtom::MillingWithStep(AcArray<AcDbObjectId>& rects, double totalW, double totalH) {
	//1.创建两条老路地面线
	AcDbLine* pLine = new AcDbLine(groundLine_[0], groundLine_[1]);
	AcDbObjectId groudLineID1 = AppendEntityToModelSpace(pLine, layer_Sections);

	pLine = new AcDbLine(groundLine_[2], groundLine_[3]);
	AcDbObjectId groudLineID2 = AppendEntityToModelSpace(pLine, layer_Sections);

	//2.求老路地面线与与路面结构的交点
	AcGePoint3dArray& points = theApp.BorrowGlobalPt3dAry();
	LineInstersectRects(groudLineID1, rects, points);
	LineInstersectRects(groudLineID2, rects, points);
	if (points.length() < 2) {
		logInfo.Format(L"老路地面线 与 新路没有交点，没有生成分台阶铣刨 面 桩号:%.2f\n", stakeNo_); RDTRACE
		#if true
				RemoveEntityFromModelSpace(groudLineID1);
				RemoveEntityFromModelSpace(groudLineID2);
		#else
				//将地面线移动other层以供调试
				MoveEntityToLayer(groudLineID1, gOptions.layers[layer_Other]);
				MoveEntityToLayer(groudLineID2, gOptions.layers[layer_Other]);
		#endif
		theApp.ReturnGlobalPt3dAry(&points);
		return ;
	}

	AcArray<AcDbObjectId>  tempTestLineIDs;
	AcArray<tagPointPair>	 somePointPairs;
	AcGePoint3dArray& points2 = theApp.BorrowGlobalPt3dAry();
	for (int j = 0; j < points.length(); j++) {
		//TagAPoint(points[j], 2, 0.2);
		//4.从交点向下创建一竖直线
		AcGePoint3d start = points[j];
		AcGePoint3d end = points[j];
		start.y -= 0.0001; // 不将这根线的起点与交点重合，以防止后面被选中
		end.y -= totalH;
		AcDbLine* pLine = new AcDbLine(start, end);
		pLine->setColorIndex(10);
		AcDbObjectId lineID = AppendEntityToModelSpace(pLine, layer_Sections);
		tempTestLineIDs.append(lineID);

		//5.求此竖直线与路面结构的交点
		points2.removeAll();
		LineInstersectRects(lineID, rects, points2);
		if (points2.isEmpty()) continue;
		AcGePoint3d maxYPt = GetMaxYPoint(points2);
		//TagAPoint(maxYPt, 3, 0.2);
		somePointPairs.append({ points[j] ,maxYPt });
		//TRACE(L" %.2f,  %.2f -------  %.2f,  %.2f\n", points[j].x, points[j].y, maxYPt.x, maxYPt.y);
	}
	if (somePointPairs.isEmpty()) {
		logInfo.Format(L"老路地面线 与 新路没有交点，没有生成分台阶铣刨 面 桩号:%.2f\n", stakeNo_); RDTRACE
		theApp.ReturnGlobalPt3dAry(&points);
		return;
	}

	//6.将控制点在x方向排升序
	qsort(somePointPairs.asArrayPtr(), somePointPairs.length(), sizeof(tagPointPair), qsortCompare2);

	//7.创建一个包围面域
	CreateRegionTool tool;
	//加下台阶边
	int num = somePointPairs.length();
	for (int index = 0; index < num; index++) {
		//TRACE(L" %.2f,  %.2f -------  %.2f,  %.2f\n", somePointPairs[index].pt1.x, somePointPairs[index].pt1.y, somePointPairs[index].pt2.x, somePointPairs[index].pt2.y);
		tool.AppendKeyPoint(somePointPairs[index].pt1);
		tool.AppendKeyPoint(somePointPairs[index].pt2);
	}
	//产生一个足够大的包围边
	AcGePoint3d ptB = somePointPairs[num - 1].pt2;
	ptB.y -= totalH;
	AcGePoint3d ptC = ptB;
	if (isLeft_) {
		ptC.x += totalW;
	}
	else {
		ptC.x -= totalW;
	}
	AcGePoint3d ptD = ptC;
	ptD.y = somePointPairs[0].pt1.y;

	tool.AppendKeyPoint(ptB);
	tool.AppendKeyPoint(ptC);
	tool.AppendKeyPoint(ptD);
	AcDbObjectId sliceID = tool.CreateRegion(4);

	//8.减掉包围面域
	AcArray<AcDbObjectId> sliceIDs;
	sliceIDs.append(sliceID);
	SubtractRegions(rects, sliceIDs);
	RemoveEntityFromModelSpace(sliceID);

	//9.删除辅助线
	RemoveEntityFromModelSpace(tempTestLineIDs);
	RemoveEntityFromModelSpace(groudLineID1);
	RemoveEntityFromModelSpace(groudLineID2);
	theApp.ReturnGlobalPt3dAry(&points);
}


void CSideSectionAtom::CreatePavementComponent() {
	for (int index = 0; index < componentParameters_.length(); index++) {
		const tagComponentParameter& cp = componentParameters_[index];
		CSectionComponent* component = nullptr;
		switch (cp.componentKind) {
		case component_kind_divider:
		{
			component = new CComponentDivider(cp, this, isLeft_);		 //分隔带
			break;
		}
		case component_kind_bench:
		{
			component = new ComponentBench(cp, this, isLeft_);
			break;
		}
		case component_kind_margin:
		{
			component = new ComponentMargin(cp, this, isLeft_);	 //边部构造
			break;
		}
		default: {assert(false); }
		}

		if (component != nullptr) appendComponent(component);
	}

}

void CSideSectionAtom::Create(tagNewRoadType newRoadType) {

	assert(acdbRegions_.length() == 0);
	assert(newRoadType != new_road_type_null);

	//1.创建路面结构
	CreatePavementStruct(newRoadType);

	//2.创建本截面中组件
	CreatePavementComponent();
}

//isInward 表示分隔台阶形面域的方向,也即是要“去掉"内容的方式 true 表示朝向设计中线
void CSideSectionAtom::Create(const tagComponentParameter& dividerBench, bool isInward, tagNewRoadType newRoadType) {
	assert(acdbRegions_.length() == 0);
	assert(newRoadType != new_road_type_null);
	assert(dividerBench.componentKind == component_kind_bench);

	//1.创建路面结构
	CreatePavementStruct(newRoadType);

	RemovePart(dividerBench, isInward);

	//2.创建本截面中组件
	CreatePavementComponent();
}

void CSideSectionAtom::Create(const tagComponentParameter& dividerBench1, const tagComponentParameter& dividerBench2, tagNewRoadType newRoadType) {
	assert(acdbRegions_.length() == 0);
	assert(newRoadType != new_road_type_null);
	assert(dividerBench1.componentKind == component_kind_bench);
	assert(dividerBench2.componentKind == component_kind_bench);

	//1.创建路面结构
	CreatePavementStruct(newRoadType);

	RemovePart(dividerBench1, dividerBench2);

	//2.创建本截面中组件
	CreatePavementComponent();
}

double CSideSectionAtom::Totalthickness() {
	const AcArray<tagPavementLayer>&   pavamentStruct = pavamentStruct_;
	double d = 0;
	for (int index = 0; index < pavamentStruct_.length(); index++) {
		d += pavamentStruct_[index].thickness;
	}
	return d;
}

double CSideSectionAtom::GetOutwardSlop() {
	const AcArray<tagPavementSegment>& pw = pavementWidth_;
	int l = pw.length() - 1;
	return pw[l].CrossSlop;
}

const AcArray<tagPavementSegment>& 	CSideSectionAtom::GetPavementWidth() {
	return pavementWidth_;
}



//dividerBench 分隔台阶的尺寸参数
//isInward 表示分隔台阶形面域的方向 true 表示朝向设计中线
AcDbObjectId CSideSectionAtom::CreateSliceBenchRegion(const tagComponentParameter& dividerBench, bool isInward) {
	CreateBenchTool::tagParam param;
	param.width = OriginalWidth() * 2;// 注意： 这里用更大的宽度作面域，以将可能存在的斜坡包括进来，再通过求交将多余部分去掉
	param.height = Totalthickness();
	if (isLeft_) {
		param.sx = -dividerBench.horiOffset;
	}
	else {
		param.sx = +dividerBench.horiOffset;
	}
	param.sy = dividerBench.anchorY;
	param.h[0] = dividerBench.h1;
	param.h[1] = dividerBench.h2;
	param.h[2] = dividerBench.h3;
	param.h[3] = dividerBench.h4;
	param.w[0] = dividerBench.w1;
	param.w[1] = dividerBench.w2;
	param.w[2] = dividerBench.w3;
	param.w[3] = dividerBench.w4;
	param.count = dividerBench.stepCount;
	param.isExpandY = true;
	param.kind = CreateBenchTool::bench_kind_vertial_through;
	param.isLeft = isLeft_;
	if (isLeft_) {
		param.stepIsLeft = dividerBench.benchToOutward;
	}
	else {
		param.stepIsLeft = !dividerBench.benchToOutward;
	}
	param.extendInward = isInward;
	param.stakeNo = stakeNo_;
	param.componentName = dividerBench.Name;
	param.pw.append(pavementWidth_);

	CreateBenchTool benchTool(param);
	AcDbObjectId benchID = benchTool.CreateRegion();
	return benchID;
}

//isInward 表示分隔台阶形面域的方向 true 表示朝向设计中线
void CSideSectionAtom::RemovePart(const tagComponentParameter& dividerBench, bool isInward) {

	AcDbObjectId benchID = CreateSliceBenchRegion(dividerBench, isInward);
	AcArray<AcDbObjectId> sliceIDS;
	sliceIDS.append(benchID);
	SubtractRegions(acdbRegions_, sliceIDS);

	RemoveEntityFromModelSpace(benchID);
}

void CSideSectionAtom::RemovePart(const tagComponentParameter& dividerBenchInward, const tagComponentParameter& dividerBenchOutward) {
	AcDbObjectId benchID1 = CreateSliceBenchRegion(dividerBenchInward, true);
	AcDbObjectId benchID2 = CreateSliceBenchRegion(dividerBenchOutward, false);
	AcArray<AcDbObjectId> sliceIDS;
	sliceIDS.append(benchID1);
	sliceIDS.append(benchID2);
	SubtractRegions(acdbRegions_, sliceIDS);
	RemoveEntityFromModelSpace(benchID1);
	RemoveEntityFromModelSpace(benchID2);
}

void CSideSectionAtom::SetGroundLine(AcArray<AcGePoint3d> groundLine) {
	groundLine_ = groundLine;
}