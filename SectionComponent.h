﻿#pragma once
#include "pch.h"
class CSideSectionAtom;
class CSectionComponent
{
protected:
	tagComponentParameter cp_;
	CSideSectionAtom* owner_section_ = nullptr;
	bool isLeft_ =  true; // 对于单幅路 isLeft == true，对于双幅表示组件位于左侧还是右侧
	int colorIndex_ = 7;  //默认7 号白色
public:
	CSectionComponent(const tagComponentParameter& cp, CSideSectionAtom* owner_section,bool isLeft);
	~CSectionComponent();
	virtual AcDbObjectId CreatePositiveComponet() = 0;
	virtual AcDbObjectId CreateNegaiveComponet();
};

