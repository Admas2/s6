﻿#include "BooleanOperation.h"
#include "global.h"
#include "acestext.h"
#include "EntityXData.h"

//将srcID合并到destID中去，注意：然后srcID中的对象就为空了！
void UniteRegion(AcDbObjectId& destID, AcDbObjectId srcID) {
	if (srcID.isNull()) return;
	if (destID.isNull()) {
		destID = srcID;
		return;
	}
	AcDbEntity* pEnt = nullptr;
	es = acdbOpenAcDbEntity(pEnt, destID, AcDb::kForWrite);
	if (pEnt) {
		AcDbRegion* pRegionDest = AcDbRegion::cast(pEnt);
		es = acdbOpenAcDbEntity(pEnt, srcID, AcDb::kForWrite);
		if (pEnt) {
			AcDbRegion* pRegionSrc = AcDbRegion::cast(pEnt);
			es = pRegionDest->booleanOper(AcDb::kBoolUnite, pRegionSrc);
			assert(es == Acad::eOk);
			pRegionSrc->close();
		}
		pRegionDest->close();
	}
}

//使用 destIDs中的实体去“减掉” 与sliceIDs中的相交的部分
//相减结果反应在destIDs中，sliceIDs中原实体保持不变
void SubtractSolid(const AcDbObjectId& destID, const AcDbObjectId& sliceID) {
	AcDbEntity* pEnt1 = nullptr;
	es = acdbOpenAcDbEntity(pEnt1, destID, AcDb::kForWrite);
	AcDb3dSolid* pSolidDest = AcDb3dSolid::cast(pEnt1);
	if (es == Acad::eOk) {
		AcDbEntity* pEnt2 = nullptr;
		es = acdbOpenAcDbEntity(pEnt2, sliceID, AcDb::kForWrite);
		if (es == Acad::eOk) {
			AcDb3dSolid* pSolidSrc = AcDb3dSolid::cast(pEnt2->clone());
			if (es == Acad::eOk && pSolidSrc != nullptr) {
				pSolidDest->booleanOper(AcDb::kBoolSubtract, pSolidSrc);
				pSolidSrc->clone();
				pEnt2->close();
			}
		}
		pSolidDest->close();
	}
}

//使用 destID中的实体去“减掉” 与sliceIDs中的相交的部分
//相减结果反应在destIDs中，sliceIDs中原实体保持不变
void SubtractSolids(AcDbObjectId destID, const AcArray<AcDbObjectId>& sliceIDs) {
		for (AcDbObjectId sliceID : sliceIDs) {
			SubtractSolid(destID, sliceID);
		}
}

void SubtractSolids(const AcArray<AcDbObjectId>& destIDs, const AcArray<AcDbObjectId>& sliceIDs) {
	acedSetStatusBarProgressMeter(TEXT("两组实体的求差...:"), 0, destIDs.length() * sliceIDs.length());
	for (int i = 0; i < destIDs.length(); i++) {
		for (int j = 0; j < sliceIDs.length(); j++) {
			logInfo.Format(L"实体求差： %d/%d vs %d/%d\n", i + 1, destIDs.length(), j + 1, sliceIDs.length());
			RDTRACE
			acedSetStatusBarProgressMeterPos(i*destIDs.length() + j);
			bool isOptimized = false;
			SubtractSolid(destIDs[i], sliceIDs[j]);
		}
	}
	acedRestoreStatusBar();
}

void SubtractRegion(const AcDbObjectId& destID,const AcDbObjectId& sliceID) {
	AcArray<AcDbObjectId> sliceIDs;
	sliceIDs.append(sliceID);
	AcArray<AcDbObjectId> destIDs;
	destIDs.append(destID);
	SubtractRegions(destIDs,sliceIDs);
}

//使用 destIDs中的面域去“减掉” 与sliceIDs中的相交的部分
//相减结果反应在destIDs中，sliceIDs中原面域保持不变
void SubtractRegions(const AcArray<AcDbObjectId>& destIDs, const AcArray<AcDbObjectId>& sliceIDS) {
	if (destIDs.isEmpty() || sliceIDS.isEmpty()) return;
	for (int index = 0; index < destIDs.length(); index++) {
		AcDbEntity* pEnt = nullptr;
		es = acdbOpenAcDbEntity(pEnt, destIDs[index], AcDb::kForWrite);
		if (pEnt == nullptr) continue;
		AcDbRegion* pRegionDest = AcDbRegion::cast(pEnt);
		for (int j = 0; j < sliceIDS.length(); j++) {
			es = acdbOpenAcDbEntity(pEnt, sliceIDS[j], AcDb::kForWrite);
			if (pEnt == nullptr) continue;
			AcDbRegion* pRegionSlice = AcDbRegion::cast(pEnt->clone());
			es = pRegionDest->booleanOper(AcDb::kBoolSubtract, pRegionSlice);
			assert(es == Acad::eOk);
			pRegionSlice->close();
			pEnt->close();
		}
		pRegionDest->close();
	}
}


void IntersectRegions(AcDbObjectId& destID, const AcArray<AcDbObjectId>& srcIDs) {

	AcDbObjectId srcID;
	for (int index = 0; index < srcIDs.length(); index++) {
		if (!srcIDs[index].isNull()) UniteRegion(srcID, CloneEntity(srcIDs[index],layer_Sections));
	}

	AcDbEntity* pEnt = nullptr;
	es = acdbOpenAcDbEntity(pEnt, srcID, AcDb::kForWrite);
	if (pEnt) {
		AcDbRegion* pRegionSrc = AcDbRegion::cast(pEnt);
		es = acdbOpenAcDbEntity(pEnt, destID, AcDb::kForWrite);
		if (pEnt) {
			AcDbRegion* pRegionDest = AcDbRegion::cast(pEnt);
			es = pRegionDest->booleanOper(AcDb::kBoolIntersect, pRegionSrc);
			assert(es == Acad::eOk);
			pRegionDest->close();
		}
		pRegionSrc->close();
	}
	RemoveEntityFromModelSpace(srcID);
}

void IntersectRegions(AcArray<AcDbObjectId>& destIDs, const AcArray<AcDbObjectId>& srcIDs) {
	for (int index = 0; index < destIDs.length(); index++) {
		AcDbObjectId& destID = destIDs[index];
		IntersectRegions(destID, srcIDs);
	}
}

void MirrorRegions(AcArray<AcDbObjectId>&  acdbRegions) {
	AcDbObjectIdArray newEntities;
	for (int index = 0; index < acdbRegions.length(); index++) {
		AcDbEntity *pEntity = nullptr;
		es = acdbOpenObject(pEntity, acdbRegions[index], AcDb::kForWrite);
		if (es == Acad::eOk) {
			AcGeMatrix3d mat;
			mat.setToMirroring(AcGeLine3d(AcGePoint3d(0, 0, 0), AcGeVector3d(0, 1, 0)));
			es = pEntity->transformBy(mat);
			assert(es == Acad::eOk);
			pEntity->close();
		}
	}
}

AcDbObjectId CloneEntity(AcDbObjectId srcID, eLayerID layerIndex) {
	AcDbObjectId id;
	if (!srcID.isNull()) {
		AcDbEntity* pEnt = nullptr;
		es = acdbOpenAcDbEntity(pEnt, srcID, AcDb::kForWrite);
		if (es == Acad::eOk && pEnt != nullptr) {
			AcDbEntity* pEnt2 = AcDbEntity::cast(pEnt->clone());
			id = AppendEntityToModelSpace(pEnt2, layerIndex);
			pEnt->close();
		}
	}
	return id;
}

AcArray<AcDbObjectId> BoydiesBooleanOpr(const AcArray<AcDbObjectId>& opr1, const AcArray<AcDbObjectId>& opr2, AcDb::BoolOperType type) {
	AcArray<AcDbObjectId> result;
	AcDbEntity* pBody1 = nullptr;
	AcDbEntity* pBody2 = nullptr;
	for (int index1 = 0; index1 < opr1.length(); index1++) {
		es = acdbOpenAcDbEntity(pBody1, opr1[index1], AcDb::kForWrite);
		if (es == Acad::eOk) {
			for (int index2 = 0; index2 < opr2.length(); index2++) {
				AcDb3dSolid* pBody1Cpy = AcDb3dSolid::cast(pBody1->clone());
				es = acdbOpenAcDbEntity(pBody2, opr2[index2], AcDb::kForWrite);
				if (es == Acad::eOk) {
					AcDb3dSolid* pBody2Cpy = AcDb3dSolid::cast(pBody2->clone());
					es = pBody1Cpy->booleanOper(type, pBody2Cpy);
					assert(es == Acad::eOk);
					if (pBody1Cpy->isNull()) {
						pBody1Cpy->erase();
					}
					else {
						result.append(pBody1Cpy->id());
					}
					pBody1Cpy->close();

					pBody2Cpy->erase();
					pBody2Cpy->close();
					pBody2->close();
				}
			}
		}
		pBody1->close();
	}
	return result;
}

AcArray<AcDbObjectId> IntersectSolids(const AcArray<AcDbObjectId>& opr1, const AcArray<AcDbObjectId>& opr2,eLayerID layerName) {
	acedSetStatusBarProgressMeter(TEXT("两组实体的求交...:"), 0, opr1.length() * opr2.length());
	AcArray<AcDbObjectId> result;
	int  c = 0.0;
	for (int i = 0; i < opr1.length(); i++) {
		for (int j = 0; j < opr2.length(); j++) {
			logInfo.Format(L"实体求交： %d/%d vs %d/%d\n", i + 1, opr1.length(), j + 1, opr2.length());
			RDTRACE
			acedSetStatusBarProgressMeterPos(i*opr1.length() + j);
			if (!opr1[i].isNull() && !opr2[j].isNull()) {
				AcDbObjectId id = IntersectSolid(opr1[i], opr2[j], layerName);
				if (!id.isNull() && id.isValid()) {
					result.append(id);
				}
				else {
					c++;
				}
			}
		}
	}
	int s = opr1.length() * opr2.length();
	double t = c * 1.0 / s * 100;
	logInfo.Format(L"本次求交 原需执行%d次，被优化掉 %d次\n",s,c); 
	RDTRACE
	acedRestoreStatusBar();
	return result;
}

AcArray<AcDbObjectId> IntersectSolids(AcDbObjectId id1, const AcArray<AcDbObjectId>& opr2, eLayerID layerName) {
	AcArray<AcDbObjectId> result;
	for (int j = 0; j < opr2.length(); j++) {
		if (!id1.isNull() && !opr2[j].isNull()) {
			AcDbObjectId id = IntersectSolid(id1, opr2[j], layerName);
			if (!id.isNull() && id.isValid()) {
				result.append(id);
			}
		}
	}
	return result;
}

AcDbObjectId IntersectSolid(AcDbObjectId id1, AcDbObjectId id2, eLayerID layerName) {
	//1.
	AcDbEntity* pEntity = nullptr;
	es = acdbOpenAcDbEntity(pEntity, id1, AcDb::kForWrite);
	if (es != Acad::eOk) return AcDbObjectId();
	AcDb3dSolid* pBody1 = AcDb3dSolid::cast(pEntity->clone());
	pEntity->close();
	if (pBody1 == nullptr) AcDbObjectId();

	//2.
	es = acdbOpenAcDbEntity(pEntity, id2, AcDb::kForWrite);
	if (es != Acad::eOk) {
		pBody1->close();
		return AcDbObjectId();
	}
	AcDb3dSolid* pBody2 = AcDb3dSolid::cast(pEntity->clone());
	pEntity->close();
	if (pBody2 == nullptr)  return AcDbObjectId();

	//3.
	es = pBody1->booleanOper(AcDb::kBoolIntersect, pBody2);
	if (es != Acad::eOk) {
		logInfo.Format(L"求交运算失败 %s\n", acadErrorStatusText(es));
		RDTRACE
		pBody2->close();	 pBody1->close();
		return AcDbObjectId();
	}

	//4.
	pBody2->erase();
	pBody2->close();

	//5.
	AcDbObjectId id3;
	if (pBody1->isNull()) {
		pBody1->erase();
		pBody1->close();
	}
	else {
		 id3 = AppendEntityToModelSpace(pBody1, layerName);
	}

	return id3;
}

//将srcIDs中的所有对象合并到destID中去，注意：然后srcIDs中的表示对象就为空了！
AcDbObjectId UniteRegions(const AcArray<AcDbObjectId>& srcIDs) {
	AcDbObjectId result;
	for (int index = 0; index < srcIDs.length(); index++) {
		UniteRegion(result, srcIDs[index]);
	}
	return result;

}

AcArray<AcDbObjectId> CloneEntities(const AcArray<AcDbObjectId>& srcIDs,eLayerID layerIndex) {
	AcArray<AcDbObjectId> ary;
	for (int index = 0; index < srcIDs.length(); index++) {
		ary.append(CloneEntity(srcIDs[index], layerIndex));
	}
	return ary;
}