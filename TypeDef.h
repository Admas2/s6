﻿#pragma once

#include <acarray.h>
#include <dbid.h>
#include <gept3dar.h>
#include <string>
#define PI  (atan(1) * 4)
//材料
struct tagMaterialKind {
	std::wstring  name;  //名称
};

struct tagCorrectStake {
	double idealStakeNo = -1.0;
	double actualStakeNo = -1.0;
};

//路面结构中每一层的属性
struct tagPavementLayer
{
	double  thickness = 0.0;			//厚度 (单位: m)
	int			materialID = 0;
	std::wstring LayerName;
	std::wstring Explain;
};

enum ecomponentKind {
	component_kind_divider = 0,
	component_kind_bench	 = 1,
	component_kind_margin  = 2,
	component_kind_null		 = 3,
};

enum eLayerID {
	layer_CenterLine		 = 0,
	layer_Sections			 = 1,
	layer_old_road			 = 2,
	layer_new_road			 = 3,
	layer_extend_road		 = 4,
	layer_intersect_road = 5,
	layer_Other				   = 6,
};

struct tagComponentParameter {
	ecomponentKind  componentKind = component_kind_null;
	double h1 = 0;
	double h2 = 0;
	double h3 = 0;
	double h4 = 0;
	
	double w1 = 0;
	double w2 = 0;
	double w3 = 0;
	double w4 = 0;
  
	double n1 = 0;	//组件参数中，坡度初始化为0，表示台阶是垂直的
	double n2 = 0;
	double n3 = 0;
	double n4 = 0;
	int stepCount = 0;				//台阶级数
	double horiOffset = 0.0;	//组件起始点,相对于设计中线水平方向偏移 正值
	double anchorY = 0.0;			//Y轴方向取值，有正负 负值表示位路面以下
	bool benchToOutward = true; //台阶的走向 true表示向外，false表示向内
	int materialID = 0; //材料ID号
	bool grading = true;      //对于边部构造，是否带有外侧放坡
	std::wstring Name;
	//---------------------------
	std::string component_id;
};

//定义路幅中的每一段
struct tagPavementSegment {
	int SortID = 0;
	double width = 0.0;
	double CrossSlop = 0.0;  // 横向坡度
	std::wstring SortName;  
};
//路幅的类型
enum eRoadRangeKind {
	rand_kind_null = 0,
	rand_kind_single = 1, //整体式路基 单幅路
	rand_kind_double = 2, //分离式路基 双幅路
};

enum tagWideningType {
	widening_type_null = 0,
	widening_type_double_widening = 1,			//双侧拼宽   
	widening_type_transitional_widening = 2,//过渡拼宽 
	widening_type_single_widening = 3,			//单侧拼宽    
	widening_type_build_new = 4,						//完全新建
};

enum tagNewRoadType {
	new_road_type_null = 0,
	new_road_type_direct_overlap		= 1,	//直接加铺
	new_road_type_step_milling			= 2,  //分台阶铣刨
	new_road_type_add_leveling_layer = 3,	//增加调平层
};

struct tagOverlapArea {
	int levelingLayerMaterialID = 0;			//调平层的材料编号
	AcArray<tagPavementLayer>  layers;
};

struct tagSideSectionData {
	double  stakeNo = -1;							//所在桩号
	bool isLeft = true;
	AcArray<tagPavementSegment>     pavementWidth;		   //路幅
	AcArray<tagPavementLayer>       pavamentStruct;			 //路面结构
	AcArray<tagComponentParameter>  componentParameters; //组件参数
	bool isNewRoad = false;  //新路截面还是老路截面

	//下面7个成员仅针对新路截面时有用
	tagWideningType wideningType = widening_type_null;	 
	bool overlapSame = false;	// false 加铺异侧   true 加铺同侧
	//如果某一侧只有一个加铺区域，就使用overlapArea1，
	//如果某一侧有两个加铺区域，overlapArea1表示向内（靠近设计中线)的一个，overlapArea2表示向外的一个
	tagOverlapArea  overlapArea1;			//加铺区域1的路面结构
	tagOverlapArea  overlapArea2;			//加铺区域2的路面结构
	tagNewRoadType newRoadType1 = new_road_type_direct_overlap; //铺区域1的铣刨类型
	tagNewRoadType newRoadType2 = new_road_type_direct_overlap; //铺区域2的铣刨类型
	AcArray<tagComponentParameter>  dividerBenchParameters;     //分隔台阶,按插入点水平方向偏移 从小到大排序
	AcArray<AcGePoint3d> groundLine;		//两条老路地面线，(x1,y1)---(x2,y2)   (x3,y3) --- (x4,y4)
	                                    //按x轴方向排升序 即要确保：x1 < x2 < x3 < x4  

	// 路面的原始宽度（没有加上横坡的时的宽度）																										
	double OriginalWidth() const {
		double r = 0.0;
		for (int index = 0; index < pavementWidth.length(); index++) {
			r += pavementWidth[index].width;
		}
		return r;
	}

	// 路面深度
	double TotalThickness() const {
		double r = 0.0;
		for (int index = 0; index < pavamentStruct.length(); index++) {
			r += pavamentStruct[index].thickness;
		}
		return r;
	}

	//下面方法仅用于调试--------------------------------------
	void ResetWidthScale(double scale) {
		for (int index = 0; index < pavementWidth.length(); index++) {
			pavementWidth[index].width *= scale;
		}
	}
	void ResetSlopScale(double scale) {
		for (int index = 0; index < pavementWidth.length(); index++) {
			pavementWidth[index].CrossSlop *= scale;
		}
	}
};

//定义中线上某一连连续区间
struct tagLineInterval {
	double startStake = -1.0, endStake = -1.0; //桩号范围
	tagLineInterval() {}
	tagLineInterval(int s, int e) {
		startStake = s;
		endStake = e;
	}
	bool in(tagLineInterval other) {
		return (startStake <= other.startStake && other.endStake <= endStake);
	}
	int length() {
		return (endStake - startStake);
	}
};
struct tagFitPoint {
	double stakeNo = -1;//桩号
	double x = 0.0;
	double y = 0.0;
	double z = 0.0;
	double A = 0.0; //此点处平面方向角
	AcGeVector3d vecTagent;  //此点的切换向方向
};
struct tagMeasureWidth {
	double stakeNo = -1;
	double LeftWidth = 0.0;
	double RightWidth = 0.0;
};

struct tagRoadwayData {
	double  startStake = -1, endStake = -1;					// 道路建模的起止桩号
	std::wstring RoadLine_Name;
	std::string  roadLineID;
	bool isNewRoad = false;
	AcArray<tagFitPoint> fitPoints;									//
	AcArray<tagSideSectionData>		sections;					//截面参数
	AcArray<tagLineInterval>  allNoModelInervals;		// 所有不建模型区间
	AcArray<tagLineInterval>  modelSegments;				// 建模区段
};

struct tagOptions {
	AcArray<ACHAR*>  layers;
	ACHAR* appName;
	std::string dbName;
	bool    isStepModel = true;			      //true :使用逐步长建模  false: 按多步长建模
	double	maxModelLength = 50;					//多步长建模时，每个道路体的最大长度
	bool    isMoveToViewCenter = false;		//是否将线路移到视图中心，仅为调试观察使用
	double  searchRange = 20;							//进行两条线路求交,求差运算时，向前，向后的搜索范围
	bool    disableLog = false;						//是否禁止在建模过程中输出日志信息
	bool    onlyBuildIntersectBody = false; //对于扩建路，仅仅只创建相交体（新∩老），不创建完整体（新+（老-新∩老））
};

enum eSectionSideKind {
	section_side_kind_null										= 0,
	section_side_kind_old_road								= 1,		//老路
	section_side_kind_new_road_expand					= 2,		//新路扩建侧
	section_side_kind_new_road_overlap_inner  = 3,		//新路加铺区域内侧
	section_side_kind_new_road_overlap_outter = 4,		//新路加铺区域外侧
	section_side_kind_new_road_overlap_inner_and_outter = 5,		//新路加铺区域内侧和外侧（仅于调平层标识的情况）
	section_side_kind_intersect								= 6 		//新老路相交部分
};



struct tagPointPair {
	AcGePoint3d pt1;
	AcGePoint3d pt2;
};

struct tagRegionXData {
	int materialID = -1;		//材料ID号
	bool isLeft = true;
	int colorIndex = -1;
	eSectionSideKind  sideKind = section_side_kind_null;
	std::wstring name;
	double stakeNo = -1;
	double depth = -1;// 只有路面结构层每层才有厚度，其它组件此项没有设值
};

//三维体的附加信息
struct tagBodyXData {
	double startStake = -1;
	double endStake   = -1;
	int materialID = -1;
	eSectionSideKind   sideKind = section_side_kind_null;
	bool isLeft       = true;
	std::wstring      name;

	double volumn = -1.0;  //体积
};