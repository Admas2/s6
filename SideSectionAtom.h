﻿#pragma once
#include "SectionComponent.h"
class CSideSectionAtom
{
private:
	int colorIndexStart_ = 9;
	bool isLeft_ = true;
	double stakeNo_ = -1;
	const AcArray<tagPavementSegment>&     pavementWidth_;		   //路幅
	const AcArray<tagPavementLayer>&       pavamentStruct_;			 //路面结构
	const AcArray<tagComponentParameter>  componentParameters_; //组件参数
	AcArray<AcGePoint3d> groundLine_;					//两条老路地面线
	double originalWidth_ = 0.0, originalHeight_ = 0.0;
private:
	AcArray<CSectionComponent*> componets_;		//本截面所含的组件
	AcArray<AcDbObjectId>  acdbRegions_;			//组成截面的面域
	AcArray<AcDbObjectId>  sideRoadStructRegions_;
	eSectionSideKind  sideKind_ = section_side_kind_null;

public:
	CSideSectionAtom(double stakeNo,bool isLeft,
		const AcArray<tagPavementSegment>& pavementWidth,
		const AcArray<tagPavementLayer>& pavamentStruct,
		const AcArray<tagComponentParameter>& componentParameters,
		eSectionSideKind  sideKind);

	CSideSectionAtom(double stakeNo,bool isLeft,
		const AcArray<tagPavementSegment>& pavementWidth,
		const AcArray<tagPavementLayer>& pavamentStruct,
		eSectionSideKind  sideKind);

	~CSideSectionAtom();
	
	void Create(tagNewRoadType newRoadType);
	void Create(const tagComponentParameter& dividerBench, bool isInward, tagNewRoadType newRoadType);
	void Create(const tagComponentParameter& dividerBenchInward, const tagComponentParameter& dividerBenchOutward,tagNewRoadType newRoadType);

	void appendComponent(CSectionComponent*);
	AcArray<AcDbObjectId> GetRegions() const;
	double Totalthickness();
	double OriginalWidth();
	double OriginalHeight();
	double GetOutwardSlop();
	const AcArray<tagPavementSegment>& 	GetPavementWidth();
	AcArray<AcDbObjectId>  GetSideRoadStructRegions();
	eSectionSideKind GetSideKind() { return sideKind_; }
	double stakeNo() { return stakeNo_; }
	void SetGroundLine(AcArray<AcGePoint3d> groundLine);
private:
	void RemovePart(const tagComponentParameter& dividerBench, bool isInward);
	void RemovePart(const tagComponentParameter& dividerBenchNear, const tagComponentParameter& dividerBenchFar);
	void CreatePavementStruct(tagNewRoadType newRoadType);
	void CreatePavementComponent();
	AcDbObjectId CreateSliceBenchRegion(const tagComponentParameter& dividerBench, bool isInward);
	void CalculateOrginWidthHeight();
public:
	void MillingWithStep(AcArray<AcDbObjectId>& rects, double totalW, double totlaH);
};

