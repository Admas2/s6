﻿#include "pch.h"
#include "TestPlatformDlg.h"
#include "afxdialogex.h"
#include "DividerDlg.h"
#include "BenchDlg.h"
#include "MarginDlg.h"
#include "TestPlatform.h"
#include "RoadSection.h"
#include "Roadway.h"
#include "RoadSection.h"

IMPLEMENT_DYNAMIC(CTestPlatformDlg, CDialogEx)

CTestPlatformDlg::CTestPlatformDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_TEST_PLATFORM_DLG, pParent)
	, m_bHasBench(TRUE)
	, m_bHasDivider(TRUE)
	, m_bHasMargin(TRUE)
	, m_bSectionMirror(TRUE)
	, m_dbPavementSlop(0.0)
{
	m_ia = CTestPlatform::MockCreateInputUserData1();
	
	//目前只测试一个截面的情况
	tagSectionData sd;
	sd.statkeNo = -1;
	sd.pavementSlop = 0.00;
	m_ia.sections.append(sd);

	m_dividerCP.componentKind = component_kind_divider;
	m_dividerCP.h1 = 3.16;
	m_dividerCP.h2 = 1.9;
	m_dividerCP.h3 = 0.2;
	m_dividerCP.w_1 = 2.4;
	m_dividerCP.w_2 = 1.4;
	m_dividerCP.materialIndex = 4;
	m_benchCP.componentKind = component_kind_bench;
	m_benchCP.h1 = 0.5;
	m_benchCP.h2 = 1.5;
	m_benchCP.h3 = 2.8;
	m_benchCP.w_1 = 0.3;
	m_benchCP.w_2 = 6;
	m_marginCP.componentKind = component_kind_margin;
	m_marginCP.h1 = 2;
	m_marginCP.h2 = 1;
	m_marginCP.h3 = 1.1;
	m_marginCP.w_1 = 1.8;
	m_marginCP.w_2 = 0.3;
	m_marginCP.w_3 = 3;
	m_marginCP.n_1 = 4.5;
	m_marginCP.materialIndex = 6;
}

CTestPlatformDlg::~CTestPlatformDlg()
{
}

void CTestPlatformDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Check(pDX, IDC_CHECK_BENCH, m_bHasBench);
	DDX_Check(pDX, IDC_CHECK_DIVIDER, m_bHasDivider);
	DDX_Check(pDX, IDC_CHECK_MARGIN, m_bHasMargin);
	DDX_Check(pDX, IDC_CHECK_COMPONET_MIRROR, m_bSectionMirror);
	DDX_Text(pDX, IDC_EDIT_PAVEMNT_SLOP, m_dbPavementSlop);
	DDX_Control(pDX, IDC_COMBO_ROAD_WIDTH, m_ctrlRoadWidth);
	DDX_Control(pDX, IDC_COMBO_ROAD_FLOOR, m_ctrlRoadFloor);
	DDX_Control(pDX, IDC_LIST_ROAD_WIDTH_DETAIL, m_ctrlWidthDetail);
	DDX_Control(pDX, IDC_LIST_ROAD_FLOOR_DETAIL, m_ctrlRoadFloorDetail);
}


BEGIN_MESSAGE_MAP(CTestPlatformDlg, CDialogEx)
	ON_BN_CLICKED(IDC_COMPONET_DIVIER, &CTestPlatformDlg::OnBnClickedComponetDivier)
	ON_BN_CLICKED(IDC_BUTTON_COMPONET_BENCH, &CTestPlatformDlg::OnBnClickedButtonComponetBench)
	ON_BN_CLICKED(IDC_BUTTON_COMPONET_MARGIN, &CTestPlatformDlg::OnBnClickedButtonComponetMargin)
	ON_BN_CLICKED(IDC_BUTTON_REBUILD_3D_SOLID, &CTestPlatformDlg::OnBnClickedButtonRebuild3dSolid)
	ON_BN_CLICKED(IDC_CHECK_DIVIDER, &CTestPlatformDlg::OnBnClickedCheckDivider)
	ON_BN_CLICKED(IDC_CHECK_BENCH, &CTestPlatformDlg::OnBnClickedCheckBench)
	ON_BN_CLICKED(IDC_CHECK_MARGIN, &CTestPlatformDlg::OnBnClickedCheckMargin)
	ON_BN_CLICKED(IDC_BUTTON_REBUILD_SECTION, &CTestPlatformDlg::OnBnClickedButtonRebuildSection)
	ON_BN_CLICKED(IDC_BUTTON_REBUILD_CENTER_LINE, &CTestPlatformDlg::OnBnClickedButtonRebuildCenterLine)
	ON_BN_CLICKED(IDOK, &CTestPlatformDlg::OnBnClickedOk)
	ON_CBN_SELCHANGE(IDC_COMBO_ROAD_WIDTH, &CTestPlatformDlg::OnSelchangeComboRoadWidth)
	ON_CBN_SELCHANGE(IDC_COMBO_ROAD_FLOOR, &CTestPlatformDlg::OnSelchangeComboRoadFloor)
	ON_WM_DESTROY()
	ON_WM_CLOSE()
END_MESSAGE_MAP()

void CTestPlatformDlg::OnBnClickedComponetDivier()
{
	CDividerDlg dlg(m_dividerCP);
	if (dlg.DoModal() == IDOK) {
		m_dividerCP = dlg.m_cp;
	}
}


void CTestPlatformDlg::OnBnClickedButtonComponetBench()
{
	CBenchDlg dlg(m_benchCP);
	if (dlg.DoModal() == IDOK) {
		m_benchCP = dlg.m_cp;
	}
	
}


void CTestPlatformDlg::OnBnClickedButtonComponetMargin()
{
	CMarginDlg dlg(m_marginCP);
	if (dlg.DoModal() == IDOK) {
		m_marginCP = dlg.m_cp;
	}
}


void CTestPlatformDlg::OnBnClickedOk()
{
	CDialogEx::OnOK();
}


void CTestPlatformDlg::OnBnClickedCheckDivider()
{
	this->UpdateData();
	this->GetDlgItem(IDC_COMPONET_DIVIER)->EnableWindow(m_bHasDivider);
}


void CTestPlatformDlg::OnBnClickedCheckBench()
{
	this->UpdateData();
	this->GetDlgItem(IDC_BUTTON_COMPONET_BENCH)->EnableWindow(m_bHasBench);
}


void CTestPlatformDlg::OnBnClickedCheckMargin()
{
	this->UpdateData();
	this->GetDlgItem(IDC_BUTTON_COMPONET_MARGIN)->EnableWindow(m_bHasMargin);
}


void CTestPlatformDlg::OnSelchangeComboRoadWidth()
{
	CComboBox* pComboxBox = static_cast<CComboBox*>(GetDlgItem(IDC_LIST_ROAD_WIDTH_DETAIL));
	int nSel = pComboxBox->GetCurSel();
	if (nSel == 0) {
//		m_ia.aSideWdth = 45 / 2.0;
	}
	if (nSel == 1) {
//		m_ia.aSideWdth = 32 / 2.0;
	}
}


void CTestPlatformDlg::OnSelchangeComboRoadFloor()
{
	CComboBox* pComboxBox = static_cast<CComboBox*>(GetDlgItem(IDC_LIST_ROAD_FLOOR_DETAIL));
	int nSel = pComboxBox->GetCurSel();
	nSel = 1; //暂时这样用，对话框中CCombox 控件有问题
	//m_ia.sf = gPredefinedPavementStructureList[nSel];
}


BOOL CTestPlatformDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	m_ctrlRoadWidth.AddString(L"45 000 mm");
	m_ctrlRoadWidth.AddString(L"32 000 mm");
	m_ctrlRoadWidth.SetCurSel(1);

	
	m_ctrlRoadFloor.AddString(L"0.单层");
	m_ctrlRoadFloor.AddString(L"1.三层");
	m_ctrlRoadFloor.AddString(L"2.七层");
	m_ctrlRoadFloor.SetCurSel(1);
	return TRUE;  // return TRUE unless you set the focus to a control
								// EXCEPTION: OCX Property Pages should return FALSE
}

void CTestPlatformDlg::OnBnClickedButtonRebuildCenterLine()
{
	UpdateData();
	acDocManager->lockDocument(acDocManager->curDocument());
	//生成道路中线
	m_centerLine.Clear();
	//m_centerLine.Create(m_ia.fitPoints);
	acDocManager->unlockDocument(acDocManager->curDocument());
}

void CTestPlatformDlg::OnBnClickedButtonRebuildSection()
{
	UpdateData();
	acDocManager->lockDocument(acDocManager->curDocument());
	//1.收集数据
	OnSelchangeComboRoadWidth();
	OnSelchangeComboRoadFloor();
	m_sectionData.pavementSlop = m_dbPavementSlop;
	m_sectionData.componentParameters.removeAll();
	m_sectionData.componentParameters.append(m_floorCP);
	if (m_bHasDivider) {
		m_sectionData.componentParameters.append(m_dividerCP);
	}
	if (m_bHasBench) {
		m_sectionData.componentParameters.append(m_benchCP);
	}
	if (m_bHasMargin) {
		m_sectionData.componentParameters.append(m_marginCP);
	}

	//2.生成截面
	m_section.Clear();
	//m_section.setMirror(m_bSectionMirror);
//	m_section.SetASideWidth(m_ia.aSideWdth);
	//m_section.Create(m_ia.sf,m_sectionData);
	acDocManager->unlockDocument(acDocManager->curDocument());
}

void CTestPlatformDlg::OnBnClickedButtonRebuild3dSolid()
{
	UpdateData();
	//1.重建中线
	OnBnClickedButtonRebuildCenterLine();
	
	//2.重建截面
	OnBnClickedButtonRebuildSection();

	acDocManager->lockDocument(acDocManager->curDocument());
	//3.创建道路
	assert(false);
	//m_roadWay.SetCenterLine(&m_centerLine);
	m_roadWay.AppendSection(&m_section);

	//4.生成道路实体
	//m_roadWay.Rebuild3DSolid();
	acDocManager->unlockDocument(acDocManager->curDocument());
}

void CTestPlatformDlg::OnDestroy()
{
	CDialogEx::OnDestroy();
}


void CTestPlatformDlg::OnClose()
{
	CDialogEx::OnClose();
}
