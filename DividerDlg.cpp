// CDividerDlg.cpp : implementation file
//

#include "pch.h"
#include "DividerDlg.h"
#include "afxdialogex.h"


// CDividerDlg dialog

IMPLEMENT_DYNAMIC(CDividerDlg, CDialogEx)

CDividerDlg::CDividerDlg(tagComponentParameter cp,CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_DIVIDER_DLG, pParent)
	, m_cp(cp)
{

}

CDividerDlg::~CDividerDlg()
{
}

void CDividerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_H1, m_cp.h1);
	DDX_Text(pDX, IDC_EDIT_H2, m_cp.h2);
	DDX_Text(pDX, IDC_EDIT_H3, m_cp.h3);
	DDX_Text(pDX, IDC_EDIT_B1, m_cp.w_1);
	DDX_Text(pDX, IDC_EDIT_B2, m_cp.w_2);
}


BEGIN_MESSAGE_MAP(CDividerDlg, CDialogEx)
	ON_BN_CLICKED(IDOK, &CDividerDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CDividerDlg message handlers


void CDividerDlg::OnBnClickedOk()
{
	UpdateData(true);
	CDialogEx::OnOK();
}
