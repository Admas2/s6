﻿#include "pch.h"
#include "RoadCenterLine.h"
#include "TypeDef.h"
#include "global.h"

CRoadCenterLine::CRoadCenterLine() {

}

CRoadCenterLine::~CRoadCenterLine() {
	RemoveEntityFromModelSpace(curveEntityID_);
	RemoveEntityFromModelSpace(splineID_);
}
CRoadCenterLine::CRoadCenterLine(std::string roadLineID):roadLineID_(roadLineID) {

}

CRoadCenterLine* CRoadCenterLine::PartialCopy() {
	CRoadCenterLine* result = new CRoadCenterLine(this->roadLineID_);
	result->segmentPoints_ = this->segmentPoints_;					 
	result->noModelIntervals_ = this->noModelIntervals_;			 
	return result;
}

void CRoadCenterLine::Create(AcArray<tagFitPoint> fitPoints) {
	
	assert(fitPoints.length() > 1);

	//1.产生一条空间曲线实体
	AcGePoint3dArray positions;
	for (int index = 0; index < fitPoints.length(); index++) {
		positions.append(AcGePoint3d(fitPoints[index].x, fitPoints[index].y, fitPoints[index].z));
	}
	
	//仅用于显示线
	AcDb3dPolyline *pRoadCenterLine1 = new AcDb3dPolyline(AcDb::k3dSimplePoly, positions);
	curveEntityID_ = AppendEntityToModelSpace(pRoadCenterLine1, layer_CenterLine);
	
	//仅用于造型的线不显示
	AcDbSpline* pRoadCenterLine2 = new AcDbSpline(positions, 4, 0.0);
	pRoadCenterLine2->setVisibility(AcDb::Visibility::kInvisible); 
	splineID_ = AppendEntityToModelSpace(pRoadCenterLine2, layer_CenterLine);

	segmentPoints_ = fitPoints;
}

void CRoadCenterLine::RemoveEntities() {
	RemoveEntityFromModelSpace(curveEntityID_);
	segmentPoints_.removeAll();
}

//加入不建模型区间段
void CRoadCenterLine::AppendNoModelInterval(int startStake, int endStake) {
	tagLineInterval interval(startStake, endStake);
	noModelIntervals_.append(interval);
}

//所有需要建模的区段集
AcArray<tagLineInterval> CRoadCenterLine::GetModelIntervals() {
	//1.初始时将原已经有段加入result中
	AcArray<tagLineInterval> result;
	if (modelSegments_.isEmpty()) {
		int t = segmentPoints_.length() - 1;
		tagLineInterval a;
		a.startStake = segmentPoints_[0].stakeNo;
		a.endStake = segmentPoints_[t].stakeNo;
		result.append(a); //初始时将整个中线纳入其中
	}
	else {
		result.append(modelSegments_);
	}

	//2.合并上不建模型区段
	//算法：定义一个新的集合result,用于记录所有区段，初始时将modelSegments_都纳入其中，
	//如果modelSegments_为空，就将整条线路者纳入其中
	//不断从 需要中断的集合noModelIntervals_中，取出某一中断区间m,
	//逐个比较result中的元素，如果其中某一元素（连续区段）a,包含了这个中断区间m，就将这个元素a从result中去掉，然后加入两个新
	//分成区间newInterval1和newInterval2到result中
	//对所有noModelIntervals_中的元素都这样处理一遍
	AcArray<tagLineInterval>	m = noModelIntervals_;
	for (int j = 0; j < m.length(); j++) {
		const tagLineInterval b = m[j]; //取出每个需要中断区间
		for (int index = 0; index < result.length(); index++) {
			tagLineInterval a = result[index];
			if (a.in(b)) {
				tagLineInterval newInterval1(a.startStake, b.startStake);
				if (newInterval1.length() > 0) {
					result.append(newInterval1);	 //加入分成的新区段
				}
				tagLineInterval newInterval2(b.endStake, a.endStake);
				if (newInterval2.length() > 0) {
					result.append(newInterval2);  //加入分成的新区段
				}
				result = result.removeAt(index);	 //去掉被打断的区段
				break;
			}
		}
	}
	
	//for (tagLineInterval item : result) {
	//	logInfo.Format(L"划分区间段：%.2f -- %.2f\n", item.startStake, item.endStake); RDTRACE
	//}

	return result;
}

//计算道路全长
double CRoadCenterLine::GetWholeLength() {
	assert(segmentPoints_.length() > 0);
	double result = GetLastStakeNo() - segmentPoints_[0].stakeNo;
	return result;
}

//得到与指定桩号最接近项的索引
int CRoadCenterLine::GetIndexAtStake(double stakeNo) const{
	assert(segmentPoints_.length() > 1);
	int result = 0;
	double minDist = fabs(segmentPoints_[0].stakeNo - stakeNo);
	for (int index = 1; index < segmentPoints_.length(); index++) {
		double dist = fabs(segmentPoints_[index].stakeNo - stakeNo);
		if (dist < minDist) {
			minDist = dist;
			result = index;
		}
	}
	return result;
}

double CRoadCenterLine::GetLastStakeNo() const {
	assert(segmentPoints_.length() > 0);
	int lastIndex = segmentPoints_.length() - 1;
	return segmentPoints_[lastIndex].stakeNo;
}

double CRoadCenterLine::GetFirstStakeNo() const {
	assert(segmentPoints_.length() > 0);
	return segmentPoints_[0].stakeNo;
}

AcGeVector3d CRoadCenterLine::GetTagentDir(double stakeNo) const {
	AcGeVector3d vecTagent;
	int index = GetIndexAtStake(stakeNo);
	if (0 <= index && index < segmentPoints_.length()) {
		vecTagent = segmentPoints_[index].vecTagent;
	}
	return vecTagent;
}

AcGePoint3d  CRoadCenterLine::GetPoint(double stakeNo) const {
	assert(segmentPoints_.length() > 0);
	int index = GetIndexAtStake(stakeNo);
	return AcGePoint3d(segmentPoints_[index].x, segmentPoints_[index].y, segmentPoints_[index].z);
}

AcGePoint3dArray CRoadCenterLine::GetSegmentPoints() const {
	AcGePoint3dArray positions;
	for (int index = 0; index < segmentPoints_.length(); index++) {
		positions.append(AcGePoint3d(segmentPoints_[index].x, segmentPoints_[index].y, segmentPoints_[index].z));
	}
	return positions;
}

void CRoadCenterLine::DebugLookData() {
	for (int index = 0; index < segmentPoints_.length(); index++) {
		tagFitPoint& pt = segmentPoints_[index];
		wchar_t buffer[500];
		swprintf_s(buffer,L"%d,x = %10.2f,y = %10.2f,z = %10.2f\n",index,segmentPoints_[index].x, segmentPoints_[index].y, segmentPoints_[index].z);
		::OutputDebugString(buffer);
	}
}

void CRoadCenterLine::SetNoModelIntervals(const AcArray<tagLineInterval>&	noModelIntervals) {
	noModelIntervals_.removeAll();
	noModelIntervals_.append(noModelIntervals);
}

void CRoadCenterLine::SetModelSegments(const AcArray<tagLineInterval>&	modelSegments) {
	modelSegments_.removeAll();
	modelSegments_.append(modelSegments);
}
