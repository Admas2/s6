﻿#pragma once
#include "TypeDef.h"
#include <windows.h>
bool CheckRawModelData(const tagRoadwayData& rd);
bool CheckSection(const tagSideSectionData& ssd);
bool CheckComponentDivider(const tagComponentParameter& cp);
bool CheckComponents(const tagComponentParameter& cp, double originalWidth, double totalthickness);