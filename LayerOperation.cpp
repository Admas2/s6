﻿#include "LayerOperation.h"
#include "global.h"

//创建一个命名图层，如果已经存在就不创建，直接返回
bool CreateLayer(AcDbDatabase* pDbDatabase,const ACHAR* layerName)
{
	//获得当前图形的层表
	AcDbLayerTable *pLayerTbl = nullptr;
	es = pDbDatabase->getLayerTable(pLayerTbl, AcDb::kForWrite);
	if (es != Acad::eOk) {
		return false;
	}

	//是否已经包含指定的层表记录
	if (pLayerTbl->has(layerName)) {
		pLayerTbl->close();
		return false;
	}

	//创建层表记录
	AcDbLayerTableRecord *pLayerTblRcd = new AcDbLayerTableRecord();
	pLayerTblRcd->setName(layerName);

	//将新建的层表记录添加到层表中
	AcDbObjectId layerTblRcdId;
	pLayerTbl->add(layerTblRcdId, pLayerTblRcd);

	//设置当前图层
	pDbDatabase->setClayer(layerTblRcdId);

	//关闭层表
	pLayerTblRcd->close();
	pLayerTbl->close();
	return true;
}

// 使用[图形对话框]设置指定图层的颜色
void SetLayerColor()
{
	//获得当前图形的层表
	AcDbLayerTable *pLayerTbl;
	acdbHostApplicationServices()->workingDatabase()->getLayerTable(pLayerTbl, AcDb::kForRead);
	//判断是否包含指定名称的层表记录
	ACHAR* layerName = L"admas";
	if (!pLayerTbl->has(layerName)) {
		pLayerTbl->close();
		return;
	}
	//获得指定层表记录的指针
	AcDbLayerTableRecord *pLayerTblRcd = nullptr;
	pLayerTbl->getAt(layerName, pLayerTblRcd, AcDb::kForWrite);
	//弹出“颜色”对话框
	AcCmColor oldColor = pLayerTblRcd->color();
	//图层修改前的颜色
	int nCurColor = oldColor.colorIndex();
	//用户选择的颜色
	int nNewColor = oldColor.colorIndex();
	if (acedSetColorDialog(nNewColor, Adesk::kFalse, nCurColor)) {
		AcCmColor color;
		color.setColorIndex(nNewColor);
		pLayerTblRcd->setColor(color);
	}
	//关闭层表
	pLayerTblRcd->close();
	pLayerTbl->close();
}

// 删除一个图层
void DeleteLayer()
{
	//获得当前图形的层表
	AcDbLayerTable *pLayerTbl;
	acdbHostApplicationServices()->workingDatabase()->getLayerTable(pLayerTbl, AcDb::kForRead);
	
	//判断是否包含指定名称的层表记录
	ACHAR* layerName = L"admas";
	if (!pLayerTbl->has(layerName)) {
		pLayerTbl->close();
		return;
	}

	//获得指定层表记录的指针
	AcDbLayerTableRecord *pLayerTblRcd = nullptr;
	pLayerTbl->getAt(layerName, pLayerTblRcd, AcDb::kForWrite);
	pLayerTblRcd->erase();		//设置删除标记！！
	
	//关闭层表
	pLayerTblRcd->close();		
	pLayerTbl->close();
}

void IterateALayer() {
	//获取层表
	AcDbLayerTable* pLayerTable = NULL;
	acdbHostApplicationServices()->workingDatabase()->getLayerTable(pLayerTable, AcDb::kForWrite);
	//获取层表记录的浏览器
	AcDbLayerTableIterator *it = NULL;
	pLayerTable->newIterator(it);
	//循环遍历
	AcDbLayerTableRecord *pLayerTableRcd = NULL;
	for (it->start(); !it->done(); it->step())
	{
		it->getRecord(pLayerTableRcd, AcDb::kForRead);
		//TODO 处理
		pLayerTableRcd->close();
	}

	delete it;
	pLayerTable->close();
}

//得到指定层图层上的指定类型的所有实体
AcArray<AcDbObjectId>  GetEntitiesOnLayer(AcDbDatabase* pDbDatabase,const ACHAR* layerName, const AcRxClass* pClassKind)
{
	AcArray<AcDbObjectId>	 result;
	
	//1.检查是否存在此图层
	AcDbLayerTable *pLayerTbl = nullptr;
	es = pDbDatabase->getLayerTable(pLayerTbl, AcDb::kForRead);
	if (es != Acad::eOk) return result;
	if (!pLayerTbl->has(layerName))
	{
		logInfo.Format(L"当前图形中未包含 [%s] 图层！",layerName);	RDTRACE
		pLayerTbl->close();
		return result;
	}
	pLayerTbl->close();

	//2.遍历块记录，找到属于此层的实体
	AcDbBlockTable *pBlkTbl = nullptr;
	pDbDatabase->getBlockTable(pBlkTbl, AcDb::kForRead);
	AcDbBlockTableRecord *pBlkTblRcd = nullptr;
	pBlkTbl->getAt(ACDB_MODEL_SPACE, pBlkTblRcd, AcDb::kForRead);
	pBlkTbl->close();
	AcDbBlockTableRecordIterator* pltr = nullptr;
	pBlkTblRcd->newIterator(pltr);

	AcDbEntity *pEntity = nullptr;
	for (pltr->start(); !pltr->done(); pltr->step())
	{
		pltr->getEntity(pEntity, AcDb::kForRead);
		ACHAR* layerName2 = pEntity->layer();
		if (std::wstring(layerName2) == layerName)
		{
			//logInfo.Format(L"%s\n", pEntity->isA()->name());	 RDTRACE
			if (pEntity->isKindOf(pClassKind))
			{
				result.append(pEntity->id());
			}
		}
		pEntity->close();
	}
	delete pltr;
	pBlkTblRcd->close();
	return result;
}

void MoveEntityToLayer(AcDbObjectId entity, ACHAR* layer_name) {
	AcDbEntity* pEnt = nullptr;
	es = acdbOpenAcDbEntity(pEnt, entity, AcDb::kForWrite);
	if (es == Acad::eOk) {
		pEnt->setLayer(layer_name);
		pEnt->close();
	}
}

void MoveEntitiesToLayer(AcArray<AcDbObjectId> entities,ACHAR* layer_name) {
	CreateLayer(acdbHostApplicationServices()->workingDatabase(),layer_name);
	for (int index = 0; index < entities.length(); index++) {
		MoveEntityToLayer(entities[index], layer_name);
	}
}




