﻿#pragma once

#include <windows.h>
#include <string>
#include <vector>
using namespace std;
std::wstring Utf82Unicode(const std::string& utf8string);
std::string WideByte2Acsi(wstring& wstrcode);
string UTF_82ASCII(string& strUtf8Code);
wstring Acsi2WideByte(string& strascii);
std::string Unicode2Utf8(const std::wstring& widestring);
string ASCII2UTF_8(string& strAsciiCode);
