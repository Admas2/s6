#pragma once
#include "pch.h"
#include <acadstrc.h>
#include <dbspline.h>

class CRoadSection;
class CTestPlatform
{
public:
	CTestPlatform();
	virtual ~CTestPlatform();
	
	void TestLoftModel();
	void TestSection();
	void LookASection();
	void LookEntityXData();
	void TestCenterLine1();
	void TestCenterLine2();
	void TestRoadway1();
	void TestRoadway2();
	void TestRoadway3();
	
	void TestCreateOldRoad();
	void TestCreateNewRoad();
	void TestCreateExtendRoad();

	void TestRoadwayIntersect();
	void TestRoadwayDlg();
	void TestOverlaySection1();
	void TestOverlaySection2();
	void TestOverlaySection3();
	void TestOverlaySection4();
	void TestOverlaySection5();
	void TestShearTrasfrom1();
	void TestShearTrasfrom2();
	void TestShearTrasfrom3();
	void TestBenchRegion();
	void TestActualWidthByPickLine();
	void TestPickEntity();
	void TestDivider();
	void TestBench();
	void TestMargin();
	void TestSliceBody();
	void TestSetAddtionalInfo();
	void TestStatisticsBodyMaterialVolume();
	void TestAddLevelingLayer();
	void TestMillingWithStep();
	void TestRoadwayIntersect2();
	void TestInsterct();

private:
	void Initialize();
	void GeneratePavementStructTestData();
	void GeneratePavementWidthTestData();
	tagRoadwayData MockCreateInputUserData1();
	tagRoadwayData MockCreateInputUserData2();
	tagRoadwayData MockCreateInputUserData3();
	AcGePoint3dArray MockCreateRoadCenterLinePoint1();
	void ReadRoadCenterLineFromCSV(tagRoadwayData& ia,const char* fileName,bool needMove = true);
	void forceModifySectionsData(tagSideSectionData& ssd);
	AcArray<tagFitPoint> MockCreateRoadCenterLinePoint2();
	Acad::ErrorStatus MockGenerateRoadCenterLine1(AcDbSpline*& p3DRoadLine);
};

